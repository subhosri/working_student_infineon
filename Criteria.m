function [It2, Vt2, crit_spot, strContfail, nonfail, hbm_at_it2, V_HBM_fail, cnt_fail, criteria_matrix, Value_table,fail_pulse] = Criteria(data_matrix, criterion_mode, extraction_threshold, Min_Max_Limit, contact_fail, hbm_mode, min_fail, criteria_table, Max_Variation,threshold,spots_2,data_cell)
% This function chooses the right exraction criterion given by the user
% and calculates the critical spot.
% Called in TLP_analyse_vTG_v07_12
% -------------------------------------------------------------------------
% Input:
% data_matrix - contains all measurement data from TLP_data.csv
% criterion_mode - chosen criterium
% out - whole content of TLP_data.csv
% extraction_threshold
% Min_Max_Limit - threshold given by user, used in case of Min Limit or Max Limit
% contact_fail
% Statistik
% hbm_mode
% -----------------------------------------------------------------------
%augerufen im TLP_analyse_vTG_v07_12

%% Calculation of SPOT_DIFF
length_data = size(data_matrix, 1);
nonfail = 'passed';
cnt_fail = 0;
count_fail = 0;
[criteria_matrix,spot1LimitMAX,spot1LimitMIN,spot2LimitMAX,spot2LimitMIN] = get_criteriaParams(data_cell);
switch criterion_mode
    case 1 %Import by default
        if isempty(spot1LimitMAX) && isempty(spot2LimitMIN) && isempty(spot1LimitMIN) && isempty(spot2LimitMAX)
            errordlg ('---Programm kann die ben�tigten Daten f�r das gew�hlte Kriterium nicht finden !!! ---','Excel FILE Problem');
            error('---Programm nicht erfolgreich durchgef�hrt !!!--- Bitte neues Kriterium w�hlen!!! ');
        elseif data_matrix(:,10) == 0
            %importbydefault_Criterion==Spot_1_Import_by_default
            crit_spot_u=find(abs(data_matrix(:,8))>=spot1LimitMAX | abs(data_matrix(:,8))<=spot1LimitMIN, 1) - 1;
            % crit_spot_u=min(crit_spot_u1)-1;
            crit_spot=min(crit_spot_u);
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end
            
        elseif ~isempty(spot2LimitMAX) & ~isempty(spot2LimitMIN) & ~isempty(spot1LimitMIN) & ~isempty(spot1LimitMAX) & data_matrix(:,10)~= 0
            %importbydefault_Criterion==Spot_1_Spot_2_Import_by_default
            crit_spot_u1 = find(abs(data_matrix(:,8))>=spot1LimitMAX | abs(data_matrix(:,8))<=spot1LimitMIN);
            crit_spot_u2 = find(abs(data_matrix(:,10))>=spot2LimitMAX | abs(data_matrix(:,10))<=spot2LimitMIN);
            if isempty(crit_spot_u1) & isempty(crit_spot_u2)
                crit_spot = size(data_matrix, 1);
            elseif isempty(crit_spot_u1)
                crit_spot = min(crit_spot_u2) - 1;
            elseif isempty(crit_spot_u2)
                crit_spot = min(crit_spot_u1) - 1;
            else
                crit_spot_u = min(crit_spot_u2,crit_spot_u1) - 1;
                crit_spot = min(crit_spot_u);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end
            
        end
        %fill criteria matrix
        Value_table = 'Value';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr([num2str(spot1LimitMIN) '...' num2str(spot1LimitMAX)]);
        criteria_matrix(2,5) = cellstr([num2str(spot2LimitMIN) '...' num2str(spot2LimitMAX)]);
        
    case 2 % spots_1_delta
        if min_fail>3
            min_fail=3;
        end
        count_fail = 1;
        spot_delta_square1 = abs( data_matrix(1, 8) - data_matrix(2:end,8));
        avg = mean(spot_delta_square1(1:end));
        for z=2:length_data-2
            if min_fail == 1
                crit_spot= find(spot_delta_square1>((extraction_threshold+100)/100)*avg,1);
                nonfail = 'failed';
                cnt_fail = 1;
                break
            end
            if (spot_delta_square1(z)>((extraction_threshold+100)/100)*avg) && (spot_delta_square1(z+1)>((extraction_threshold+100)/100)*avg)
                count_fail = count_fail+1;
                if count_fail == min_fail
                    crit_spot = z;
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                    
                elseif z == length_data -2
                    crit_spot = z;
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                end
            elseif (z == length_data -2) && ((spot_delta_square1(z+1)>((extraction_threshold+100)/100)*avg))
                crit_spot = z;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            else
                crit_spot=z+2;
                cnt_fail = 0;
                count_fail=1;
            end
            
        end
        
        %             % Differenz aller Elemente zum 1. Wert
        %             spot_delta_square1 = abs( data_matrix(1, 8) - data_matrix(2:end,8));
        %            % X=find(spot_delta_square1>((extraction_threshold+100)/100)*mean(spot_delta_square1(1:end)));
        %             %if length(X) >= min_fail
        %              %  crit_spot = X(min_fail); %Mohamed: crit_spot = z-min_fail                       nonfail = 'failed';
        %               % nonfail = 'failed';
        %                %cnt_fail = 1;
        %
        %             %else
        %              %crit_spot=length_data;
        %               %cnt_fail= 0;
        %             %end
        %
        %            for z=2:length_data - 1
        %                 if (spot_delta_square1(z)>((extraction_threshold+100)/100)*mean(spot_delta_square1(1:end))) && ( mean(spot_delta_square1(1:end))  > (1e-17) )   %Alter Wert  > (1e-22) )   %Neuer Wert
        %                            %beim_sophie if (spot_delta_square1(z)>((extraction_threshold+100)/100)*mean(spot_delta_square1(2:end))) && ( mean(spot_delta_square1(2:end))  > (1e-17) )   %Alter Wert  > (1e-22) )   %Neuer Wert
        %                     count_fail = count_fail+1;
        %                     if count_fail == min_fail
        %                         crit_spot = z; %Mohamed: crit_spot = z-min_fail                       nonfail = 'failed';
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break
        %                     elseif z == length_data
        %                         crit_spot = z-1;
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break
        %                     end
        %                 else
        %                     crit_spot=z;
        %                     cnt_fail = 0; %count_fail
        %                 end
        %
        %            end
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
    case 3 %spots_2_delta
        % Differenz aller Elemente zum 1. Wert
        if min_fail>3
            min_fail=3;
        end
        count_fail = 1;
        spot_delta_square2 = abs( data_matrix(1, 10) - data_matrix(2:end,10));
        avg = mean(spot_delta_square2(1:end));
        for z=2:length_data-2
            if min_fail == 1
                crit_spot= find(spot_delta_square2>((extraction_threshold+100)/100)*avg,1);
                nonfail = 'failed';
                cnt_fail = 1;
                break
            end
            if (spot_delta_square2(z)>((extraction_threshold+100)/100)*avg) && (spot_delta_square2(z+1)>((extraction_threshold+100)/100)*avg)
                count_fail = count_fail+1;
                if count_fail == min_fail
                    crit_spot = z;
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                    
                elseif z == length_data -2
                    crit_spot = z;
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                end
            elseif (z == length_data -2) && ((spot_delta_square2(z+1)>((extraction_threshold+100)/100)*avg))
                crit_spot = z;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            else
                crit_spot=z;
                cnt_fail = 0;
                count_fail=1;
            end
            
        end
        %             spot_delta_square2 = abs(data_matrix(1, 10) - data_matrix(1:end, 10));
        %             for z=2:length_data
        %                 if (spot_delta_square2(z)>((extraction_threshold+100)/100)*mean(spot_delta_square2(2:end)) ) && ( mean(spot_delta_square2(2:end))  > (1e-17) )   %Alter Wert > (1e-22) )   %Neuer Wert
        %                     count_fail = count_fail+1;
        %                     if count_fail == min_fail
        %                         crit_spot = z-min_fail;
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break
        %                     elseif z == length_data
        %                         crit_spot = z-1;
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break
        %                     end
        %                 else
        %                     crit_spot=z;
        %                     count_fail = 0;
        %                 end
        %
        %             end
        
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
    case 4 % 2_spots Delta
        if min_fail>3
            min_fail=3;
        end
        count_fail=1;
        % Differenz zwischen Spot 1 und Spot 2
        spot12_diff = abs((data_matrix(:,10)) - abs(data_matrix(:,8)));
        % Differenz zwischen erstem Wert von Spot 1 und 2
        spot12_ref = abs(data_matrix(1,10) - abs(data_matrix(1,8)));
        th = (extraction_threshold/100)*spot12_ref;
        crit_spot = length_data;
        for z = 1:length_data -1 %-1
            if min_fail == 1
                crit_spot= (find(abs(spot12_diff - spot12_ref) > th ,1)) - 1;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            end
            
            if ((abs(spot12_diff(z) - spot12_ref) > th) && (abs(spot12_diff(z+1) - spot12_ref) > th)) %|| (((abs(spot12_diff(z) - spot12_ref))< th1) && (abs(spot12_diff(z+1) - spot12_ref) < th1))
                count_fail = count_fail+1;
                if count_fail == min_fail
                    crit_spot = z+1-min_fail;%z - min_fail
                    if crit_spot == 0
                        crit_spot = length_data - 1;
                    end
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                elseif z == length_data -1%-1
                    crit_spot = z - count_fail + 1 ;%-1
                    if crit_spot == 0
                        crit_spot = length_data - 1;
                    end
                    nonfail = 'failed';
                    cnt_fail = 1;
                    break
                    
                end
            else
                crit_spot=z;
                cnt_fail = 0;
                count_fail=1;
            end
        end
        
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
        
    case 5
        % spot1-Delta-Prevalue
        % Calculate difference between successive vector values
        if min_fail>3
            min_fail=3;
        end
        d1 = abs(diff(data_matrix(:, 8)));
        crit_spot = length_data;
        Abstand_vector= zeros(length_data+min_fail,1);
        for z=2:length_data-1
            th = ((extraction_threshold+100)/100)*data_matrix(z-1, 8); %Mohamed: th = ((extraction_threshold+100)/100)*d1(z-1
            v(z,:) = abs (data_matrix(z-1, 8)-th); %Mohamed
        end
        Delta_d=d1>v;
        Abstand_vector(Delta_d)=1;
        for i = 2: length_data - min_fail  %i =1;
            if min_fail==1 && Abstand_vector(i)==1 && Abstand_vector(i+1)==0
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            end
            if Abstand_vector(i) == Abstand_vector(i+1) && min_fail > 1
                Abstand_vector(i) = 0;
                %                    Abstand_vector(i+1)= 0;
            end
            if Abstand_vector(i) == Abstand_vector(i+ min_fail) && Abstand_vector(i) == 1
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            elseif (Abstand_vector(i) ~= Abstand_vector(i+ min_fail)) && (Abstand_vector(i) == Abstand_vector(length(Delta_d)-1))&& Abstand_vector(i) == 1
                crit_spot = i-1;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            else
                crit_spot = length_data;
                cnt_fail = 0;
            end
        end
        % %                 if d1(z)~= 0 && d1(z+1)==0 %Mohamed
        % %                    d1(z+1) = d1(z);        %Mohamed
        % %                 end
        %                 if d1(z)> v && min_fail > 1
        %                 count_fail = 1;
        %                    for i =1: length_data - z
        %                       if d1(z+i)==0 || d1(z+i) < v
        %                          count_fail = count_fail +1;
        %
        %                       else
        %                           break
        %                       end
        %                    end
        %                    if count_fail == min_fail
        %                        crit_spot = z;
        %                        nonfail = 'failed';
        %                        cnt_fail = 1;
        % %                        d1(z+min_fail) = 0;
        %                        break
        % %                    else
        % %                         crit_spot = length_data;
        % %                         cnt_fail = 0;
        % %                         break
        %                   end
        %                 elseif d1(z) > v && d1(z+1) > v  && min_fail ==1 %Mohamed: if d1(z) > th && d1(z+1) > th
        %                     crit_spot = z;
        %                     nonfail = 'failed';
        %                     cnt_fail = 1;
        %                     break
        %                 elseif d1(z) > v && min_fail ==1
        %                     crit_spot = z;
        %                     nonfail = 'failed';
        %                     cnt_fail = 1;
        %                     break
        %                 else              % �if z==length_data - 2
        %                     crit_spot = length_data;
        %                     cnt_fail = 0;
        %                 end
        %             end
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
    case 6
        
        % spot2-Delta-Prevalue
        if min_fail>3
            min_fail=3;
        end
        d2 = abs(diff(data_matrix(:, 10)));
        crit_spot = length_data;
        Abstand_vector= zeros(length_data+min_fail,1);
        for z=2:length_data
            th = ((extraction_threshold+100)/100)*data_matrix(z-1, 10); %Mohamed: th = ((extraction_threshold+100)/100)*d1(z-1
            v(z-1,:) = abs (data_matrix(z-1, 10)-th); %Mohamed
        end
        Delta_d2=d2>v;
        Abstand_vector(Delta_d2)=1;
        for i = 1: length_data - min_fail
            if min_fail==1 && Abstand_vector(i)==1 && Abstand_vector(i+1)==0
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            end
            if Abstand_vector(i) == Abstand_vector(i+1) && min_fail > 1
                Abstand_vector(i) = 0;
                Abstand_vector(i+1)= 0;
            end
            if Abstand_vector(i) == Abstand_vector(i+ min_fail) && Abstand_vector(i) == 1
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            else
                crit_spot = length_data;
                cnt_fail = 0;
            end
        end
        
        
        %             d2 = abs(diff(data_matrix(:, 10)));
        %             crit_spot = length_data;
        %             for z = 2:length_data-2
        %                 %th = ((extraction_threshold+100)/100)*d2(z-1);
        %                 th = ((extraction_threshold+100)/100)*data_matrix(z-1,10); %Mohamed: th = ((extraction_threshold+100)/100)*d2(z-1);
        %                      v = abs (data_matrix(z-1, 10)-th);   %Mohamed
        %                  if d2(z)~= 0 && d2(z+1)==0   %Mohamed
        %                    d2(z+1) = d2(z);           %Mohamed
        %                 end
        %                 if  d2(z) > v && d2(z+1) > v   %Mohamed:  if d2(z) > th && d2(z+1) > th
        %                         crit_spot = z;
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break;
        %                 end
        %             end
        
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
        
    case 7 % 2-spots-Delta-Prevalue
        % Differenz zwischen Spot 1 und Spot 2
        if min_fail>3
            min_fail=3;
        end
        spot12_diff = abs((data_matrix(:,10)) - (data_matrix(:,8)));
        d12 = abs(diff(spot12_diff)); %abs
        crit_spot = length_data;
        Abstand_vector = zeros(length_data+min_fail,1);
        for z = 2:length_data-1
            th = ((extraction_threshold+100)/100)*spot12_diff(z);
            v(z,:)= abs (spot12_diff(z)-th);
        end
        
        Delta_d = d12>v;
        Abstand_vector(Delta_d)=1;
        for i=1:length_data - min_fail
            if Abstand_vector(i) == Abstand_vector(i+min_fail) && Abstand_vector(i)==1
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            elseif (Abstand_vector(i)==1) && (~any(Abstand_vector(i+1:end))== 1) && min_fail==1
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            elseif (Abstand_vector(i)==1) && (~any(Abstand_vector(i+1:end))== 1) && min_fail>1 && (Abstand_vector(i-1)==0)
                crit_spot = i;
                nonfail = 'failed';
                cnt_fail = 1;
                break
            else
                crit_spot = length_data;
                cnt_fail = 0;
            end
            
        end
        
        %             for z = 2:length_data-2 % Mohamed.    vorher war :z = 1:length_data-2
        %                 th = ((extraction_threshold+100)/100)*spot12_diff(z);
        %                 v= abs (spot12_diff(z)-th); %von Mohamed hinzugef�gt
        %                  %if d12(z) > th && d12(z+1) > th
        %                 if d12(z) > v   %Mohamed.    vorher war %if d12(z) > th && d12(z+1) > th
        %                         crit_spot = z;
        %                         nonfail = 'failed';
        %                         cnt_fail = 1;
        %                         break;
        %                 end
        %             end
        
        %fill criteria matrix
        Value_table = 'Extraction Threshold';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
        
        
    case 8 % voltage_peak (Cap)
        maximum=max(abs(data_matrix(:,2)));
        crit_spot=find(abs(data_matrix(:,2)==maximum));
        if isempty(crit_spot)
            crit_spot=size(data_matrix, 1);
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'abs(Maximum value)';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(maximum));
        criteria_matrix(2,5) = cellstr(num2str(maximum));
        
        
    case 9 % Spot_1-Min Limit
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot = find(abs(data_matrix(:,8))<=Min_Max_Limit, 1) - 1;
        if isempty(crit_spot)
            crit_spot=size(data_matrix, 1);
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
    case 10 % Spot_1-Max Limit
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot = find(abs(data_matrix(:,8))>=Min_Max_Limit, 1) - 1;
        if isempty(crit_spot)
            crit_spot=size(data_matrix, 1);
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
        
    case 11 % %Spot_2-Min Limit%
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot = find(abs(data_matrix(:,10))<=Min_Max_Limit, 1) - 1;
        if isempty(crit_spot)
            crit_spot = size(data_matrix, 1);
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
        
    case 12 %Spot_2-Max Limit
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot=find(abs(data_matrix(:,10))>=Min_Max_Limit, 1) - 1;
        if isempty(crit_spot)
            crit_spot=size(data_matrix, 1);
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
        
    case 13 % %Spot_12-Min Limit%
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot_1 = find(abs(data_matrix(:,8))<=Min_Max_Limit);
        crit_spot_2 = find(abs(data_matrix(:,10))<=Min_Max_Limit);
        %Mohamed
        if isempty(crit_spot_1)
            crit_spot = min(crit_spot_2) - 1;
        else
            if isempty (crit_spot_2)
                crit_spot = min(crit_spot_1) - 1;
            else
                crit_spot = min(min(crit_spot_1), min(crit_spot_2)) - 1;
            end
        end
        %Mohamed
        %Fr�her: crit_spot = min(crit_spot_1, crit_spot_2) - 1;
        if isempty(crit_spot)
            crit_spot=size(data_matrix, 1);s
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
        
    case 14 %Spot_12-Max Limit
        if isnan (Min_Max_Limit)
            msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
        end
        crit_spot_1 = find(abs(data_matrix(:,8))>=Min_Max_Limit,1);% Fr�her: crit_spot_1 = find(abs(data_matrix(:,8))>=Min_Max_Limit)
        crit_spot_2 = find(abs(data_matrix(:,10))>=Min_Max_Limit,1);%Fr�her: find(abs(data_matrix(:,10))>=Min_Max_Limit)
        %Mohamed
        if isempty(crit_spot_1)
            crit_spot = min(crit_spot_2) - 1;
        else
            if isempty (crit_spot_2)
                crit_spot = min(crit_spot_1) - 1;
            else
                crit_spot = min(crit_spot_1, crit_spot_2) - 1;
            end
        end
        %Mohamed
        %crit_spot = min(crit_spot_1, crit_spot_2) - 1;
        if isempty(crit_spot)
            crit_spot = length_data;
        end
        if crit_spot ~= length_data
            nonfail = 'failed';
            cnt_fail = 1;
        end
        
        %fill criteria matrix
        Value_table = 'Min_Max Limit';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
    case 15              % spot1-Prevalue_variation
        data_norm = data_matrix(:, 8)/data_matrix(1, 8);
        d2 = abs(diff(data_norm));
        crit_spot = length_data;
        for z = 2:length_data-2
            th = Max_Variation;
            second_pt = data_norm(z+1)-data_norm(z-1);
            if  d2(z) > th && abs(second_pt) > th
                crit_spot = z;
                nonfail = 'failed';
                cnt_fail = 1;
                break;
            end
        end
        %fill criteria matrix
        Value_table = 'Limit Max Variation';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
    case 16              % spot2-Prevalue_variation
        data_norm = data_matrix(:, 10)/data_matrix(1, 10);
        d2 = abs(diff(data_norm));
        crit_spot = length_data;
        for z = 2:length_data-2
            th = Max_Variation;
            second_pt = data_norm(z+1)-data_norm(z-1);
            if  d2(z) > th && abs(second_pt) > th
                crit_spot = z;
                nonfail = 'failed';
                cnt_fail = 1;
                break;
            end
        end
        %fill criteria matrix
        Value_table = 'Limit Max Variation';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
        
    case 17              % 2spots-Prevalue_variation
        data_norm1 = data_matrix(:, 10)/data_matrix(1, 10);
        d1 = abs(diff(data_norm1));
        data_norm2 = data_matrix(:, 8)/data_matrix(1, 8);
        d2 = abs(diff(data_norm2));
        crit_spot = length_data;
        for z = 2:length_data-1
            th = Max_Variation;
            second_pt1 = data_norm1(z+1)-data_norm1(z-1);
            second_pt2 = data_norm2(z+1)-data_norm2(z-1);
            if  (d1(z) > th && abs(second_pt1) > th)|| (d2(z) > th && abs(second_pt2) > th)
                crit_spot = z;
                nonfail = 'failed';
                cnt_fail = 1;
                break;
            end
        end
        %fill criteria matrix
        Value_table = 'Limit Max Variation';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(num2str(threshold));
        criteria_matrix(2,5) = cellstr(num2str(threshold));
    case 18
        crit_spot = length_data;
        Value_table = 'NO_Spots';
        criteria_matrix(1,4) = criteria_table(criterion_mode);
        criteria_matrix(2,4) = criteria_table(criterion_mode);
        criteria_matrix(1,5) = cellstr(threshold);
        criteria_matrix(2,5) = cellstr(threshold);
end

V_HBM_fail = 'NaN';
hbm_at_it2 = 0;
if crit_spot == 0
    crit_spot = 1;
end

if hbm_mode
    hbm_at_it2 = data_matrix(crit_spot, 1);
    if size(data_matrix, 1) > crit_spot
        V_HBM_fail = data_matrix(crit_spot + 1, 1);
    end
end

It2=data_matrix(crit_spot,3);
Vt2=data_matrix(crit_spot,2);
fail_pulse = data_matrix(crit_spot, 1);

%boolContfail=true(contact_fail>abs(It2));
%boolStatistik=true(Statistik);
%if isempty(boolStatistik)
%if isempty(boolContfail)
%    strContfail='no contact issue';
%else
%    strContfail='CONTACT FAIL DETECTED';
%end

boolContfail = false;

for z = length_data
    if abs(data_matrix(z,3)) > abs(contact_fail)
        boolContfail = true;
        break;
    end
end

if boolContfail
    strContfail = 'pass';
else
    strContfail = 'failed';
end

%check if one or two spots used
if ~spots_2
    criteria_matrix(2,:) = [];
elseif criterion_mode == 18
    criteria_matrix = [];
end
end




%new criteria
%
%     X_norm = XCopy(1:end)/XCopy(1);
% diif_norm = abs(diff(X_norm));
% mean_value = mean(diif_norm);
% fail = [];
% j=0;
% for i=1:length(diif_norm)
%     if diif_norm(i)>3*mean_value
%         j=j+1;
%         fail(j,1) = i+1;
%         fail(j,2) = diif_norm(i);
%     end
% end


