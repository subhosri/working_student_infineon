function [It2, Vt2, crit_spot, strContfail, nonfail, hbm_at_it2, V_HBM_fail, cnt_fail,fail_pulse] = Criteria_GTS(data_matrix, criterion_mode, extraction_threshold, Min_Max_Limit, contact_fail, parameters_num, c, parameters, failure_tests_spots, outcome, hbm_mode, min_fail, Max_Variation, criteria_matrix)
    % This function chooses the right exraction criterion given by the user
    % and calculates the critical spot.
    %
    % Input:
    % data_matrix - contains all measurement data from TLP_data.csv
    % criterion_mode - chosen criterium
    % out - whole content of TLP_data.csv
    % extraction_threshold
    % Min_Max_Limit - threshold given by user, used in case of Min Limit or Max Limit
    % contact_fail
    % Statistik
    % hbm_mode
     %augerufen im TLP_analyse_vTG_v07_12
    
    %% Calculation of SPOT_DIFF
    length_data = size(data_matrix, 1);
    nonfail = 'passed';
    cnt_fail = 0;
    count_fail = 0;

    switch criterion_mode
        case 1 %Import by default
            % Possible Failure Parameters of "Failure Test" in sheet
            % "Parameters"
            default_criteria = {'% Increase Enabled?', '% Decrease Enabled?', 'Delta Increase Enabled?',...
                'Delta Decrease Enabled?', 'Greater Than Enabled?', 'Less Than Enabled?'};

            % get criteria which were used during measurement
            criteria_applied = zeros(6, length(failure_tests_spots));
            for k = 1:length(failure_tests_spots)
                for j = 1:6
                    criteria_applied(j, k) = parameters_num{ismember(parameters(:,c(k)), default_criteria{j}), c(k)+1};
                end
            end
            
            % no cirteria used -> error
            if sum(sum(criteria_applied)) == 0
%                warndlg('---Programm kann die ben�tigten Daten f�r das gew�hlte Kriterium nicht finden !!! --- Execution will continue.');
               nonfail = 'Not Available';
               cnt_fail = length_data;
               crit_spot = length_data;
            else
                % get crit_spot saved in sheet "Data" in column "outcome"
                crit_spot = min(cellfun('length',outcome))-1;
                crit_spot_temp = 0;
                for u = 1:length(failure_tests_spots)
                    for k = 1:size(outcome{u}, 1)
                        if strcmp(outcome{u}(k),'Failed')
                            crit_spot_temp = k - 1;
                             break
                        end
                    end
                    if crit_spot_temp ~= 0 && crit_spot_temp < crit_spot 
                       crit_spot = crit_spot_temp ;
                    end  
                end

                if crit_spot ~= length_data
                      nonfail = 'failed';
                      cnt_fail = 1;
                end

            end

        case 2 % spots_1_delta
            % Differenz aller Elemente zum 1. Wert
            spot_delta_square1 = abs(data_matrix(1, 8) - data_matrix(1:end, 8));
            for z=2:length_data
                if (spot_delta_square1(z)>((extraction_threshold+100)/100)*mean(spot_delta_square1(2:end))) && ( mean(spot_delta_square1(2:end))  > (1e-17) )   %Alter Wert  > (1e-22) )   %Neuer Wert
                    count_fail = count_fail+1;
                    if count_fail == min_fail 
                        crit_spot = z-min_fail;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    elseif z == length_data
                        crit_spot = z-1;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    end
                else
                    crit_spot=z;
                    count_fail = 0;
                end
            end

        case 3 %spots_2_delta
            % Differenz aller Elemente zum 1. Wert
            spot_delta_square2 = abs(data_matrix(1, 10) - data_matrix(1:end, 10));
            for z=2:length_data
                if (spot_delta_square2(z)>((extraction_threshold+100)/100)*mean(spot_delta_square2(2:end)) ) && ( mean(spot_delta_square2(2:end))  > (1e-17) )   %Alter Wert > (1e-22) )   %Neuer Wert
                    count_fail = count_fail+1;
                    if count_fail == min_fail 
                        crit_spot = z-min_fail;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    elseif z == length_data
                        crit_spot = z-1;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    end       
                else
                    crit_spot=z;
                    count_fail = 0;
                end
            end

        case 4 % 2_spots Delta
            % Differenz zwischen Spot 1 und Spot 2
            spot12_diff = abs((data_matrix(:,10)) - (data_matrix(:,8)));
            % Differenz zwischen erstem Wert von Spot 1 und 2
            spot12_ref = abs(data_matrix(1,10) - data_matrix(1,8));
            th = (extraction_threshold/100)*spot12_ref;
            crit_spot = length_data;
            for z = 1:length_data
                if abs(spot12_diff(z) - spot12_ref) > th
                    count_fail = count_fail+1;
                    if count_fail == min_fail  
                        crit_spot = z - min_fail;
                        if crit_spot == 0
                            crit_spot = length_data - 1;
                        end
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    elseif z == length_data
                        crit_spot = z - 1;
                        if crit_spot == 0
                            crit_spot = length_data - 1;
                        end
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break
                    end
                end
            end
            
        case 5
            % spot1-Delta-Prevalue
            % Calculate difference between successive vector values            
            d1 = abs(diff(data_matrix(:, 8)));
            crit_spot = length_data;
            for z=2:length_data-2
                th = ((extraction_threshold+100)/100)*d1(z-1);
                if d1(z) > th && d1(z+1) > th
                    crit_spot = z;
                    nonfail = 'failed';
                    cnt_fail = 1;
                     break;                          
                end
            end
            
        case 6
            % spot2-Delta-Prevalue
            d2 = abs(diff(data_matrix(:, 10)));
            crit_spot = length_data;
            for z = 2:length_data-2
                th = ((extraction_threshold+100)/100)*d2(z-1);
                if  d2(z) > th && d2(z+1) > th 
                        crit_spot = z;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break;                          
                end
            end
            
         case 7 % 2-spots-Delta-Prevalue
            % Differenz zwischen Spot 1 und Spot 2
            spot12_diff = abs((data_matrix(:,10)) - (data_matrix(:,8)));
            d12 = diff(spot12_diff);
            crit_spot = length_data;
            for z = 1:length_data-2
                th = ((extraction_threshold+100)/100)*spot12_diff(z);
                if d12(z) > th && d12(z+1) > th
                        crit_spot = z;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break;                          
                end
            end


        case 8 % voltage_peak (Cap)
            maximum=max(abs(data_matrix(:,2)));
            criteria_matrix(:,5) = cellstr(num2str(maximum));
            crit_spot=find(abs(data_matrix(:,2)==maximum));
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 9 % Spot_1-Min Limit
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot = find(abs(data_matrix(:,8))<=Min_Max_Limit, 1) - 1;
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 10 % Spot_1-Max Limit
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot = find(abs(data_matrix(:,8))>=Min_Max_Limit, 1) - 1;
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 11 % %Spot_2-Min Limit%
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot = find(abs(data_matrix(:,10))<=Min_Max_Limit, 1) - 1;
            if isempty(crit_spot)
                crit_spot = size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 12 %Spot_2-Max Limit
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot=find(abs(data_matrix(:,10))>=Min_Max_Limit, 1) - 1;
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 13 % %Spot_12-Min Limit%
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot_1 = find(abs(data_matrix(:,8))<=Min_Max_Limit);
            crit_spot_2 = find(abs(data_matrix(:,10))<=Min_Max_Limit);
            crit_spot = min(crit_spot_1, crit_spot_2) - 1;
            if isempty(crit_spot)
                crit_spot=size(data_matrix, 1);
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end

        case 14 %Spot_12-Max Limit
            if isnan (Min_Max_Limit)
                msgbox ({'Please insert Min/Max Limit Value', 'in the corresponding BOX'},'Error','error')
            end
            crit_spot_1 = find(abs(data_matrix(:,8))>=Min_Max_Limit);
            crit_spot_2 = find(abs(data_matrix(:,10))>=Min_Max_Limit);
            crit_spot = min(crit_spot_1, crit_spot_2) - 1;
            if isempty(crit_spot)
                crit_spot = length_data;
            end
            if crit_spot ~= length_data
                nonfail = 'failed';
                cnt_fail = 1;
            end
            
        case 15              % spot1-Prevalue_variation
            data_norm = data_matrix(:, 8)/data_matrix(1, 8);
            d2 = abs(diff(data_norm));
            crit_spot = length_data;
            for z = 2:length_data-1
                th = Max_Variation;
                second_pt = data_norm(z+1)-data_norm(z-1);
                if  d2(z) > th && abs(second_pt) > th 
                        crit_spot = z;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break;                          
                end
            end
            
        case 16              % spot2-Prevalue_variation
            data_norm = data_matrix(:, 10)/data_matrix(1, 10);
            d2 = abs(diff(data_norm));
            crit_spot = length_data;
            for z = 2:length_data-1
                th = Max_Variation;
                second_pt = data_norm(z+1)-data_norm(z-1);
                if  d2(z) > th && abs(second_pt) > th 
                        crit_spot = z;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break;                          
                end
            end
            
            case 17              % 2spots-Prevalue_variation
            data_norm1 = data_matrix(:, 10)/data_matrix(1, 10);
            d1 = abs(diff(data_norm1));
            data_norm2 = data_matrix(:, 8)/data_matrix(1, 8);
            d2 = abs(diff(data_norm2));
            crit_spot = length_data;
            for z = 2:length_data-1
                th = Max_Variation;
                second_pt1 = data_norm1(z+1)-data_norm1(z-1);
                second_pt2 = data_norm2(z+1)-data_norm2(z-1);
                if  (d1(z) > th && abs(second_pt1) > th)|| (d2(z) > th && abs(second_pt2) > th) 
                        crit_spot = z;
                        nonfail = 'failed';
                        cnt_fail = 1;
                        break;                          
                end
            end
            case 18
            crit_spot = length_data;
            
    end

    V_HBM_fail = 'NaN';
    hbm_at_it2 = 0;
    if crit_spot == 0
        crit_spot = 1;
    end
    
    if hbm_mode
        hbm_at_it2 = data_matrix(crit_spot, 1);
        if size(data_matrix, 1) > crit_spot
            V_HBM_fail = data_matrix(crit_spot + 1, 1);
        end
    end

    It2=data_matrix(crit_spot,3);
    Vt2=data_matrix(crit_spot,2);
    fail_pulse = data_matrix(crit_spot, 1);
    boolContfail=true(contact_fail>abs(It2));
    %boolStatistik=true(Statistik);
    %if isempty(boolStatistik)
    if isempty(boolContfail)
        strContfail='pass';
    else
        strContfail='failed';
    end

    
