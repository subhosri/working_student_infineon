function [P,I,V, Spot1,Spot2,V_post_sweep,I_post_sweep,I_pre_sweep,V_pre_sweep, measure_params,spotLimit,data_list] = Data_generieren(Data)
%Data_generieren is
%   Detailed explanation goes here
%   Grabs data from the given paths and returnes the mesurments in the
%   corresponding variables (I,V,Sport1,...)
%   mesure_params holds all the paramter of the mesurments like the module
%   name and the validity of the dataset
Nb_Path= size(Data,1);
k=1;
%% Generation of the measrue_params table
varNames = {'Module Name','Device Name','Device Number','Die/Coordinate','PinVsGnd',...
    'Bias','Lot Name', 'Wafer Number','IT2','VT2','V_Hold', 'VatI', 'IatV', 'Val_R_on1', 'Val_R_on2' , 'Test Chip', 'Val_spot1', 'Val_spot2', 'PATH',  'Bias Pin','Spot 1','Spot 2','Nr_Spot'...
    'Validity_pre','Validity_TLP','Validity_post'};
varTypes = {'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string','string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'int8', 'logical', 'logical', 'logical'};
%varTypes = {'string, "string","string','string','string','string','string','string','string','string','string','string','string','string','string','string','string','int8','logical','logical','logical'};
sz = [1 26];
measure_params = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

% Fild Spot limit:
varNames = {'spot1LimitMAX','spot1LimitMIN','spot2LimitMAX','spot2LimitMIN'};
sz = [1 4];
varTypes = {'double','double','double','double'};
spotLimit = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

%% Data and parameter extraction
for i=1:Nb_Path
    [~,~,Path_nr_TLP] = dirr(char(Data{ i , 1 }),'\TLP_data.csv\>','name');
    Path_nr_pre_sweep = strcat(erase(Path_nr_TLP,'TLP_data.csv'), 'pre_sweep.csv');
    Path_nr_post_sweep = strcat(erase(Path_nr_TLP,'TLP_data.csv'), 'post_sweep.csv');
    
    for j=1:size(Path_nr_pre_sweep,2)
        measure_params{k,{'PATH'}} = string(Path_nr_TLP{j});
        
        measure_params{k,'Nr_Spot'}=0;
        
        if isfile(Path_nr_TLP{j})
            data_cell = readcell(Path_nr_TLP{j},'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            % Extract the parameter of the experiment and put them in a cell
            % array measure_params:
            
            % Module Name
            [x,y] = find(strcmp(data_cell,'Module'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Module Name'} = string(data_cell{x,y+1});
            else
                measure_params{k,'Module Name'} = strcat("Module_Name_", 'Empty');
            end
            
            % Device Name
            [x,y] = find(strcmp(data_cell,'Device Name'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Device Name'} = string(data_cell{x,y+1});
            else
                measure_params{k,'Device Name'} = strcat("Device_Name_", 'Empty');
            end
            
            % Test Chip
            
             [x,y] = find(strcmp(data_cell,'Test Chip'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Test Chip'} = strcat('Test chip_',string(data_cell{x,y+1}));
            else
                measure_params{k,'Test Chip'} = strcat("Test chip_", 'Empty');
            end
            
            % Device Number
            [x,y] = find(strcmp(data_cell,'Device Number'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Device Number'} = strcat("Dev_",string(data_cell{x,y+1}));
                DeviceName = data_cell{x,y+1};
            else
                measure_params{k,'Device Number'} = strcat("Dev_Number_", 'Empty');
                %DeviceName = data_cell{x,y+1};
            end
            
            % Die/Coordinater
            [x,y] = find(strcmp(data_cell,'Die'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Die/Coordinate'} = string(data_cell{x,y+1});
                Die = data_cell{x,y+1};
            else
                measure_params{k,'Die/Coordinate'} = strcat("Die_", 'Empty');
            end
             
            % PinVSGND
            [x,y] = find(strcmp(data_cell,'Pulse Pin'));
            [x1,y1] = find(strcmp(data_cell,'Gnd Pin'));
            
            if not(isempty(x) | ismissing(string(data_cell(x,y+1)))) & not(isempty(x1) | ismissing(string(data_cell(x1,y1+1))))
                Pulse = string(data_cell{x,y+1});
                Gnd = string(data_cell{x1,y1+1});
                measure_params{k,'PinVsGnd'} = Pulse + "_Vs_" + Gnd;
            else
                measure_params{k,'PinVsGnd'} = "";
            end
            
            % BIAS
            [x,y] = find(strcmp(data_cell,'Bias Pin'));
            if not(isempty(x) | ismissing(string(data_cell{x,y+1})))
                Bias_Pin = data_cell{x,y+1};
                if ~isempty(Bias_Pin)
                    measure_params{k,'Bias Pin'} = string(Bias_Pin); % I dont know if the Bias Pin can even be filled here
                else
                    measure_params{k,'Bias Pin'} = "Bias";
                    measure_params{k,'Bias'} = "";
                    
                end
            end
            
            % Lot
            [x,y] = find(strcmp(data_cell,'Lot'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Lot Name'} = string(data_cell{x,y+1});
            end
            
            % Wafer
            [x,y] = find(strcmp(data_cell,'Wafer'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Wafer Number'} = string(data_cell{x,y+1});
            else
                measure_params{k,'Wafer Number'} = "";
            end
            
            % Spot 1
            [x,y] = find(strcmp(data_cell,'Spot1 Value'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Spot 1'} = string(data_cell{x,y+1});
            else
                measure_params{k,'Spot 1'} = "";
            end

            [x,y] = find(strcmp(data_cell,'Spot1 Value'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Val_spot1'} = strcat('Spot1_', string(data_cell{x,y+1}));
            else
                measure_params{k,'Val_spot1'} = "";
            end
            
            
            % Spot 2
            [x,y] = find(strcmp(data_cell,'Spot2 Value'));
            if not(isempty(x) | ismissing(data_cell{x,y+1}))
                measure_params{k,'Spot 2'} = string(data_cell{x,y+1});
            else
                measure_params{k,'Spot 2'} = "";
            end

            %[x,y] = find(strcmp(data_cell,'Spot2 Value'));
%               for i = 1:size(app.measure_params,1)
%                             Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
%                             data_cell = readcell(Path_txt,'Delimiter',',');
%                             [xIn,~] = find(strcmp(data_cell,'Index'));
%                             data_matrix = cell2mat(data_cell(xIn+1:end,:));
%                             data_matrix = data_matrix(:,2:end);
%                
%               end
              if not(isempty(data_matrix))
                  if max(data_matrix(:,9)) == 0 && min(data_matrix(:,9)) == 0
                      measure_params{k,'Val_spot2'} = "Spot2 values does not exist in TLP data";

                  else
                      [x,y] = find(strcmp(data_cell,'Spot2 Value'));
                      if not(isempty(x) | ismissing(data_cell{x,y+1}))
                          measure_params{k,'Val_spot2'} = strcat("Spot2_", string(data_cell{x,y+1}));
                      else
                          measure_params{k,'Val_spot2'} = "";
                      end
                  end
              else
                  msg = ['TLP_data.csv for ', num2str(DeviceName), ' ', '&', ' ',Die, ' is empty'];
                  warndlg(msg ,'File Error');
              end
            
            % Spot1 Limit Max
            [x,y] = find(strcmp(data_cell,'Spot1 Limit Max'));
            if not(isempty(data_cell{x,y}))
                spotLimit{k,'spot1LimitMAX'} = data_cell{x,y+1};
            end
            
            % Spot1 Limit Min
            [x,y] = find(strcmp(data_cell,'Spot1 Limit Min'));
            if not(isempty(data_cell{x,y}))
                spotLimit{k,'spot1LimitMIN'} = data_cell{x,y+1};
            end
            
            % Spot2 Limit Max
            [x,y] = find(strcmp(data_cell,'Spot2 Limit Max'));
            if not(isempty(data_cell{x,y}))
                spotLimit{k,'spot2LimitMAX'} = data_cell{x,y+1};
            end
            
            % Spot2 Limit Min
            [x,y] = find(strcmp(data_cell,'Spot2 Limit Min'));
            if not(isempty(data_cell{x,y}))
                spotLimit{k,'spot2LimitMIN'} = data_cell{x,y+1};
            end

            
            
            [xIn,~] = find(strcmp(data_cell,'Index'));
            if size(data_cell,1)>xIn
                measure_params{k,'Validity_TLP'} = true;
                % Generate the Datamatrix
                data_matrix = cell2mat(data_cell(xIn+1:end,:));
                data_matrix = data_matrix(:,2:end);
                
                data_list{k} = data_matrix;
                I{:,k} = data_matrix(:,3);
                V{:,k}= data_matrix(:,2);
                P{:,k}= data_matrix(:,1);
                

                
                Spot1{:,k} = abs(data_matrix(:,8));
                Spot2{:,k} = abs(data_matrix(:,10));
                if Spot1{1,k} ~= 0
                    measure_params{k,'Nr_Spot'}=measure_params{k,'Nr_Spot'}+1;
                end
                
                if Spot1{1,k} ~= 0
                    measure_params{k,'Nr_Spot'}=measure_params{k,'Nr_Spot'}+1;
                end
            else
                I{:,k} = [];
                V{:,k}= [];
                P{:,k}= []
                Spot1{:,k} = [];
                Spot2{:,k} = [];
                data_list{k} = [];
            end
        end
        
        if isfile(Path_nr_pre_sweep{j})
            data_matrix_pre_sweep = csvread(Path_nr_pre_sweep{j}, 1, 0);
            
            I_pre_sweep{:,k}= data_matrix_pre_sweep(:,2);
            V_pre_sweep{:,k} = data_matrix_pre_sweep(:,1);
            measure_params{k,'Validity_pre'} = true;
        else
            I_pre_sweep{:,k}= [];
            V_pre_sweep{:,k} = [];
        end
        
        if isfile(Path_nr_post_sweep{j})
            data_matrix_post_sweep = csvread(Path_nr_post_sweep{j}, 1, 0);
            I_post_sweep{:,k} = data_matrix_post_sweep(:,2);
            V_post_sweep{:,k} = data_matrix_post_sweep(:,1);
            measure_params{k,'Validity_post'} = true;
        else
            I_post_sweep{:,k} = [];
            V_post_sweep{:,k} = [];
        end
        k=k+1;
    end
end
measure_params{:,'Bias'} = fillmissing(measure_params{:,'Bias'},'constant',"")
measure_params{:,'Bias Pin'} = fillmissing(measure_params{:,'Bias Pin'},'constant',"");
measure_params{:,'IT2'} = fillmissing(measure_params{:,'IT2'},'constant',"");
measure_params{:,'VT2'} = fillmissing(measure_params{:,'VT2'},'constant',"");
%measure_params{:,'VatI'} = fillmissing(measure_params{:,'VatI'},'constant', 'missing');
%measure_params{:,'IatV'} = fillmissing(measure_params{:,'IatV'},'constant', 'missing');
end