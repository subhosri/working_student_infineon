function GTS_analyse_vTG_v07_12(~,~,fatherPath,pathNr,PathName_save, report_path, extraction_threshold,contact_fail,criterion_mode,IforV,VforI,VHBMlevel,imin,imax,umin,umax,Min_Max_Limit, Wafermap_Checkbox, sPath, hbm_mode, snapback_values, Vbreakdown_checkbox,Option_Vbreakdown, save_pngs, summary, path_name, first_run, min_fail,test_journal, Max_Variation, copy_report, report_name)

 try
    %% Remove backslash and create cells with path slices
    % pathstrc: structure with the fields
    %           - path: all paths to HBM_data.csv
    %           - pathfields: all paths seperated 

    for i=1:size(sPath,2)
        if ~isempty(strfind(sPath{i},'~$'))
            sPath{i}=[]; 
        end
    end
    
    sPath=sPath(~cellfun('isempty', sPath));
    
    [pathstrc, ~] = path2strc(sPath);
    %% FIRST_FOLDER = pathstrc(1,n).pathfields{1,length(pathstrc(1,n).pathfields)-1};
    
    PATH_NR = ['Path_' int2str(pathNr)];
    TODATE = datestr(datetime('today'));


%%%%%%%%%%%%%%%%%%%%%%to change
    try
        ALLSAMP = cellfun(@(x)regexp(x, '\X.*Y(.?)\d', 'match'), sPath);
    catch
        ALLSAMP =  sPath;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%to be changed
 %% Read Test job information
 
    %read the test job information of the first Path and then compare it to
    %the others
    
    %Process bar
    h = mywaitbar(1,'Preparing to process'); 
    
    file_info=sPath{1};
    [~, parameters, parameters_num] = xlsread(file_info, 1);
    job_information = find(~cellfun(@isempty, parameters(:, 4)));
    
    % check if empty
    if length(job_information) == 1
        error_dlg = errordlg(['Excel file is corrupted: ' file_info]);
        waitfor(error_dlg);
        close (h);
        return;
    else
        for n=1:length(job_information)-1
            TestJobInfo{n,1} = parameters_num{n+1,4}(1:end-1);
            if ~isempty(parameters_num{n+1, 5})&& ~sum(isnan(parameters_num{n+1, 5}))
                %[device_prop_1.(parameters{n,4}(1:end-1))] = parameters_num{n,5};
                TestJobInfo{n,2}= parameters_num{n+1,5}; %create a structure with "job information"; use "matlab.lang.makeValidName" if error with name
            else
                TestJobInfo{n,2} = '';
            end
        end
    end
 
    for k = 2:length(sPath) % analyse all xlsx.-files in that path    
        file_info=sPath{k};
        [~, parameters_2, parameters_num_2] = xlsread(file_info, 1);
        % get contained data in "Job Information"
        job_information_2 = parameters_num_2(find(~cellfun(@isempty, parameters(:, 4))),5);
        job_information_2 = job_information_2(2:end); 
        %check if empty
        if prod((cellfun(@(x)any(isnan(x)),job_information_2,'UniformOutput',true)))
                error_dlg = errordlg(['Excel file is corrupted: ' file_info]);
                waitfor(error_dlg);
                close (h);
        return;
        end
        job_information_2(cellfun(@(x)any(isnan(x)),job_information_2,'UniformOutput',true))=[];  %remove NANs 
        % Find the intersection
        [C,ia,ib] = intersect(job_information_2, TestJobInfo, 'stable');
        % Remove from A the intersection of A and B
        TestJobInfo_temp = [];
        for i = 1:length(C)
            [row_element, ~] = find(ismember(TestJobInfo, C(i)));
            TestJobInfo_temp = [TestJobInfo_temp ; TestJobInfo(row_element,:)];
        end
        TestJobInfo = TestJobInfo_temp;
    end
        
        % get row indices which contain data in "Job Information"
        %job_information = find(~cellfun(@isempty, parameters(:, 4)));
        [row_technology, col_technology] = find(ismember(TestJobInfo, 'Technology'));
        [row_product, col_product] = find(ismember(TestJobInfo, 'Product'));
        [row_revision, col_revision] = find(ismember(TestJobInfo, 'Revision'));
        [row_operator, col_operator] = find(ismember(TestJobInfo, 'Operator'));

    
    if ~isempty(row_technology)
        Lot = TestJobInfo{row_technology, col_technology+1};
    else
        Lot = 'NONE';
    end

    if ~isempty(row_operator)
        Operator = TestJobInfo{row_operator, col_operator+1};
    else
        Operator = 'NONE';
    end
    
    if ~isempty(row_product)
        DeviceName = TestJobInfo{row_product, col_product+1};
    else
        DeviceName = 'NONE';
    end

    if ~isempty(row_revision)
        Macro = TestJobInfo{row_revision, col_revision+1};
    else
        Macro = 'NONE';
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%to be changed
    Chip=Macro;
    Technology=Lot;
    wafer_number='';
    Pin='';
    Reticle='';
    pulsetime=[]; pulseshape=[];
    Die=[]; ModuleDie1=[];
    list_fail={};
    Test_Journal={};
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %fill first parameters
    Test_Journal{1,4} = TODATE;
    Test_Journal{1,5} = 'GTS';
    Test_Journal{1,6} = Operator;
    
   %% create the Measurement_Report folder
    Pathnames = {report_name,PATH_NR, Lot, DeviceName, Macro, Chip};
     if isempty(Pathnames{path_name})
        folder_name = Pathnames{2};
    else
        folder_name = Pathnames{path_name};
    end
    [~,~,sPath4] = dirr(char([PathName_save '\' folder_name]),['\' folder_name '_Measurement_Report.tex\>'],'name');
    if ~isempty(sPath4)
        error_dlg = errordlg(['Measurement_Report schon vorhanden: ' sPath4], 'Data Conflict');
        waitfor(error_dlg);
        close (h);
        return;
    end
    
    % Use path number/path name selected by user as save folder
    try
        dir_structure = [PathName_save '\' folder_name];
    catch
        dir_structure = [PathName_save '\' PATH_NR];
    end
    mkdir(dir_structure)

    csvsave_dir = [dir_structure '\' folder_name '_HBM_eval.csv'];

     %% Obtain extraction criterion name
    criteria_table = {'Import from TLP Data','Spot_1-Delta-Mean','Spot_2-Delta-Mean',...
         
        'Voltage_peak (Cap)','Spot_1-Min Limit','Spot_1-Max Limit',...
        'Spot_2-Min Limit','Spot_2-Max Limit','Spot_12-Min Limit','Spot_12-Max Limit','Spot_1-Prevalue Variation',...
        'Spot_2-Prevalue Variation','2_Spots-Prevalue Variation','NO_Spots'};
    if criterion_mode >= 8 && criterion_mode < 15
        threshold = escapeFunction(num2str(Min_Max_Limit));
    elseif criterion_mode < 8
        threshold =  [escapeFunction(num2str(extraction_threshold)) '\\%%'];
    elseif criterion_mode >= 15 && criterion_mode ~= 18
        threshold = [escapeFunction(num2str( Max_Variation)) '\\%%'];
        Max_Variation = Max_Variation*0.01;
    elseif criterion_mode == 18
        threshold = 'NONE';
    end
    
    TEST = 'HBM GTS';

    slash_pos=strfind(sPath{1},'\');

%% create latex file 

    autodetect='No';
    device_prop = struct('autodetect', autodetect,...
        'PATH_NR', PATH_NR, 'TEST', TEST, 'criteria_table', {criteria_table},...
        'criterion_mode', criterion_mode, 'extraction_threshold', threshold,...
        'IforV', IforV, 'VforI', VforI, 'VHBMlevel', VHBMlevel);      
    [fileID, ~, plotID] = createLatexFile2_GTS(dir_structure, TestJobInfo, device_prop, ALLSAMP, fatherPath, Wafermap_Checkbox, folder_name);

    %% WRITE CSV FILE HEADER&OBTAIN HBM CASES
    [Calc_case, fid2, hbm_var] = CSV_File_Header(VforI, IforV, VHBMlevel, csvsave_dir, PATH_NR, TODATE, TEST, Macro, DeviceName, ALLSAMP, sPath, pulsetime, pulseshape, criteria_table, criterion_mode, contact_fail, hbm_mode, snapback_values.eval_snapback);

    %% READ csvFILE
    id = 1: size(sPath, 2);
    all_dies = cell(1, size(sPath, 2));

    cnt_sample_plots = 0;
    cnt_fails = 0;
    DeviceName_list = {};
    
    %start Waitbar
    if ishandle(h)
        delete (h);
    end
    h = mywaitbar(2,'Data processing');
        
    %% File exists.  Do stuff....
    for k = 1:length(sPath) % analyse all xlsx.-files in that path
            
            f=sPath{k};
            indx = length(pathstrc(1,k).pathfields);
            % read sheets of .xlsx-file
            [A, parameters, parameters_num] = xlsread(f, 1);
            [~, data_string, data_num] = xlsread(f, 2);
            [~, sweep_string, sweep_num] = xlsread(f, 4);
            waitbar(k / (length(sPath)+1))
            [~, current, current_num] = xlsread(f, 7);
            [~, voltage, voltage_num] = xlsread(f, 6);
            
            waitbar(k / (length(sPath)))
            
            
            % read "Test Parameters and Outcome" for every file
            [row_start_time, col_start_time] = find(ismember(parameters, 'Start Time:'));
            if ~isempty(row_start_time)
                start_time = parameters_num{row_start_time, col_start_time + 1};
            else
                start_time = '';
            end
            
            [row_job, col_job] = find(ismember(parameters, 'Job:'));
            if ~isempty(row_job)
               job = parameters_num{row_job, col_job+1};
            else
               job = 'NONE';
            end
            
            DeviceName = job;
            
            [row_stop_time, col_stop_time] = find(ismember(parameters, 'Stop Time:'));
            if ~isempty(row_stop_time)
                stop_time = parameters_num{row_stop_time, col_stop_time + 1};
            else
                stop_time = '';
            end

            [row_DUT_name, col_DUT_name] = find(ismember(parameters, 'DUT Name:'));
            if ~isempty(row_DUT_name)
                DUT_name = parameters_num{row_DUT_name, col_DUT_name + 1};
            else
                DUT_name = '';
            end
            all_dies{k} = DUT_name;

            [row_stress_polarity, col_stress_polarity] = find(ismember(parameters, 'Stress Polarity:'));
            if ~isempty(row_stress_polarity)
                stress_polarity = parameters_num{row_stress_polarity, col_stress_polarity + 1};
            else
                stress_polarity = '';
            end

            [row_stresses_per_voltage, col_stresses_per_voltage] = find(ismember(parameters, 'Stresses Per Voltage:'));
            if ~isempty(row_stresses_per_voltage)
                stresses_per_voltage = parameters_num{row_stresses_per_voltage, col_stresses_per_voltage + 1};
            else
                stresses_per_voltage = '';
            end
            
            [row_stop_time, col_stop_time] = find(ismember(parameters, 'Stop Time:'));
            if ~isempty(row_stop_time)
                stop_time = parameters_num{row_stop_time, col_stop_time + 1};
            else
                stop_time = '';
            end
            
           [row_pin1, col_pin1] = find(ismember(parameters, 'Stress Pin:'));
            if ~isempty(row_pin1)
                Pin1 = parameters_num{row_pin1, col_pin1 + 1};
            else
                Pin1 = '';
            end
            
            [row_pin2, col_pin2] = find(ismember(parameters, 'Ground Pin:'));
            if ~isempty(row_pin2)
                Pin2 = parameters_num{row_pin2, col_pin2 + 1};
            else
                Pin2 = '';
            end
            
            % fill Test_journal Pin combination
            Test_Journal{k,1} = Pin1;
            Test_Journal{k,2} = Pin2;
            
            %% read Data
            [data_matrix, failure_test_names, outcome, Pre_sweep, Post_sweep, cols_failure_test, failure_tests_spots, criteria_matrix, Value_table] = extract_gts_data(parameters,parameters_num, data_string, data_num, sweep_num, current_num, voltage_num, stress_polarity, criterion_mode, threshold, criteria_table, Min_Max_Limit, Max_Variation);

            %% criterion modes
            [It2, Vt2, crit_spot, strContfail, nonfail, hbm_at_it2, V_HBM_fail, fail, fail_pulse] = Criteria_GTS(data_matrix, criterion_mode, extraction_threshold, Min_Max_Limit, contact_fail, parameters_num, cols_failure_test, parameters, failure_tests_spots, outcome, hbm_mode, min_fail, Max_Variation,criteria_matrix);
            cnt_fails = cnt_fails + fail;
            %% CALCULATE R_DYN
            [start_index, stop_index, fit_neu, R_dyn] = Rdyn(imax, imin, umin, umax, data_matrix, It2, Vt2);

            %% READ csvFILE PRE SWEEP
            % does a presweep or postsweep exist for the current
            % [VV, IA, VVpost, IApost, presweep_exists, postsweep_exists] = check_sweep(sPath{k});
            index_nan=cellfun(@isnan,sweep_num(4, :),'uni',false);
            index_nan=cellfun(@any,index_nan);
            index_nan=find(index_nan==1, 1, 'first');
            if ~isempty(sweep_num)
                cnt_sample_plots = cnt_sample_plots+1;
                postsweep_exists = 1;
                VVpost = cell2mat(sweep_num(5:end, 1));
                IApost = cell2mat(sweep_num(5:end, index_nan-1));
                IApost=abs(IApost);

                presweep_exists = 1;
                VV = cell2mat(sweep_num(5:end, 1));
                IA = cell2mat(sweep_num(5:end, 2));
                IA=abs(IA);
            else
                VVpost = [];
                IApost = [];
                VV = [];
                IA = [];
                presweep_exists = 0;
                postsweep_exists = 0;
            end
            if presweep_exists || postsweep_exists
                names{cnt_sample_plots} = ['Leakage_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];
                cnt_sample_plots = cnt_sample_plots + 1;
                names{cnt_sample_plots} = ['Stress_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];
                write_sample_latex(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, VV, IA, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, fid2, strContfail, nonfail, hbm_at_it2, VVpost, IApost, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name);

            else
                names{cnt_sample_plots} = ['Stress_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];

                % if NO pre_sweep and NO post sweep
                write_sample_latex_no_sweep(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, fid2, strContfail, nonfail, hbm_at_it2, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name)

            end
            
            % list of fail and pass
            if strcmp(nonfail, 'failed')
                list_fail{k,1} =  all_dies{k};
                list_fail{k,2} = 'red' ;
                Test_Journal{k,3} = fail_pulse;
            elseif strcmp(nonfail,'passed')
                list_fail{k,1} = all_dies{k} ;
                list_fail{k,2} = 'green' ;
                Test_Journal{k,3} = 'Pass';
            else
                list_fail{k,1} = all_dies{k} ;
                list_fail{k,2} = 'black' ;
                Test_Journal{k,3} = fail_pulse;
            end
            DeviceName_list{k} = DeviceName;
            %It2 in Testjournal
             Test_Journal{k,7} = It2;
    end
         if ishandle(h)
                delete (h);
         end
            
        h = mywaitbar(1,'Generating report...');

        %% ANALYZE DATA - GENERATE STATISTIC
        [parameter_values, stat_values, device_name, angel, space_str] = extract_statistic_values(csvsave_dir, hbm_var, hbm_mode, snapback_values.eval_snapback, fileID);
        statistics(csvsave_dir, dir_structure, hbm_mode, snapback_values.eval_snapback, parameter_values, stat_values, device_name, angel, plotID, cnt_sample_plots, space_str, folder_name)
        names{cnt_sample_plots + 1} = 'Statistik.png';
        names{cnt_sample_plots + 2} = 'CDF.png';
        
        % Write data summary in Statistics_Summary
        if first_run==1  
                fprintf(summary, '%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\r\n', 'Technology',' Device Name',' Los_Nr.',' #Wf',' Reticle',' Chip',' Pin',' Samples',' Mean_Vt2[V]',' Mean_It2[A]',' Mean_HBM[V]',' Mean_Rdyn[Ohm]',' Min_Vt2[V]',' Min_It2[A]',' Min_HBM[V]',' Min_Rdyn[Ohm]',' Max_Vt2[V]',' Max_It2[A]',' Max_HBM[V]',' Max_Rdyn[Ohm]',' Sigma_Vt2[V]',' Sigma_It2[A]',' Sigma_HBM[V]',' Sigma_Rdyn[Ohm]');
        end
        fprintf(summary, '%s;%s;%s;%s;%s;%s;%s;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f\r\n', Technology ,  DeviceName ,  Lot ,  wafer_number ,  Reticle ,  Chip ,  Pin ,  size(ALLSAMP,2) , stat_values.meanV ,  stat_values.meanI ,  stat_values.meanHBM ,  stat_values.meanRdyn ,  stat_values.minV , stat_values.minI ,  stat_values.minHBM ,  stat_values.minRdyn ,  stat_values.maxV , stat_values.maxI ,  stat_values.maxHBM ,  stat_values.maxRdyn ,  stat_values.stdV ,  stat_values.stdI ,  stat_values.stdHBM ,  stat_values.stdRdyn);
        
        % tabelle erstellen im report(liste of evaluated samples)
        mktable(id, all_dies, dir_structure,list_fail,Test_Journal,Wafermap_Checkbox,TEST,DeviceName_list,snapback_values.eval_snapback);
        generate_pdf(fileID, plotID, dir_structure, cnt_sample_plots, names, save_pngs,folder_name);
        
        % create Test Journal
        if test_journal
            createExcelSummaryTable(Test_Journal,dir_structure,DeviceName);
        end
        
        % end waitbar 
        delete(h);
        % Check if report has been generated
        [~,~,sPath5] = dirr(char(PathName_save),'\Measurement_Report.pdf\>','name');
        if isempty(sPath5)
            warn_dlg = warndlg (['Measurement Report f�r Pfad Nummer ' num2str(pathNr) ' konnte nicht generiert werden.'], 'Data Conflict');
            waitfor(warn_dlg);
        end
        
        % Copy measurement report to path
        if copy_report 
            if strcmp(report_path ,'Copy "Measurement Report" to...')
                msgbox('Path for Copy "Measurement Report" is missing');
            else
               mkdir(report_path);
               if isempty(report_name)
                   report_path = [report_path '\' folder_name '.pdf'];
               else
                   report_path = [report_path '\' report_name '.pdf'];
               end
               measurement_report_path = [dir_structure '\' folder_name '_Measurement_Report.pdf'];      
               copyfile (measurement_report_path,report_path);
            end
        end
    F = findall(0,'type','figure','tag','TMWWaitbar');
    delete(F);
 catch 
    ErrorMessage=lasterr;
    errordlg(ErrorMessage)
    F = findall(0,'type','figure','tag','TMWWaitbar');
    delete(F);
    delete(h);
end
    
end
