function [Results_String_Titel,Index_titel] = Get_Titel_Checkboxen(app)
%Get_Titel_Checkboxen Summary of this function goes here
%   composes the results string for the title of the figure

Val_Module_Name_Titel= get(app.ModName_Titel, 'Value');

Val_Dev_Name_Titel= get(app.DevName_Titel, 'Value'); % Device Name
Val_DeviceNb_Titel=get(app.DevNb_titel, 'Value'); % Device Number
Val_Coord_Titel=get(app.Coord_titel, 'Value'); % Coordinate
Val_PulseVsGnd_titel= get(app.PulseVsGnd_titel, 'Value'); % PulsVsGnd
Val_Bias1_Titel = get(app.Bias1_titel, 'Value'); % Bias1
Val_Bias2_Titel= get(app.Bias2_titel, 'Value'); % Bias2
Val_Lot = get(app.CheckBox_Lot_title,'Value'); % Lot
Val_WaferNb = get(app.CheckBox_wafernb_title,'Value'); % Wafer Number

if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value,'SPOT') || strcmp(app.LIST_SOA.Value, 'IV Curve with SPOT')
    Val_IT2 = get(app.IT2_title,'Value');
    Val_VT2 = get(app.VT2_title,'Value');
    Val_VHold = get(app.Vhold_Title,'Value');
else
    Val_IT2 = false;
    Val_VT2 = false;
    Val_VHold = false;
end

if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value, 'IV Curve with SPOT')
    Val_VatI = get(app.V_at_I_Checkbox_T,'Value');
    Val_IatV = get(app.I_at_V_Checkbox_T,'Value');
else    
    Val_VatI = false;
    Val_IatV = false;
end
% Combine the Checkbox to one array 
Results = [Val_Module_Name_Titel,Val_Dev_Name_Titel,Val_DeviceNb_Titel,Val_Coord_Titel,Val_PulseVsGnd_titel,Val_Bias1_Titel,Val_Lot,Val_WaferNb,Val_IT2,Val_VT2,Val_VHold,Val_VatI,Val_IatV]; %,Val_Bias2
Index_titel = find(Results); % find index of the checked box

Results_String_Titel = app.measure_params{1,Index_titel}
end
