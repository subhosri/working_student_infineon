function [VatI, IatV, V_clamp, HBM_Correlation, plot_VatI, plot_IatV, plot_V_clamp] = HBM_calculations(Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, Die_coord, LAST, IforV, VforI, csvsave_dir, data_matrix, strContfail, nonfail, hbm_mode, hbm_at_it2, Vhold, Vtrig, Vbreakdown, Vholdx,Vhold_1)
%augerufen im write_sample_latex

plot_VatI = '';
plot_IatV = '';
plot_V_clamp = '';
%% HBM calculations
if hbm_mode
    HBM_Correlation = hbm_at_it2;
else
    % HBM Correlation
    HBM_Correlation = 1500*It2;
end

if size(data_matrix, 1) > 1
    VatI = NaN;
    IatV = NaN;
    V_clamp = NaN;
    switch Calc_case
        case 0
            display 'No extra calculations';
            %HBM_Correlation=1500*It2;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%NEGATIVE
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PARAMETER
            %                 VatI = NaN;
            %                 IatV = NaN;
            %                 V_clamp = NaN;
        case 1
            if It2<0
                IforV=IforV*(-1);
                VforI=VforI*(-1);
                VHBMlevel=VHBMlevel*(-1);
                
                % V_clamp from It2
                %HBM_Correlation=1500*It2;
                % A V@I from Idef
                if IforV < data_matrix(size(data_matrix, 1)-1,3) || IforV > data_matrix(1,3)
                    display(['VatI could not be calculated for ' LAST]);
                    VatI=NaN;
                else
                    for r=1:(size(data_matrix, 1)-1)
                        if data_matrix(r,3)>=IforV && IforV >=data_matrix(r+1,3)
                            VatI=(IforV-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
                            break
                        end
                    end
                end
                % B I@V from Vdef
                if VforI < min(data_matrix(:,2)) || VforI > max(data_matrix(:,2))
                    display(['IatV could not be calculated for ' LAST]);
                    IatV=NaN;
                else
                    for s=1:(size(data_matrix, 1)-1)
                        if data_matrix(s,2)>=VforI && VforI>=data_matrix(s+1,2)
                            IatV=(VforI-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3);
                            break
                        end
                    end
                end
                % C V_clamp from HBM-voltage-level
                I_HBMref=2/3*VHBMlevel;
                if I_HBMref < data_matrix(size(data_matrix, 1),3) || I_HBMref > data_matrix(1,3)
                    display(['V_clamp could not be calculated for ' LAST]);
                    V_clamp=NaN;
                else
                    for t=1:(size(data_matrix, 1)-1)
                        if data_matrix(t,3)>=I_HBMref && I_HBMref>=data_matrix(t+1,3)
                            V_clamp=(I_HBMref-data_matrix(t,3))/(data_matrix(t+1,3)-data_matrix(t,3))*(data_matrix(t+1,2)-data_matrix(t,2))+data_matrix(t,2);
                            break
                        end
                    end
                end
                
            else
                % V_clamp from It2
                %HBM_Correlation=1500*It2;
                % A V@I from Idef
                if IforV > data_matrix(size(data_matrix, 1)-1,3) || IforV < data_matrix(1,3)
                    display(['VatI could not be calculated for ' LAST]);
                    VatI=NaN;
                else
                    for r=1:(size(data_matrix, 1)-1)
                        if data_matrix(r,3)<=IforV && IforV<=data_matrix(r+1,3)
                            VatI=(IforV-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
                            %break :
                        end
                    end
                end
                % B I@V from Vdef
                if VforI < min(data_matrix(:,2)) || VforI > max(data_matrix(:,2))
                    display(['IatV could not be calculated for ' LAST]);
                    IatV=NaN;
                else
                    for s=1:(size(data_matrix, 1)-1)
                        if data_matrix(s,2)<=VforI && VforI<=data_matrix(s+1,2)
                            IatV=(VforI-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3);
                            %break
                        end
                    end
                end
                % C V_clamp from HBM-voltage-level
                I_HBMref=2/3*VHBMlevel;
                if I_HBMref > data_matrix(size(data_matrix, 1),3) || I_HBMref < data_matrix(1,3)
                    display(['V_clamp could not be calculated for ' LAST]);
                    V_clamp=NaN;
                else
                    for t=1:(size(data_matrix, 1)-1)
                        if data_matrix(t,3)<=I_HBMref && I_HBMref<=data_matrix(t+1,3)
                            V_clamp=(I_HBMref-data_matrix(t,3))/(data_matrix(t+1,3)-data_matrix(t,3))*(data_matrix(t+1,2)-data_matrix(t,2))+data_matrix(t,2);
                            break
                        end
                    end
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 2
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % B I@V from Vdef
            if VforI < min(data_matrix(:,2)) || VforI > max(data_matrix(:,2))
                display(['IatV could not be calculated for ' LAST]);
                IatV=NaN;
            else
                for s=1:(size(data_matrix, 1)-1)
                    if data_matrix(s,2)<=VforI && VforI<=data_matrix(s+1,2)
                        IatV=(VforI-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3);
                        break
                    end
                end
            end
            % C V_clamp from HBM-voltage-level
            I_HBMref=2/3*VHBMlevel;
            if I_HBMref > data_matrix(size(data_matrix, 1),3) || I_HBMref < data_matrix(1,3)
                display(['V_clamp could not be calculated for ' LAST]);
                V_clamp=NaN;
            else
                for t=1:(size(data_matrix, 1)-1)
                    if data_matrix(t,3)<=I_HBMref && I_HBMref<=data_matrix(t+1,3)
                        V_clamp=(I_HBMref-data_matrix(t,3))/(data_matrix(t+1,3)-data_matrix(t,3))*(data_matrix(t+1,2)-data_matrix(t,2))+data_matrix(t,2);
                        break
                    end
                end
            end
        case 3
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % A V@I from Idef
            if IforV > data_matrix(size(data_matrix, 1)-1,3) || IforV < data_matrix(1,3)
                display(['VatI could not be calculated for ' LAST]);
                VatI=NaN;
            else
                for r=1:(size(data_matrix, 1)-1)
                    if data_matrix(r,3)<=IforV && IforV<=data_matrix(r+1,3)
                        VatI=(IforV-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
                        break
                    end
                end
            end
            % C V_clamp from HBM-voltage-level
            I_HBMref=2/3*VHBMlevel;
            if I_HBMref > data_matrix(size(data_matrix, 1),3) || I_HBMref < data_matrix(1,3)
                display(['V_clamp could not be calculated for ' LAST]);
                V_clamp=NaN;
            else
                for t=1:(size(data_matrix, 1)-1)
                    if data_matrix(t,3)<=I_HBMref && I_HBMref<=data_matrix(t+1,3)
                        V_clamp=(I_HBMref-data_matrix(t,3))/(data_matrix(t+1,3)-data_matrix(t,3))*(data_matrix(t+1,2)-data_matrix(t,2))+data_matrix(t,2);
                        break
                    end
                end
            end
        case 4
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % A V@I from Idef
            if IforV > data_matrix(size(data_matrix, 1)-1,3) || IforV < data_matrix(1,3)
                display(['VatI could not be calculated for ' LAST]);
                VatI=NaN;
            else
                for r=1:(size(data_matrix, 1)-1)
                    if data_matrix(r,3)<=IforV && IforV<=data_matrix(r+1,3)
                        VatI=(IforV-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
                        break
                    end
                end
            end
            % B I@V from Vdef
            if VforI < min(data_matrix(:,2)) || VforI > max(data_matrix(:,2))
                display(['IatV could not be calculated for ' LAST]);
                IatV=NaN;
            else
                for s=1:(size(data_matrix, 1)-1)
                    if data_matrix(s,2)<=VforI && VforI<=data_matrix(s+1,2)
                        IatV=(VforI-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3);
                        break
                    end
                end
            end
        case 5
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % C V_clamp from HBM-voltage-level
            I_HBMref=2/3*VHBMlevel;
            if I_HBMref > data_matrix(size(data_matrix, 1),3) || I_HBMref < data_matrix(1,3)
                display(['V_clamp could not be calculated for ' LAST]);
                V_clamp=NaN;
            else
                for t=1:(size(data_matrix, 1)-1)
                    if data_matrix(t,3)<=I_HBMref && I_HBMref<=data_matrix(t+1,3)
                        V_clamp=(I_HBMref-data_matrix(t,3))/(data_matrix(t+1,3)-data_matrix(t,3))*(data_matrix(t+1,2)-data_matrix(t,2))+data_matrix(t,2);
                        break
                    end
                end
            end
        case 6
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % A V@I from Idef
            if IforV > data_matrix(size(data_matrix, 1)-1,3) || IforV < data_matrix(1,3)
                display(['VatI could not be calculated for ' LAST]);
                VatI=NaN;
            else
                for r=1:(size(data_matrix, 1)-1)
                    if data_matrix(r,3)<=IforV && IforV<=data_matrix(r+1,3)
                        VatI=(IforV-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
                        break
                    end
                end
            end
        case 7
            % V_clamp from It2
            %HBM_Correlation=1500*It2;
            % B I@V from Vdef
            if VforI < min(data_matrix(:,2)) || VforI > max(data_matrix(:,2))
                display(['IatV could not be calculated for ' LAST]);
                IatV=NaN;
            else
                for s=1:(size(data_matrix, 1)-1)
                    if data_matrix(s,2)<=VforI && VforI<=data_matrix(s+1,2)
                        IatV=(VforI-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3);
                        break
                    end
                end
            end
    end
else
    VatI = NaN;
    IatV = NaN;
    V_clamp = NaN;
end
if Vbreakdown
    Vb_str = Vbreakdown;
else
    Vb_str = NaN;
end

if  Vhold
    vhold1_str = Vhold_1;
else
    vhold1_str = NaN;
end

if  Vholdx
    vhold_str = Vholdx;
else
    vhold_str = NaN;
end



%Write coordinates Vt2 It2 into csv
try
    switch Calc_case
        case 0
            data_cell = {Die , Die_coord , LAST , Vt2 , It2 , R_dyn , HBM_Correlation , VatI ,IatV ,...
                V_clamp, strContfail , nonfail, Vb_str, vhold_str,  vhold1_str};
        case 1
             data_cell = {Die , Die_coord , LAST  , Vt2 , It2 , R_dyn ,HBM_Correlation , VatI , IatV ,...
                 V_clamp , strContfail , nonfail,  Vb_str, vhold_str, vhold1_str};
        case 2
            data_cell = {Die , Die_coord , LAST , Vt2 , It2 , R_dyn , HBM_Correlation , IatV , V_clamp,...
                strContfail , nonfail, Vb_str, vhold_str, vhold1_str};
        case 3
            data_cell = {Die , Die_coord , LAST  , Vt2 , It2 , R_dyn , HBM_Correlation , VatI , V_clamp,...
                strContfail , nonfail, Vb_str,vhold_str, vhold1_str};
        case 4
            data_cell = {Die , Die_coord , LAST  , Vt2 , It2 , R_dyn , HBM_Correlation , VatI, IatV,...
                strContfail , nonfail,  Vb_str, vhold_str, vhold1_str};
        case 5
            data_cell = {Die , Die_coord , LAST  , Vt2 , It2 ,R_dyn , HBM_Correlation , V_clamp ,...
                strContfail , nonfail,  Vb_str, vhold_str, vhold1_str};
        case 6
            data_cell = {Die , Die_coord , LAST  , Vt2 , It2 ,R_dyn, HBM_Correlation , VatI ,...
                strContfail , nonfail,  Vb_str, vhold_str, vhold1_str};
        case 7
            data_cell = {Die , Die_coord , LAST  , Vt2 , It2 , R_dyn , HBM_Correlation , IatV ,...
                strContfail ,nonfail,  Vb_str, vhold_str, vhold1_str};
    end
    writecell(data_cell,csvsave_dir,'WriteMode','append')
catch
    disp(['cannot write to ' fid2])
    fclose('all');
end


%% plot HBM-correlation lines
if hbm_mode
    V_clamp = nan(1);
end
switch Calc_case
    case 0
        hold off
    case 1
        % plot A
        if ~isnan(VatI)
            plot_VatI = ['\\addplot[\n',...
                'color = red,\n',...
                'fill = red,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VatI) ',' num2str(IforV) ')\n',...
                '};\n'];
        end
        % plot B
        if ~isnan(IatV)
            plot_IatV = ['\\addplot[\n',...
                'color = black,\n',...
                'fill = black,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VforI) ',' num2str(IatV) ')\n',...
                '};\n'];
        end
        % plot C
        if ~isnan(V_clamp)
            plot_V_clamp = ['\\addplot[\n',...
                'color = mycyan,\n',...
                'fill = mycyan,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(V_clamp) ',' num2str(I_HBMref) ')\n',...
                '};\n'];
        end
    case 2
        % plot B
        if ~isnan(IatV)
            plot_IatV = ['\\addplot[\n',...
                'color = black,\n',...
                'fill = black,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VforI) ',' num2str(IatV) ')\n',...
                '};\n'];
        end
        % plot C
        if ~isnan(V_clamp)
            plot_V_clamp = ['\\addplot[\n',...
                'color = mycyan,\n',...
                'fill = mycyan,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(V_clamp) ',' num2str(I_HBMref) ')\n',...
                '};\n'];
        end
    case 3
        % plot A
        %plot(ax2,VatI,IforV,'ro','LineWidth',1.5,'MarkerSize',6)
        if ~isnan(VatI)
            plot_VatI = ['\\addplot[\n',...
                'color = red,\n',...
                'fill = red,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VatI) ',' num2str(IforV) ')\n',...
                '};\n'];
        end
        % plot C
        if ~isnan(V_clamp)
            plot_V_clamp = ['\\addplot[\n',...
                'color = mycyan,\n',...
                'fill = mycyan,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(V_clamp) ',' num2str(I_HBMref) ')\n',...
                '};\n'];
        end
    case 4
        % plot A
        if ~isnan(VatI)
            plot_VatI = ['\\addplot[\n',...
                'color = red,\n',...
                'fill = red,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' VatI ',' IforV ')\n',...
                '};\n'];
        end
        % plot B
        if ~isnan(IatV)
            plot_IatV = ['\\addplot[\n',...
                'color = black,\n',...
                'fill = black,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VforI) ',' num2str(IatV) ')\n',...
                '};\n'];
        end
    case 5
        % plot C
        if ~isnan(V_clamp)
            plot_V_clamp = ['\\addplot[\n',...
                'color = mycyan,\n',...
                'fill = mycyan,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(V_clamp) ',' num2str(I_HBMref) ')\n',...
                '};\n'];
        end
    case 6
        % plot A
        if ~isnan(VatI)
            plot_VatI = ['\\addplot[\n',...
                'color = red,\n',...
                'fill = red,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VatI) ',' num2str(IforV) ')\n',...
                '};\n'];
        end
    case 7
        % plot B
        if ~isnan(IatV)
            plot_IatV = ['\\addplot[\n',...
                'color = black,\n',...
                'fill = black,\n',...
                'mark = *,\n',...
                'only marks\n',...
                '] coordinates {\n',...
                '(' num2str(VforI) ',' num2str(IatV) ')\n',...
                '};\n'];
        end
end
end