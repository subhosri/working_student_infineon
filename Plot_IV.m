function  Plot_IV(app)
% -----------------------------------------------------------------------
% Plots Curves in the IV Curve mode
% Called by SOA_mode
% -----------------------------------------------------------------------
% Inputs:
% app: calling App
% -----------------------------------------------------------------------
m = {'*','o','x','.','+','s','d','^','v','>','<','p','h','*','o','x','.','+','s','v'};
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
set(app.axes1,'YScale', 'linear','XScale', 'linear')
app.X =  gobjects(2,size(app.measure_params,1));
for i = 1: size (app.I,2)
    if (~isempty(app.V{1,i}))&(~isempty(app.I{1,i}))
        if i<=16 %Cover against running out of colors
            app.X(:,i) = plot(app.axes1,app.V{1,i},app.I{1,i},'Color',C{i},'marker',m{i});
        else
            app.X(:,i) = plot (app.axes1,app.V{1,i},app.I{1,i});
        end
    end
    hold (app.axes1,'on')
end
grid (app.axes1, 'on')
zoom(app.axes1, 'on')
xlabel(app.axes1,'Voltage (V)', "FontSize",20);
ylabel(app.axes1,'Current (A)',"FontSize",20);
title(app.axes1,'IV Curve');
set(app.axes1,'FontSize',20);
end

