function Plot_IV_Spot(app)
% -----------------------------------------------------------------------
% Plots Curves in the IV and Spot Curve modes
% Called by SOA_mode
% -----------------------------------------------------------------------
% Inputs:
% app: calling App
% -----------------------------------------------------------------------
m = {'*','o','x','.','+','s','d','^','v','>','<','p','h','*','o','x','.','+','s','v'};
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
app.X =  gobjects(3,size(app.measure_params,1));
val_spot1_iv = get(app.Spot1_IV_with_spot, 'Value');
val_spot2_iv = get(app.Spot2_IV_with_spot, 'Value');
for i = 1: size (app.Spot1,2)
    if (~isempty(app.V{1,i}))&(~isempty(app.I{1,i}))
%         if i <= 16
%             if val_spot1_iv == 1
%                 app.X(1:2,i)=semilogx(app.UIAxes2,app.Spot1{1,i},app.I{1,i},'color',C{i},'marker',m{i});
%                 set(semilogx(app.UIAxes2,app.Spot2{1,i},app.I{1,i},'color',C{i},'marker',m{i}),'visible','off');
% 
%             elseif val_spot2_iv == 1
%                 app.X(1:2,i)=semilogx(app.UIAxes2,app.Spot2{1,i},app.I{1,i},'color',C{i},'marker',m{i});
%                 set(semilogx(app.UIAxes2,app.Spot1{1,i},app.I{1,i},'color',C{i},'marker',m{i}),'visible','off');
%             end
%        
% 
%                        
% %         if i <= 16
% %              app.X(1:2,i)=semilogx(app.UIAxes2,app.Spot1{1,i},app.I{1,i},app.Spot2{1,i},app.I{1,i},'color',C{i},'marker',m{i});
% %          
%         else
%             app.X(1:2,i)=semilogx(app.UIAxes2,app.Spot1{1,i},app.I{1,i},app.Spot2{1,i},app.I{1,i});
%         end
 
    app.X(3,i) = plot(app.UIAxes,app.V{1,i},app.I{1,i},'Color',C{i},'marker',m{i});
    hold(app.UIAxes, 'on')
    hold(app.UIAxes2, 'on')
    %app.X(3,i) = plot(app.UIAxes,app.V{1,i},app.I{1,i},'color',app.X(1,i).Color,'marker',app.X(1,i).Marker);
    end
end

grid(app.UIAxes,'on')
% dim_IV = get(app.UIAxes,'position');
% w=dim_IV(3)
% set(app.UIAxes,'Position',[0.1300,0.1100,0.3347*1.5,0.8150])
% set(app.UIAxes,'Position',[dim_IV(1),dim_IV(2),w*1.5,dim_IV(4)])
% dim_spot = get(app.UIAxes2,'position');
% l = dim_spot(1);
% w_spot = dim_spot(3);
% set(app.UIAxes2,'Position',[0.5703*1.2,0.1100,0.3347*0.5,0.8150])
% set(app.UIAxes2,'Position',[l*1.2,dim_spot(2),w_spot*0.7,dim_spot(4)],'box','on')
grid(app.UIAxes2,'on')
zoom (app.UIAxes, 'on')
zoom (app.UIAxes2, 'on')
xlabel(app.UIAxes,'Voltage (V)',"FontSize",20);
ylabel(app.UIAxes,'Current (A)',"FontSize",20);
title(app.UIAxes,'IV Curve');
set(app.UIAxes,'FontSize',20);
xlabel(app.UIAxes2,'Leakage (A)',"FontSize",10);
%ylabel(app.UIAxes2,'Current (A)',"FontSize",20);
title(app.UIAxes2,'SPOT');
set(app.UIAxes2,'FontSize',20)
set(app.UIAxes2, 'YScale', 'linear','XScale', 'log');
end