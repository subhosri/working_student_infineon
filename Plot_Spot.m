function Plot_Spot(app)
% -----------------------------------------------------------------------
% Plots Curves in the Spot Curve mode
% Called by SOA_mode
% -----------------------------------------------------------------------
% Inputs:
% app: calling App
% -----------------------------------------------------------------------
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
app.X =  gobjects(2,size(app.measure_params,1));
set(app.axes1, 'YScale', 'linear','XScale', 'log');
for i = 1: size (app.Spot1,2)
    if isempty(app.Spot2{i})
        if ~isempty(app.Spot1{i})
            if i<= 16
                app.X(:,i)=semilogx(app.axes1,app.Spot1{1,i},app.I{1,i},'Color',C{i},'marker','*');
            else
                app.X(:,i)=semilogx(app.axes1,app.Spot1{1,i},app.I{1,i});
            end
            hold(app.axes1, 'on')
        end
    else
        if i <= 16
            app.X(1,i)= semilogx(app.axes1,app.Spot1{1,i},app.I{1,i},'Color',C{i},'marker','*');
        else
            app.X(1,i)= semilogx(app.axes1,app.Spot1{1,i},app.I{1,i},'marker','*');
        end
        hold(app.axes1, 'on')
        app.X(2,i)=  semilogx(app.axes1,app.Spot2{1,i},app.I{1,i},':','Color',app.X(1,i).Color,'marker','o');
        hold(app.axes1, 'on')
    end
end
grid(app.axes1,'on')
xlabel(app.axes1,'Leakage (A)',"FontSize",20);
ylabel(app.axes1,'Current (A)',"FontSize",20);
title(app.axes1,'Spot');
set(app.axes1,'FontSize',20)
end

