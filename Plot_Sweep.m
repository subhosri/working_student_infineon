function  Plot_Sweep(app)
% -----------------------------------------------------------------------
% Plots Curves in the Sweep Curve mode
% Called by SOA_mode
% -----------------------------------------------------------------------
% Inputs:
% app: calling App
% -----------------------------------------------------------------------
m = {'*','o','x','.','+','s','d','^','v','>','<','p','h','*','o','x','.','+','s','v'};
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
app.X =  gobjects(2,size(app.measure_params,1));
if app.LOG.Value == 1
     set(app.axes1, 'YScale', 'log','XScale', 'linear');
else
    set(app.axes1, 'YScale', 'linear','XScale', 'linear');
end
for i = 1: size (app.V_pre_sweep,2)
    if ~strcmp(app.ShowSweepDropDown.Value,"POST")
        if i<= 16
            app.X(:,i) = plot(app.axes1,app.V_pre_sweep{1,i},abs(app.I_pre_sweep{1,i}),'Color',C{i},'marker',m{i});
        else
            app.X(:,i) = plot(app.axes1,app.V_pre_sweep{1,i},abs(app.I_pre_sweep{1,i}));
        end
        hold(app.axes1, 'on')
    end
    if ~isempty (app.V_post_sweep{i})
        if strcmp(app.ShowSweepDropDown.Value,"BOTH")
            app.X(2,i) = plot(app.axes1,app.V_post_sweep{1,i},abs(app.I_post_sweep{1,i}),':','Color',app.X(1,i).Color,'marker',app.X(1,i).Marker);
        elseif strcmp(app.ShowSweepDropDown.Value,"POST")
            app.X(2,i) = plot(app.axes1,app.V_post_sweep{1,i},abs(app.I_post_sweep{1,i}),':','marker',m{2});
        end
        hold(app.axes1, 'on')
    end
end
grid (app.axes1, 'on')
hold(app.axes1, 'on')
zoom (app.axes1, 'on')
xlabel(app.axes1,'Voltage (V)', "FontSize",20);
ylabel(app.axes1,'Current (A)',"FontSize",20);
title(app.axes1,'DC Sweep')
set(app.axes1,'FontSize',20)
end

