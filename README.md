# ESD Analyzer

The ESD Analyzer Software is a tool for evaluating TLP and HBM measurement data.

## User interface

![The graphical user interface is as shown](Images/esd_analyzer_interface.PNG)

