function [l,p1,p2,tdata] =   R_ON_Rechnen(app)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Nb_Path=  size(app.measure_params,1)
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};

zoom(app.axes1, 'off')

for i= 1:Nb_Path
    xlim = get(app.axes1,'XLim');
    ylim = get(app.axes1,'YLim');
    roi_line = drawline(app.axes1,'SelectedColor',C{i});
%     [x1,y1] = getpts(app.axes1);
%     p1(:,i) = plot(app.axes1,x1,y1,'r*');
%     
%     [x2,y2] = getpts(app.axes1);
%     p2(:,i) = plot(app.axes1,x2,y2,'r*');
    
    m = (y2-y1)/(x2-x1);
    m2 = (x2-x1) / (y2-y1);
    r_on(:,i) =m2;
    n = y2 - x2*m;
    xmin = (1/m)*ylim(1) - n/m;
    xmax = (1/m)*ylim(2) - n/m;
    l(:,i)=line(app.axes1,[xmin xmax],[ylim(1) ylim(2)], 'Color', C{i});
    %             txt = ['R_{on}' num2str(i) ' = ' num2str(m2(i)) '\Omega'];
    %
    %
    %             %t =
    %              text (app.axes1,0.05, 1-i *0.06,txt,'Units','normalized','Color',C{i},'Fontsize',18,'fontweight','bold');
    %
    %dim = [.2 .25- i *0.06 .5 .6];
    %             t=annotation(app.axes1,'textbox',dim ,'String',txt,'COlor',C{i},'FitBoxToText','on');
    %             t.FontSize = 11;
    %set (app.R_on1EditField, 'Value',sscanf(m2,'%f'))   
end
num = 1:i;
tdata = table(num.',r_on.','VariableNames',{'Dies','R_ON'});
zoom(app.axes1, 'on')
end

