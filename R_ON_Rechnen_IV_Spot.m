function [l,p1,p2] =  R_ON_Rechnen_IV_Spot(app)
% Allowes to plot a line and calculate the R_ON in the IV and Spot 
% mode

[~,~,Path_nr] = dirr(char(app.data{ 1 , 1 }),'\pre_sweep.csv\>','name');

Nb_Path=  size(Path_nr,2)* size(app.data,1);
C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]};



for i= 1:Nb_Path
    xlim = get(app.UIAxes,'XLim');
    ylim = get(app.UIAxes,'YLim');
    [x1,y1] = getpts(app.UIAxes);
    p1(:,i) = plot(app.UIAxes,x1,y1,'r*');
    
    [x2,y2] = getpts(app.UIAxes);
    p2(:,i) = plot(app.UIAxes,x2,y2,'r*');
    
    m = (y2-y1)/(x2-x1);
    m2 = (x2-x1) / (y2-y1);
    m2=sprintf('%.2f',  m2);
    n = y2 - x2*m;
    xmin = (1/m)*ylim(1) - n/m;
    xmax = (1/m)*ylim(2) - n/m;
    l(:,i) = line(app.UIAxes,[xmin xmax],[ylim(1) ylim(2)], 'Color', C{i});
    txt = ['R_{on}' num2str(i) ' = ' num2str(m2) '\Omega'];
    
    
    %t =
    text (app.UIAxes,0.05, 1-i *0.06,txt,'Units','normalized','Color',C{i},'Fontsize',18);
    
    %dim = [.2 .25- i *0.06 .5 .6];
    %             t=annotation(app.UIAxes,'textbox',dim ,'String',txt,'COlor',C{i},'FitBoxToText','on');
    %             t.FontSize = 11;
    %set (app.R_on1EditField, 'Value',sscanf(m2,'%f'))
    
    
end
end



