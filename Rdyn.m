function [start_index, stop_index, fit_neu, R_dyn] = Rdyn(imax, imin, umin, umax, data_matrix, It2, Vt2)
% CALCULATE R_DYN
%FITTING FUNCTION%
%augerufen im TLP_analyse_vTG_v07_12
 if all(data_matrix(:,3) < 0)
    threshold = -0.10;
    signe=-1;
 else
     threshold = 0.10;
     signe=1;
 end
    % value from which on to start fitting
if isnan(imax) && isnan(imin) && isnan(umin) && isnan(umax)
    if signe>0
        start_index = find(data_matrix(:,3)>threshold,1);        % Bei INDEX -- Threshold  > 0.15
        stop_index = find (data_matrix(:,3)>= It2,1);
    else
        start_index = find(data_matrix(:,3)<threshold,1);        % Bei INDEX -- Threshold  > 0.15
        stop_index = find(data_matrix(:,3)<= It2,1);
    end
    
    
elseif isnan(umin) && isnan(umax)
    %%%%STROMBEREICH Berechnung �ber die Oberfl�che %%%
    fit_max=imax;   % Obere LIMIT Strombereich [A]
    fit_min=imin;   %Untere LIMIT Strombereich [A]
    if abs(fit_min) > abs(threshold)
        start_index = find(abs(data_matrix(:,3))>abs(fit_min),1);
    end
    if abs(fit_max) < abs(It2)
        stop_index_u= find (abs(data_matrix(:,3))>abs(fit_max),1);
        stop_index=stop_index_u-1;
    else
        stop_index_u= find (abs(data_matrix(:,3))>abs(It2),1);
        stop_index=stop_index_u-1;
    end
    %%%%SPANNUNGSBEREICH Berechnung �ber die Oberfl�che %%%
elseif isnan(imax) && isnan(imin)
    fit_max=umax;   % Obere LIMIT Strombereich [A]
    fit_min=umin;   %Untere LIMIT Strombereich [A]
    start_index = find(abs(data_matrix(:,2))>abs(fit_min),1);
    if abs(fit_max) < abs(Vt2)
        stop_index_u= find (abs(data_matrix(:,2))>abs(fit_max),1);
        stop_index=stop_index_u-1;
    else
        stop_index_u= find (abs(data_matrix(:,2))>abs(Vt2),1);
        stop_index=stop_index_u-1;
    end
    
elseif ~isnan(imax) && ~isnan(imin)  && ~isnan(umin) && ~isnan (umax)
    msgbox ({'Calculation with input over the User Interface not possible with', '--BOTH-- Current Range and Voltage Range'},'Error','error')
end



%% I Werte kleiner als 0.15
R_dyn = nan(1);
fit_neu = nan(1);
if isempty (start_index) || start_index == stop_index + 1 || start_index > stop_index
    start_index=1;
    if start_index > stop_index || start_index == stop_index + 1 || start_index == stop_index
        stop_index = min(size(data_matrix, 1), 3);
    end
    v_values=data_matrix(start_index:stop_index,2);
    i_values=data_matrix(start_index:stop_index,3);

    if size(v_values, 1) > 1
        r_fit=fit(v_values,i_values,'poly1');
        r_fitCoeff = coeffvalues(r_fit);
        a_1= r_fitCoeff(1,1);
        a_0= r_fitCoeff(1,2);
        R_dyn= 1/a_1;
        fit_neu=a_0+a_1*v_values;
    end
else
    
    v_values=data_matrix(start_index:stop_index,2);
    i_values=data_matrix(start_index:stop_index,3);

    if size(v_values, 1) > 1
        r_fit=fit(v_values,i_values,'poly1');
        r_fitCoeff = coeffvalues(r_fit);
        a_1= r_fitCoeff(1,1);
        a_0= r_fitCoeff(1,2);

        R_dyn= 1/a_1
        fit_neu=a_0+a_1*v_values;
    end
end