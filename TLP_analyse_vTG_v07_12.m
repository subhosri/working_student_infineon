function TLP_analyse_vTG_v07_12(~,fatherPath,pathNr,PathName_save, report_path, extraction_threshold,contact_fail,criterion_mode,IforV,VforI,VHBMlevel,imin,imax,umin,umax,imin2,imax2,umin2,umax2,Min_Max_Limit, die_x, die_y, subdie_x, subdie_y, offset_x, offset_y, Wafermap_Checkbox, sPath, hbm_mode, notch_pos, colorbar_values, snapback_values, target_values, critical_area, Vbreakdown_checkbox,Option_Vbreakdown, save_pngs, summary, path_name, first_run, autodetect, min_fail,test_journal, Max_Variation, copy_report, report_name, Vhold1x, filter, checkbox28,Limit)
%    load('unittest_files\input_TLP.mat')
%    save unittest_files\input_TLP.mat pathNr PathName_save Plots_show extraction_threshold contact_fail criterion_mode IforV VforI VHBMlevel Statistik imin imax umin umax Min_Max_Limit die_x die_y subdie_x subdie_y offset_x offset_y Disable_Wafermap table_data hbm_mode notch_pos colorbar_values snapback_values target_values critical_area

try
    %% Remove backslash and create cells with path slices
    % pathstrc: structure with the fields
    %           - path: all paths to TLP_data.csv
    %           - pathfields: all paths seperated
    [pathstrc, ~] = path2strc(sPath);
    
    %% FIRST_FOLDER = pathstrc(1,n).pathfields{1,length(pathstrc(1,n).pathfields)-1};
    PATH_NR = ['Path_' int2str(pathNr)];
    TODATE = datestr(datetime('today'));
    list_fail={};
    Test_Journal={};
    
    %    ALLSAMP = cell(1,length(pathstrc));
    %     for sampcount=1:length(sPath)
    % %        ALLSAMP1(1,sampcount) = cellstr(pathstrc(1,sampcount).pathfields{1,length(pathstrc(1,1).pathfields)-2});
    % %        ALLSAMP(1, sampcount) = (regexp(sPath(sampcount), '\X.*Y(.?)\d', 'match'));
    %     end
    try
        ALLSAMP = cellfun(@(x)regexp(x, '\X.*Y(.?)\d', 'match'), sPath);
    catch
        ALLSAMP = sPath;
    end
    
    %chefai
    %    for i=1:size(ALLSAMP,2)
    %        ALLSAMP{:,i} = ALLSAMP{i}(3:end);
    %    end
    
    %% OBTAIN DATA
    fid1 = fopen(sPath{end});
    output = textscan(fid1,'%s%s','delimiter',',');
    c1 = output{1};
    c2 = output{2};
    fclose(fid1);
    pulseshape = char(c2(6));
    pulsetime = char(c2(7));
    IndexDeviceName = strfind(c1, 'Device Name');
    IndexWafer = strfind(c1, 'Wafer');
    DeviceName = char(c2(not(cellfun('isempty', IndexDeviceName))));
    %%Mohamed
    indice = c2(not(cellfun('isempty', IndexWafer)));
    wafer_number = char(indice(1));
    %%mohamed_end
    IndexMacro = strfind(c1, 'Module');
    Macro = char(c2(not(cellfun('isempty', IndexMacro))));
    IndexTechnology = strfind(c1, 'Technology');
    Technology = char(c2(not(cellfun('isempty', IndexTechnology))));
    IndexLot = strfind(c1, 'Lot');
    Lot = char(c2(not(cellfun('isempty', IndexLot))));
    IndexChip = strfind(c1, 'Test Chip');
    Chip = char(c2(not(cellfun('isempty', IndexChip))));
    IndexPin = strfind(c1, 'Gnd Pin');
    Pin = char(c2(not(cellfun('isempty', IndexPin))));
    IndexReticle = strfind(c1, 'Reticle');
    Reticle = char(c2(not(cellfun('isempty', IndexReticle))));
    spots_2_idx = strfind( c1, 'Spot 2 on Start I');
    spots_2 = sum(not(cellfun('isempty', spots_2_idx)));
    
    if isempty(autodetect)
        autodetect='No';
    end
    
    Pathnames = {report_name, PATH_NR, [Lot, '_', wafer_number], DeviceName, Macro, Chip};
    if isempty(Pathnames{path_name})
        folder_name = Pathnames{2};
    else
        folder_name = Pathnames{path_name};
    end
    
    if checkbox28
        if ~isnan(filter)
            folder_name = [folder_name '_' filter];
        end
    end
    
    [~,~,sPath4] = dirr(char([PathName_save '\' folder_name]),['\' folder_name '_Measurement_Report.tex\>'],'name');
    if ~isempty(sPath4)
        error_dlg = errordlg(['Measurement_Report schon vorhanden: ' sPath4], 'Data Conflict');
        waitfor(error_dlg);
        return;
    end
    
    %fill first parameters
    Test_Journal{1,4} = TODATE;
    Test_Journal{1,5} = 'HPPI';
    
    %% Use path number/path name selected by user as save folder
    try
        dir_structure = [PathName_save '\' folder_name];
    catch
        dir_structure = [PathName_save '\' PATH_NR];
    end
    mkdir(dir_structure)
    
    if hbm_mode
        TEST = 'HBM';
        csvsave_dir = [dir_structure '\' folder_name '_HBM_eval.xlsx'];
    else
        TEST = 'TLP';
        csvsave_dir = [dir_structure '\' folder_name '_TLP_eval.xlsx'];
    end
    
    %% Obtain extraction criterion name
    criteria_table = {'Import from TLP Data','Spot_1-Delta-Mean','Spot_2-Delta-Mean',...
        '2_Spots-Delta','Spot_1-Delta-Prevalue','Spot_2-Delta-Prevalue','2_Spots-Delta-Prevalue',...
        'Voltage_peak (Cap)','Spot_1-Min Limit','Spot_1-Max Limit',...
        'Spot_2-Min Limit','Spot_2-Max Limit','Spot_12-Min Limit','Spot_12-Max Limit','Spot_1-Prevalue Variation',...
        'Spot_2-Prevalue Variation', '2_Spots-Prevalue Variation','NO_Spots'};
    if criterion_mode >= 8 && criterion_mode < 15
        threshold = escapeFunction(num2str(Min_Max_Limit));
    elseif criterion_mode < 8
        threshold =  [escapeFunction(num2str(extraction_threshold)) '\\%%'];
    elseif criterion_mode >= 15 && criterion_mode ~= 18
        threshold = [escapeFunction(num2str(Max_Variation)) '\\%%'];
        Max_Variation = Max_Variation*0.01;
    elseif criterion_mode == 18
        threshold = 'NONE';
    end
    
    device_prop = struct('PATH_NR', PATH_NR, 'TEST', TEST, 'MACRO', Macro,...
        'DEVICE', DeviceName, 'criteria_table', {criteria_table},...
        'criterion_mode', criterion_mode, 'extraction_threshold', threshold,...
        'IforV', IforV, 'VforI', VforI, 'VHBMlevel', VHBMlevel, 'WaferNum', wafer_number,...
        'Technology', Technology, 'Lot', Lot, 'Chip', Chip, 'autodetect', autodetect);
    
    [fileID, ~, plotID,plotID_stat] = createLatexFile2(dir_structure, device_prop, ALLSAMP, fatherPath, Wafermap_Checkbox,folder_name);
    
    %% WRITE CSV FILE HEADER&OBTAIN HBM CASES
    [Calc_case, hbm_var] = CSV_File_Header(VforI, IforV, VHBMlevel, csvsave_dir, PATH_NR, TODATE, TEST, Macro, DeviceName, ALLSAMP, sPath, pulsetime, pulseshape, criteria_table, criterion_mode, contact_fail, hbm_mode, snapback_values.eval_snapback);
    
    %% READ csvFILE
    id = zeros(1, size(sPath, 2));
    all_dies = cell(1, size(sPath, 2));
    
    cnt_sample_plots = 0;
    cnt_fails = 0;
    DeviceName_list = {};
    
    
    %% File exists.  Do stuff....
    h = waitbar(0,'Data is being analyzed...','Position',[290 310 620 410], 'OuterPosition',[320 300 275 120], 'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
    titleHandle = get(findobj(h,'Type','axes'),'Title');
    set(titleHandle,'FontSize',20)
    setappdata(h,'canceling',0)
    waitObject = onCleanup(@() delete(h));
    i =1;
    for k = 1:length(sPath) % analyse all csv.-files in that path
        % Init Waitbar
        waitbar(k / (length(sPath)+1))
        if getappdata(h,'canceling')
            wh = findall(0, 'tag', 'TMWWaitbar');
            delete(wh)
            errordlg ('Programm wurde abgebrochen.');
            return;
        end
        indx = length(pathstrc(1,k).pathfields);
        
        % Load data from the files
        [data_matrix,valid,Die,DeviceName, ModuleDie1, Pin1,Pin2,Operator,genParam,data_cell] = get_data(sPath{k});
        
        if valid
            cnt_sample_plots = cnt_sample_plots+1;
            example_data_matrix = data_matrix;
        else
            continue % End iteration if the Data from the TLP file is invalid
        end
        
        %fill Test_journal Pin combination
        Test_Journal{k,1} = Pin1;
        Test_Journal{k,2} = Pin2;
        Test_Journal{k,6} = Operator;
        
        id(k) = str2double(ModuleDie1);
        all_dies{k} = Die;
        
        %% criterion modes
        [It2, Vt2, crit_spot, strContfail, nonfail, hbm_at_it2, V_HBM_fail, fail, criteria_matrix, Value_table,fail_pulse] = Criteria(data_matrix, criterion_mode, extraction_threshold, Min_Max_Limit, contact_fail, hbm_mode, min_fail, criteria_table, Max_Variation,threshold,spots_2,data_cell);
        cnt_fails = cnt_fails + fail;
        %% CALCULATE R_ON
        [start_index, stop_index, fit_neu, R_dyn] = Rdyn(imax, imin, umin, umax, data_matrix, It2, Vt2);
        if ~isnan(imax2) || ~isnan(imin2) || ~isnan(umin2) || ~isnan(umax2)
            [start_index2, stop_index2, fit_neu2, R_dyn2] = Rdyn2(imax2, imin2, umin2, umax2, data_matrix, It2, Vt2);
        else
            start_index2 = NaN;
            stop_index2 = NaN;
            fit_neu2 = NaN;
            R_dyn2 = NaN;
        end
        
        %% READ csvFILE PRE SWEEP
        % does a presweep or postsweep exist for the current
        [VV, IA, VVpost, IApost, presweep_exists, postsweep_exists] = check_sweep(sPath{k});
        
        % Write the results in the latex file
        if presweep_exists || postsweep_exists
            % In case sweeps exist it is ploted 
            names{cnt_sample_plots} = ['Leakage_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];
            cnt_sample_plots = cnt_sample_plots + 1;
            names{cnt_sample_plots} = ['Stress_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];
            %[Vt_1, Vhold_x] = write_sample_latex(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, VV, IA, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, fid2, strContfail, nonfail, hbm_at_it2, VVpost, IApost, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name, Test_Journal, i, pulseshape, pulsetime, start_index2, stop_index2, fit_neu2, R_dyn2);
            [Vt_1, Vhold_x] = write_sample_latex(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, VV, IA, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, csvsave_dir, strContfail, nonfail, hbm_at_it2, VVpost, IApost, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name, Test_Journal, k, start_index2, stop_index2, fit_neu2, R_dyn2,genParam);
            i = i + 1;
        else
            names{cnt_sample_plots} = ['Stress_' num2str(cnt_sample_plots) '_' pathstrc(1,k).pathfields{1,indx-2} '_' pathstrc(1,k).pathfields{1,indx-1} '.png'];
            %% if NO pre_sweep and NO post sweep
            %  [Vt_1, Vhold_x] = write_sample_latex_no_sweep(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, fid2, strContfail, nonfail, hbm_at_it2, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name);
            [Vt_1, Vhold_x] = write_sample_latex_no_sweep(fileID, plotID, hbm_mode, snapback_values, data_matrix, start_index, stop_index, crit_spot, fit_neu, Calc_case, It2, Vt2, R_dyn, VHBMlevel, Die, ModuleDie1, DeviceName, IforV, VforI, csvsave_dir, strContfail, nonfail, hbm_at_it2, V_HBM_fail, Vbreakdown_checkbox,Option_Vbreakdown, cnt_sample_plots, k, imin, criterion_mode, criteria_matrix, Value_table, folder_name, Test_Journal, k, genParam);
            i = i + 1;
        end
        % list of fail and pass
        if strcmp(TEST, 'TLP')
            result_table = Vt2 ;
            result_journal = It2;
        else
            result_table = fail_pulse;
            result_journal = fail_pulse;
        end
        if strcmp(nonfail, 'failed')
            list_fail{k,1} =  all_dies{k};
            list_fail{k,2} = 'red' ;
            Test_Journal{k,3} = result_journal; %for the test journal
        elseif strcmp(nonfail,'passed')
            list_fail{k,1} = all_dies{k} ;
            list_fail{k,2} = 'green' ;
            Test_Journal{k,3} = 'Pass';%for the test journal
        else
            list_fail{k,1} = all_dies{k} ;
            list_fail{k,2} = 'white' ;
            Test_Journal{k,3} = 'None';%for the test journal
        end
        DeviceName_list{k} = DeviceName;
        
        % Save the processed values in Test_Journal
        Test_Journal{k,7} = It2;
        Test_Journal{k,8} = result_table;
        if snapback_values.eval_snapback
            Test_Journal{k,9} = Vt_1;
            Test_Journal{k,10} = Vhold_x;
            Test_Journal{k,11} = R_dyn;
        end
        % TLP_data.csv is empty
    end
    %% delete the empty rows of the Test_Journal and the lists of dies
    result_check_empt = find(cellfun(@isempty,Test_Journal(:,8)));
    if ~isempty(result_check_empt)
        Test_Journal(result_check_empt,:) = [];
        all_dies(result_check_empt) = [];
    end
    %% ANALYZE DATA - GENERATE STATISTIC
    [parameter_values, stat_values, device_name, angel, space_str,dev_name_stat] = extract_statistic_values(csvsave_dir, hbm_var, hbm_mode, snapback_values.eval_snapback, fileID,example_data_matrix,Limit);
    statistics(csvsave_dir, dir_structure, hbm_mode, snapback_values.eval_snapback, parameter_values, stat_values, device_name, angel, plotID, cnt_sample_plots, space_str, folder_name, Vhold1x,pathstrc,plotID_stat,dev_name_stat) %sPath\IDstat added by chefai
    %names{cnt_sample_plots + 1} = 'Statistik.png';
    
    % Store names of the plots to use it later in the latex file
    names{cnt_sample_plots + 1} = 'Statistik_Vt2.png';
    names{cnt_sample_plots + 2} = 'Statistik_It2.png';
    names{cnt_sample_plots + 3} = 'Statistik_R_on.png';
    names{cnt_sample_plots + 4} = 'Statistik_HBM.png';
    names{cnt_sample_plots + 5} = 'CDF.png'; % This holds 4 
    
    %% Print summary file
    if first_run==1
        fprintf(summary, '%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\r\n', 'Technology',' Device Name',' Los_Nr.',' #Wf',' Reticle',' Chip',' Pin',' Samples',' Mean_Vt2[V]',' Mean_It2[A]',' Mean_HBM[V]',' Mean_Rdyn[Ohm]',' Min_Vt2[V]',' Min_It2[A]',' Min_HBM[V]',' Min_Rdyn[Ohm]',' Max_Vt2[V]',' Max_It2[A]',' Max_HBM[V]',' Max_Rdyn[Ohm]',' Sigma_Vt2[V]',' Sigma_It2[A]',' Sigma_HBM[V]',' Sigma_Rdyn[Ohm]');
    end
    
    fprintf(summary, '%s;%s;%s;%s;%s;%s;%s;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f;%8.2f\r\n', Technology ,  DeviceName ,  Lot ,  wafer_number ,  Reticle ,  Chip ,  Pin ,  size(ALLSAMP,2) , stat_values.meanV ,  stat_values.meanI ,  stat_values.meanHBM ,  stat_values.meanRdyn ,  stat_values.minV , stat_values.minI ,  stat_values.minHBM ,  stat_values.minRdyn ,  stat_values.maxV , stat_values.maxI ,  stat_values.maxHBM ,  stat_values.maxRdyn ,  stat_values.stdV ,  stat_values.stdI ,  stat_values.stdHBM ,  stat_values.stdRdyn);
    
    
    %% WAFERMAP
    if Wafermap_Checkbox == 1
        disp('Wafermap')
        if ~isnan(die_x * die_y * subdie_x * subdie_y)
            plotsave_dir=[dir_structure '\WafermapPlots\'];
            mkdir(plotsave_dir);
            wafermap_sandy2(die_x, die_y, subdie_x, subdie_y, offset_x, offset_y, ALLSAMP, parameter_values, hbm_mode, plotsave_dir, notch_pos, dir_structure, colorbar_values, genParam, snapback_values.eval_snapback, target_values, critical_area, cnt_fails);
            
        else
            % Incase the size of the Wafermap is not set jet
            choice = questdlg('Wafermap-Eingabe ist unvollst�ndig! M�chten sie das Programm fortsetzen oder abbrechen?',...
                'Falsche Ordnerstruktur',...
                'Weiter','Abbrechen', 'Weiter');
            % Handle response
            switch choice
                case 'Weiter'
                    path_wafermap_texfile = [dir_structure '\wafermap.tex'];
                    fileIDwm = fopen(path_wafermap_texfile, 'w');
                    fclose(fileIDwm);
                case 'Abbrechen'
                    return;
            end
        end
    end
    %% Generate Report
    disp('Report')
    
    % Generate atable as an overview which is added on the first page
    mktable(id, all_dies, dir_structure,list_fail,Test_Journal,Wafermap_Checkbox,TEST,DeviceName_list,snapback_values.eval_snapback)
    
    % Compile the pdf of the report 
    generate_pdf(fileID, plotID, dir_structure, cnt_sample_plots, names, save_pngs,folder_name,plotID_stat)
    
    % End the waitbar
    waitbar(1);
    
    % create Test Journal
    if test_journal
        createExcelSummaryTable(Test_Journal,dir_structure,DeviceName);
    end
    
    delete(h)
    % Check if report has been generated
    [~,~,sPath5] = dirr(char(PathName_save),'\Measurement_Report.pdf\>','name');
    if isempty(sPath5)
        warn_dlg = warndlg (['Measurement Report f�r Pfad Nummer ' num2str(pathNr) ' konnte nicht generiert werden.'], 'Data Conflict');
        waitfor(warn_dlg);
    end
    
    % Copy measurement report to path
    if copy_report
        if strcmp(report_path ,'Copy "Measurement Report" to...')
            msgbox('Path for Copy "Measurement Report" is missing');
        else
            mkdir(report_path);
            if isempty(report_name)
                report_path = [report_path '\' folder_name '.pdf'];
            else
                report_path = [report_path '\' report_name '.pdf'];
            end
            measurement_report_path = [dir_structure '\' folder_name '_Measurement_Report.pdf'];
            copyfile (measurement_report_path,report_path);
        end
    end
catch
    %% Error handling
    ErrorMessage=lasterr;
    errordlg(ErrorMessage)
    F = findall(0,'type','figure','tag','TMWWaitbar');
    delete(F);
end
end