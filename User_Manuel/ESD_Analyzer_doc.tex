\documentclass{article}
\usepackage{geometry}
\geometry{a4paper, top=25mm, left=25mm, right=25mm, bottom=30mm,
headsep=10mm, footskip=12mm}
\usepackage[autostyle=true]{csquotes}
\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{caption}
\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo}
\author{Rudolf Dollinger, Jayesh Jani [IFAG DES DFE EDI ESD]}
%\author{Jayesh Jani [IFAG DES DFE EDI ESD]}
%\date{\today}
\DeclareMathOperator*{\argmax}{arg\,max}
\newcommand*{\fullref}[1]{\hyperref[{#1}]{\autoref*{#1} \nameref*{#1}}}
\usepackage[colorlinks,
pdfpagelabels,
pdfstartview = FitH,
bookmarksopen = true,
bookmarksnumbered = true,
linkcolor = black,
plainpages = false,
hypertexnames = false,
citecolor = black] {hyperref}
\title{ESD Analyzer Software}

\begin{document}
\maketitle
\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/the_difference.png}
\end{center}
\newpage
\tableofcontents 
\newpage
\section{Installation}
\begin{enumerate}
	\item \textbf{Install Matlab Runtime Distribution:}
	\begin{itemize}
		\item Search for Matlab in ISCv7 and install MATLAB Component Runtime R2015b (Windows 7)
		\begin{center}
		\includegraphics[width=1\textwidth]{imgs/runtime.png}
		\end{center}
		\item Search for Matlab in IFX App Store and install MATLAB Component Runtime (Windows 10)
		\begin{center}
		\includegraphics[width=1\textwidth]{imgs/runtime10.png}
		\end{center}		

	\end{itemize}
	\item \textbf{Copy \enquote{mti}-folder and set environment variable:}
	\begin{itemize}
		\item Copy the folder \enquote{mti} to a selected local folder on your computer
		\begin{center}
		\includegraphics[width=1\textwidth]{imgs/mti.png}
		\end{center}
		\item For Windows 7: Select System from the Control Panel, selecting Advanced system settings, and clicking Environment Variables.
		\item For Windows 10: Activate iARM. Select System and Security from the Control Panel, selecting System, and clicking Environment Variables.
		\item In German: rechts Klick auf Windows $\rightarrow$ System $\rightarrow$ Systeminfo $\rightarrow$ Erweiterte Systemeinstellungen $\rightarrow$ Erweitert $\rightarrow$ Umgebungsvariablen…
		\item Set environment variable PATH to the selected local folder:\\
		 \enquote{...\textcolor{black}{\url{\ mti\ texmfs\ install\ miktex\ bin}}}\\
		 
		 \includegraphics[width=.6\textwidth]{imgs/variables}
	\end{itemize}
	\item \textbf{Install ESD\_Analyzer:}
	\begin{itemize}
		\item Activate iARM (enable admin rights on your computer)
		\item Install software by right click on installation file \enquote{ESD\_Analyzer.exe} and selecting \enquote{install as admin}
		\item Start ESD Analyzer
	\end{itemize}
	\item \textbf{Point and Comma Setting}
	\begin{itemize}
		\item Pdf Reports will not be generated with English language settings
		\item Decimal Symbol has to be ,
		\item Digit grouping has to be .
		\item Windows 10: Settings $\rightarrow$ Time and language

		\includegraphics[width=1\textwidth]{imgs/Spracheinstellung.png}

	\end{itemize}
\end{enumerate}

\newpage
\section{Overview}
The ESD Analyzer Software is a tool for evaluating TLP and HBM measurement data.
It has many setting capabilities to make the evaluation suitable for your data.
The software generates a measurement report where all results are collected and represented graphically.
The report is produced with Latex.

The graphical user interface is devided into seven parts, which will be explained in the following sections.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/gui_new_version.png}
\end{center}
\newpage
\section{Data List}
In the first section \textit{\textbf{Data List}} you can load the measurement data you want to evaluate and make other settings, which are explained in the follwoing.

\begin{center}
\includegraphics[width=1\textwidth]{imgs/data_new_version.png}
\end{center}

\subsection{Restore settings}
Under \textbf{\textit{Restore last settings}} you can load the settings you made in a previous session. The setting file is always named  state.mat and by default located in the same folder as the evaluation. 
A state.mat file is only created when saving a session.
You can also specify an individual path for saving your settings.
A more detailed description of saving the GUI settings can be found in section \hyperref[sec:save_settings]{\color{red}{6 Save Setting File}}

%{\color{red}{\fullref{sec:save_settings}}}

\subsection{Specifiy measurement data}
There are two possibilities to load your data:
\begin{enumerate}

\item \textbf{Load data paths from text file}: If you have saved the paths to your .csv-files in a text file you can specify this text file by clicking on \textbf{\textit{Load Path-File}} . It is important to enable \textbf{\textit{Load data path from text file}}, otherwise the program will ignore the text file and read data specified in the table.
The program generates only one measurement report for all files specified in the text file.

\item \textbf{Add Path}: By clicking on the green button \textbf{\textit{Add Path}} a modal dialog box displays that lists folders in the current directory and enables you to select or enter the name of a folder (see figure below).It is possible to filter the selected directory. As you can see in the figure below, it filters all the folders which don't contain a folder or subfolder named "Pixel". Once a folder is selected it will be added to the data list table.

It is possible to read in multiple paths, for each path a single report will be generated. The selected folders will show up in the table like in the picture above. Additionally, you can type a filter in the 2nd column in order to evaluate only certain devices.
The software looks for all .csv-files in the specified directory and subdirectories recursively.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/add_path.png}
\end{center}
If you want to remove a path from the table you can do this by selecting the appropriate line and pushing the red \textbf{\textit{Delete Path}} button.

\end{enumerate}

\subsection{Filter}
It is possible to filter the given paths and thus by typing the filter name under the filter section in the dala list table (as shown in the following figure).
\begin{center}
\includegraphics[width=1\textwidth]{imgs/filter.png}
\end{center}
\textbf{Syntax}
\begin{itemize}
	\item *A\textbf{,}B : Filter after A \textbf{or} B
	\item *A\textbf{+}B : Filter after A \textbf{and} B
\end{itemize}

\subsection{Folder Name}
As already mentioned the program generates one report for each path listed in the table. Each report will be saved to an own folder. By default the folders will be named with \enquote{Report Name} that you can specify in the third column of the table. If this is left empty, the default name will be given by \enquote{Path\textunderscore Nr} as in the picture below:
\begin{center}
\includegraphics[width=.5\textwidth]{imgs/path_name2.png}
\end{center}
You can change the name in the popupmenu \textbf{\textit{Folder Name}} and choose one of the listed attributes:
\begin{center}
\includegraphics[width=.4\textwidth]{imgs/path_name3.png}
\end{center}
The chosen folder name is read from the TLP\textunderscore data.csv so make sure the appropriate attribute is available.
\begin{center}
\includegraphics[width=.5\textwidth]{imgs/path_name4.png}
\end{center}
If the selected term is not specified in the TLP\textunderscore data.csv, the folders again will be named with \enquote{Path\textunderscore Nr}.

\newpage
\section{Settings}
\begin{center}
\includegraphics[width=1\textwidth]{imgs/settings_new_version.png}
\end{center}
In this section you can specify the test type (HBM or TLP) and choose a criterion for detecting the failure of a device.

Basically, there are two types of criteria:
\begin{enumerate}
\item The first one uses a percentage deviation for detecting a failure, which can be specified in \textbf{\textit{Extraction Threshold-Delta-Spots}} or \textbf{\textit{Limit Max Variation}}.

\item The other type uses an absolute value as threshold which can be specified in \textbf{\textit{Abs (Limit)}}.
\end{enumerate}

In the following, all extraction criteria are explained in detail.

\begin{center}
\includegraphics[width=0.75\textwidth]{imgs/Kriterien.png}
\end{center}

\newpage
\subsection{HBM Mode}
By default your measurements are evaluated as TLP meausurements. You can set a HBM evaluation by enabling \textbf{\textit{HBM Mode}}.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/settings_new_version_hbm.png}
\end{center}
The differences between the two modes regarding the measurement report appear at:
\begin{itemize}
\item \textbf{I-V Plots}: Instead of the measured TLP voltage, in HBM mode the charging voltage is plotted
\item \textbf{Statistics}: In TLP Mode the HBM correlation is caculated by $\frac{3}{2}~\cdot~1000~\cdot~IT_2$, whereby in HBM Mode the critical point of the charging voltage is evaluated for statistics
\end{itemize}

When choosing HBM, you have to specify which type of data (\enquote{HPPI} or \enquote{GTS}) does the software have to process.

\subsection{Criterions}

\subsubsection{Overview Table}
\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/Criteria_Overview.png}
\end{center}


\subsubsection{Import from Measurement Data}
This criterion applies the settings which were used during measurement.
Therefore, the absolute values from the TLP Software \enquote{Spot1 Limit Max}, \enquote{Spot1 Limit Min}, \enquote{Spot2 Limit Max} and \enquote{Spot2 Limit Min} are extracted from \enquote{TLP\_data.csv}.
The algorithm then extracts all values which contains the \enquote{Min Limit} and the \enquote{Max Limit} of each spot vector. The minimum of the extracted values corrensponds to the critical spot.

If none of the limits are exceeded, the device status is set to \enquote{pass}.

%\newpage
\subsubsection{Spot 1 Delta Mean/Spot 2 Delta Mean}
The \textit{Delta Mean} extraction criterion detects the devices failure by evaluating the deviation to the first spot value. 
First, the difference between the first spot value and all other spot values is calculated. The mean of all differences is used as the reference value. 
The program then goes through all differences. 
If a value differs more than a percentage (given by the user) of the overall mean difference, the failure of the device is recognized. 
In \enquote{Number of min. fails} you can specify the minimum number of failed devices that causes a test fail. If for example the \enquote{Number of min. fails} is set to 2, a test failure will occur only if 2 consecutives deviations have been detected and thus a mistake in measurement would be ignored.

\begin{algorithm}[ht]
\caption{Spot Delta Mean}\label{euclid}
\begin{algorithmic}[1]
\State \textbf{Input: }\text{Spots vector: $\boldsymbol{s}$, Extraction Threshold: th$_\text{extract}$}
\ForEach{\text{spot value} $s_i$}
\State ${d_i} = \text{abs}(s_1-s_i)$;
\EndFor
\ForEach{$d_i$}
	\If {$d_i >$ th$_\text{extract}\cdot\text{mean}(\boldsymbol{d})$}
		\State \text{counter}$_\text{fail}$ = \text{counter}$_\text{fail} + 1$
		\If {\text{counter}$_\text{fail}$ = \text{Number of min. fails}}
			\State \text{spot}$_\text{crit} = i - 1$
			\State \text{status = failed}
			\State \textbf{break}
		\EndIf
	\EndIf
\EndFor
\end{algorithmic}
\end{algorithm}

\newpage
\subsubsection{Two Spots Delta}
The two spot delta criterion uses the difference between the first values of the two spot vectors as reference value.
Instead of one spot vector now the difference between the two spot vectors ${\boldsymbol{d}_{12}}$ is used for failure detection.
As soon as ${\boldsymbol{d}_{12}}$ differs more than a percentage (given by the user) of the reference value, the failure of the device is recognized.
\begin{algorithm}[ht]
	\caption{Two Spots Delta}\label{twoSpotsMean}
	\begin{algorithmic}[1]
		\State \textbf{Input: }\text{Spot vectors: $\boldsymbol{s_1}, \boldsymbol{s_2}$, Extraction Threshold: th$_\text{extract}$}
		
		\State{Calculate difference between spot vectors:}
		\State ${\boldsymbol{d}_{12}} = \text{abs}(\boldsymbol{s_1} - \boldsymbol{s_2})$;
		
		\State{Calculate difference between the first values as reference:}
		\State ${\text{ref}_{12}} = \text{abs}(\boldsymbol{s_{1_1}} - \boldsymbol{s_{2_1}})$;
		\ForEach{$d_{12_i}$}
			\If {$(d_{12_i} - {\text{ref}_{12}}) > \text{th$_{\text{extract}}$} \cdot {\text{ref}_{12}}$}
				\State \text{counter}$_\text{fail}$ = \text{counter}$_\text{fail} + 1$
				\If {\text{counter}$_\text{fail}$ = \text{Number of min. fails}}
					\State \text{spot}$_\text{crit} = i - 1$
					\State \text{status = failed}
					\State \textbf{break}
				\Else
					\State \text{spot}$_\text{crit} = \text{length data}$
				\EndIf
			\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}



\newpage
\subsubsection{Spot1 Delta Prevalue/Spot2 Delta Prevalue}
This criterion evaluates the difference of a spot value to its predecessor spot value. For that reason the differences between successive vector values are calculated. If a value differs more than a given percentage from its predecessor, the device is detected as broken. A test failure will occur once two successives deviations have been detected.
\begin{algorithm}
\caption{Spot Delta Prevalue}\label{prevalue}
\begin{algorithmic}[1]
\State \textbf{Input: }\text{Spots vector: $\boldsymbol{s}$, Extraction Threshold: th$_\text{extract}$}
\State{Calculate difference between successive vector values:}
\ForEach{\text{spot value} $s_i$}
\State ${d_i} = \text{abs}(s_i-s_{i+1})$;
\EndFor
\ForEach{$d_i$}
	\State $\nu = \text{th$_{\text{extract}}$} \cdot abs(s_i)$
	\If {$d_i > \nu$ and $d_{i+1} > \nu$}
	\State \text{spot}$_\text{crit} = i$
	\State \text{status = failed}
	\State \textbf{break}
\Else
	\State \text{spot}$_\text{crit} = \text{length data}$
	\EndIf
\EndFor
\end{algorithmic}
\end{algorithm}
%\begin{figure}[h]
%\centering
\begin{center}
\includegraphics[page=6]{imgs/Plots2.pdf}
\end{center}
%\caption{Example: Spot2-Delta Prevalue with a threshold of 5 \%. }
%\end{figure}

\newpage
\subsubsection{Two Spots Delta Prevalue}
Now both spot vectors are taken into account by calculating the difference between the two spot vectors which is saved to ${\boldsymbol{d}_{12}}$. Then each difference value $d_{12_{i+1}}$ is compared to its predecessor value $d_{12_{i}}$. If the difference between these two is greater than a percentage specified by the user the device failure is detected.
\begin{algorithm}
	\caption{Two Spots-Delta-Prevalue}\label{twoSpotsPrevalue}
	\begin{algorithmic}[1]
		\State \textbf{Input: }\text{Spot vectors: $\boldsymbol{s_1}, \boldsymbol{s_2}$, Extraction Threshold: th$_\text{extract}$}
		
		\State{Calculate difference between spot vectors:}
		\State ${d_{12}} = \text{abs}(\boldsymbol{s_1} - \boldsymbol{s_2})$;
		\ForEach{$d_i$}
			\If {$\text{abs}(d_{12_i} - d_{12_{i+1}}) > \text{th$_{\text{extract}}$} \cdot d_{12_i}$}
				\State \text{spot}$_\text{crit} = i$
				\State \text{status = failed}
				\State \textbf{break}
			\Else
				\State \text{spot}$_\text{crit} = \text{length data}$
			\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}

%\newpage
\subsubsection{Voltage Peak}
This criterion detects the device failure at the maximum voltage value.
\begin{algorithm}
\caption{Voltage Peak}
\begin{algorithmic}[1]
\State \textbf{Input: }\text{Voltage vector $\boldsymbol{V}$}
\State $V_\text{max} = \text{max}(V)$
\State \text{spot}$_\text{crit} = \text{Index of } V_\text{max}$
\end{algorithmic}
\end{algorithm}
\begin{center}
\includegraphics[page=20]{imgs/Plots1.pdf}
%\caption{Example: Voltage Peak}
\end{center}

\newpage
\subsubsection{Spot1 Min Limit/Spot2 Min Limit}
All Min/Max criteria are absolute criteria. 
The Min Limit criterion detects the first spot value which is smaller than the limit given by the user.
\begin{algorithm}
	\caption{Min Limit}\label{minLimit}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Spot vector: $\boldsymbol{s}$, Extraction Threshold: Limit$_{\text{min}}$
		\State Find \textbf{first} spot value which is less than Limit$_{\text{min}}$
		\State spot$_\text{crit} = \text{find(abs(}\boldsymbol{s}) \leq \text{Limit$_{\text{min}}$}) - 1$
		\State status = failed
		\If{isempty(spot$_\text{crit}$)}
			\State spot$_\text{crit} = $ length data
			\State status = passed
		\EndIf
	\end{algorithmic}
\end{algorithm}

\subsubsection{Spot1 Max Limit/Spot2 Max Limit}
This criterion searches for the first spot value which is greater than the specified limit. If the leakage does not exceed this threshold, the device status is set to passed.
\begin{algorithm}
	\caption{Max Limit}\label{maxLimit}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Spot vector: $\boldsymbol{s}$, Extraction Threshold: Limit$_{\text{max}}$
		\State Find \textbf{first} spot value which is greater than Limit$_{\text{max}}$
		\State spot$_\text{crit} = \text{find(abs(}\boldsymbol{s}) \geq \text{Limit$_{\text{max}}$}) - 1$
		\State status = failed
		\If{isempty(spot$_\text{crit}$)}
			\State spot$_\text{crit} = $ length data
			\State status = passed
		\EndIf
	\end{algorithmic}
\end{algorithm}
\begin{center}
\includegraphics[page=2]{imgs/imgs/Path1/Plots.pdf}
\end{center}

\newpage
\subsubsection{Spot12 Min/Spot12 Max Limit}
This criterion evaluates both spot vectors. As for the last two criterions the first value which is less (for the Min criterion) or greater (for the Max criterion) than the absolute limit, given by the user, is detected. The spot value which exceeds the limit first is choosen as the failure spot.
\begin{algorithm}
	\caption{Spot12 Min/Max Limit}\label{minmaxLimit}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Spot vectors: $\boldsymbol{s_1}$, $\boldsymbol{s_2}$, Extraction Threshold: Limit$_{\text{min}}$
		\State Find \textbf{first} spot value of $\boldsymbol{s_1}$ which is less/greater than Limit
		\State c1$ = \text{find(abs(}\boldsymbol{s}) \lessgtr \text{Limit$_{\text{min}}$}) - 1$
		
		\State Find \textbf{first} spot value of $\boldsymbol{s_2}$ which is less/greater than Limit
		\State c2$ = \text{find(abs(}\boldsymbol{s}) \lessgtr \text{Limit$_{\text{max}}$}) - 1$
		\State spot$_\text{crit} =$ min(c1, c2)$ - 1$
		
		\State status = failed
		\If{isempty(spot$_\text{crit}$)}
			\State spot$_\text{crit} = $ length data
			\State status = passed
		\EndIf
	\end{algorithmic}
\end{algorithm}

\subsubsection{Spot1-Prevalue variation/Spot2-Prevalue variation}
This criterion evaluates the variation of the difference between each spot value and its predecessor spot value. For that reason the normalized differences between successive vector values are calculated. If the normalized difference-value exceeds a given percentage , the device is detected as broken. A test failure will occur once two successives deviations have been detected.
\begin{algorithm}
\caption{Spot Prevalue variation}\label{prevalue}
\begin{algorithmic}[1]
\State \textbf{Input: }\text{Spots vector: $\boldsymbol{s}$, Limit Max Variation: max$_\text{var}$}
\State{Calculate normalized difference between successive vector values:}
\State ${s} = \dfrac{s}{s_1}$;
\ForEach{\text{spot value} $s_i$}
\State ${d_i} = \text{abs}(s_i-s_{i+1})$;
\EndFor
\ForEach{$d_i$}
	\If {$d_i > max_\text{var}$ and $d_{i+1} > max_\text{var}$}
	\State \text{spot}$_\text{crit} = i$
	\State \text{status = failed}
	\State \textbf{break}
\Else
	\State \text{spot}$_\text{crit} = \text{length data}$
	\EndIf
\EndFor
\end{algorithmic}
\end{algorithm}
%\begin{figure}[h]
%\centering
\begin{center}
\includegraphics[width=0.7\textwidth]{imgs/Plots2_2.png}
\end{center}
%\caption{Example: Spot1-Prevalue_variation with a Limit Max Variation of 5 \%. }
%\end{figure}

\subsubsection{Two Spots Prevalue variation}
Here both spot vectors are taken into account by calculating the normalized differences for both spot vectors (${\boldsymbol{d}_{1}}$ and ${\boldsymbol{d}_{2}}$). Then each difference value $d_{x_{i+1}}$ is compared to its predecessor value $d_{x_{i}}$. If one of the spots presents a failure, the device fails.

\begin{algorithm}
	\caption{Two Spots Prevalue variation}\label{twoSpotsPrevalue}
	\begin{algorithmic}[1]
		\State \textbf{Input: }\text{Spot vectors: $\boldsymbol{s_1}, \boldsymbol{s_2}$, Limit Max Variation: max$_\text{var}$}
		\State{Calculate normalized differences between successive vector values:}
		\State ${s_\text{vect1}} = \dfrac{s}{s_{\text{vect1}_1}}$;
		\State ${s_\text{vect2}} = \dfrac{s}{s_{\text{vect2}_1}}$;
		\ForEach{\text{spot value} $s_{\text{vect x}_i}$}
			\State ${d_{1_i}} = \text{abs}(s_{\text{vect1}_i}-s_{\text{vect1}_{i+1})}$;
			\State ${d_{2_i}} = \text{abs}(s_{\text{vect2}_i}-s_{\text{vect2}_{i+1})}$;
		\EndFor
\ForEach{$d_{x_i}$}
	\If {($d_{1_i} > max_\text{var}$ and $d_{1_{i+1}} > max_\text{var}$) or ($d_{2_i} > max_\text{var}$ and $d_{2_{i+1}} > max_\text{var}$)}
	\State \text{spot}$_\text{crit} = i$
	\State \text{status = failed}
	\State \textbf{break}
\Else
	\State \text{spot}$_\text{crit} = \text{length data}$
	\EndIf
\EndFor
	\end{algorithmic}
\end{algorithm}

\subsubsection{No Spots}
You can select this criterion if there are no spots and there is thus no failure analysis. Here the result is set to default( \enquote{pass} ) and without further calculation.

\subsection{Contact Fail}
Contact fail criteria is to check the needle contacts during measurement. If all measured TLP current are below the Contact Fail Limit, so the result of the contact fail Criteria is FAIL. 
If one or more of the TLP currents are above the limit, the result ist PASS. For example in below figure it is shown that if TLP current is below 100mA then it will be detected as Contact fail. In Evaluation report it will be shown FAIL.
\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/Contact_Fail.png}
\end{center}

\section{Wafermap}
A Wafermap basically is a colormap where the data is represented by colors. A colorbar indicates the mapping of data values into the colormap.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/Wafermap.png}
\end{center}

To generate a wafermap of your measurement data mark the \textbf{\textit{Enable Wafermap}} checkbox and click on  \textbf{\textit{Auto Detection}} which give you an approximative matrix size based on the coordinates of the selected devices and that you can adjust manually. 

If you don't need to make any special settings of the colorbar the left column \textbf{\textit{Wafer Size}} is the only one you need to fill.

\subsection{Wafer Size}
\begin{itemize}
\item \textbf{Matrix Dies}: Number of dies in x- and y-direction
\item \textbf{Matrix Subdies}: Number of subdies in x- and y-direction
\item \textbf{Offset}: Shifting of the origin die in x- and y-direction
\item \textbf{Notch Position}: The notch can be drawn on the bottom, the right, the left or the top.
\end{itemize}
\begin{figure}[ht]
\includegraphics[width=0.9\textwidth]{imgs/imgs/Wafermaps/Path_1/WafermapPlots/WafermapIt_2_size.png}
\caption*{Wafermap with Matrix Dies: $8 \times 11$, Matrix Subdies: $ 1\times 1$, Offset: $3 \times 6$}
\end{figure}
\begin{figure}
\includegraphics[width=0.9\textwidth]{imgs/WafermapVt_2.png}
\caption*{Wafermap with Matrix Dies: $10 \times 10$, Matrix Subdies: $3 \times 4$ \newline Offset: $5 \times 5$}
\end{figure}

\subsection{Colorbar Range}
The range of the colorbar is set per default automatically and is defined by the minimal and maximal value.
The range can be defind individually for each parameter by typing a minimal and maximal value. Make sure to uncheck \textit{\textbf{autom.}} as well.

\subsection{Target Value and Critical Area}
In \textbf{\textit{Target}} you can define a hard limit for the colorbar for representing a \enquote{bad} and \enquote{good} area. All values which are smaller than the target value are displayed in  red shades and all values above the target are displayed in green shades. 

Additionally, a critical area can be defined by specifying a percentage deviation of the target value. This area is displayed in yellow shades.

\begin{figure}
\includegraphics[width=.9\textwidth]{imgs/imgs/Wafermaps/Path_target2/WafermapPlots/WafermapIt_2.png}
\caption*{Wafermap with target value: 2.25 and critical area: 1\%}
\end{figure}

\newpage
\section{Correlations}
In section \textbf{\textit{Correlations}} you can evaluate the voltage at a specific current value, or vice versa the current at a specific voltage value. The reference values can be defined in \textbf{\textit{Idef}} and \textbf{\textit{Vdef}}.
Additionaly, the clamping voltage at the HBM-voltage-level can be calculated according to the TLP It2 HBM correlation $I_{HBMref}[A] = \frac{2}{3} \cdot V_{HBMlevel}[kV]$. The HBM target level can be specified in \textbf{\textit{$V_{\text{HBMLevel}}$}}.

\begin{center}
\includegraphics[width=1\textwidth]{imgs/correlation.png}
\end{center}

If the specified current/voltage range exceeds the measured values the returned value for $I_{\text{at}V}$, $V_{\text{at}I}$, and $V_{\text{clamp}}$ will be NaN.

The corresponding value is calculated using linear interpolation. Algorithms \ref{IatV}, \ref{VatI} and \ref{Vclamp} show the calculation of $I_{\text{at}V}$ (for $I_{t2} > 0$), $V_{\text{at}I}$ (for $I_{t2} < 0$) and $V_{\text{clamp}}$ (for $I_{t2} > 0$).

\begin{algorithm}[ht]
	\caption{Calculate $I_{\text{at}V}$ \textbf{if} $I_{t2} > 0$}\label{calcIatVpos}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Voltage vector: $\boldsymbol{V}$, Current vector: $\boldsymbol{I}$

		\ForEach{k}
			\If{$V_k \leq V_{\text{for}I} \text{ and } V_{\text{for}I} \leq V_{k+1}$}
			\State $I_{\text{at}V} = \frac{V_{\text{for}I} - \boldsymbol{V}_k}{\boldsymbol{V}_{k+1} - \boldsymbol{V}_k} \cdot (\boldsymbol{I}_{k+1} - \boldsymbol{I}_k) + \boldsymbol{I}_k$
			\State \textbf{break}
			\EndIf
		\EndFor
	\end{algorithmic}
	\label{IatV}
\end{algorithm}

\begin{algorithm}[ht]
	\caption{Calculate $V_{\text{at}I}$ \textbf{if} $I_{t2} < 0$}\label{calcVatIneg}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Voltage vector: $\boldsymbol{V}$, Current vector: $\boldsymbol{I}$, $V_{\text{for}I}$

		\ForEach{k}
			\If{$I_k \leq I_{\text{for}V} \text{ and } I_{\text{for}V} \leq I_{k+1}$}
			\State $V_{\text{at}I} = \frac{I_{\text{for}V} - \boldsymbol{I}_k}{\boldsymbol{I}_{k+1} - \boldsymbol{I}_k} \cdot (\boldsymbol{V}_{k+1} - \boldsymbol{V}_k) + \boldsymbol{V}_k$
			\State \textbf{break}
			\EndIf
		\EndFor
	\end{algorithmic}
	\label{VatI}
\end{algorithm}

\begin{algorithm}[ht]
	\caption{Calculate $V_{\text{clamp}}$ \textbf{if} $I_{t2} > 0$}\label{calcVclamppos}
	\begin{algorithmic}[1]
		\State \textbf{Input: }Voltage vector: $\boldsymbol{V}$, Current vector: $\boldsymbol{C}$, $V_{\text{HBMLevel}}$
		\State $I_{HBMref}=\frac{2}{3} \cdot V_{HBMlevel}$
		\ForEach{k}
			\If{$I_{HBMref} \leq I_k \text{ and } I_{HBMref} \geq I_{k+1}$}
			\State $V_{\text{clamp}} = \frac{V_{\text{HBMref}} - \boldsymbol{I}_k}{\boldsymbol{I}_{k+1} - \boldsymbol{I}_k} \cdot (\boldsymbol{V}_{k+1} - \boldsymbol{V}_k) + \boldsymbol{V}_k$
			\State \textbf{break}
			\EndIf
		\EndFor
	\end{algorithmic}
	\label{Vclamp}
\end{algorithm}

\newpage
\section{ON Resistance}
The ON resistance is calculated by fitting the I-V-curve in a specific current range or voltage range respectively and drawing the gradient triangle.
If no ranges are defined by the user, $R_\text{on}$ is calculated by fitting the points between the minimum current value (0.1A) and the value, where the critical point has been identified. By additional definition of the current and/or voltage range, you can specify the range in which the curve is fitted and $R_\text{on}$ is evaluated. Software offeres two $R_\text{on1}$ and $R_\text{on2}$ . Hence, Two current Range and Two Voltage ranges needs to be defined. If $R_\text{on2}$ is not needed then no need to define anything in current and vontage range.

\begin{center}
\includegraphics[width=0.8\textwidth]{imgs/ron_new_version.png}
\end{center}
Below one example is shown. Here  $R_\text{on1}$  is defined as current (0.2A - 0.7A) and  $R_\text{on2}$ is defined as voltage range(8V - 10V)

\begin{center}
\includegraphics[width=0.8\textwidth]{imgs/Input_4_Ron.png}
\end{center}

Then in the report it will be shown as below.
\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/Rdyn_report.png}
\end{center}
In Report IV curve is also marked with colors.  $R_\text{on1}$ is green line and  $R_\text{on2}$ magenta line as shown below.

\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/Rdyn12_IV.png}
%\includegraphics[page=2]{imgs/Plots.pdf}
\end{center}


\subsection{Holding Voltage(Vh)}
By enabling \textbf{\textit{Holding Voltage(VH)}}, the Holding voltage(Vh) will be marked in the I-V plots and listed as value in the table below the I-V Plot of each device.
Additionally, the statistics of the Holding voltage(Vh) are listed on the 2nd page of the report under \enquote{Vt$_1$}.

There are two possibilities to determine the Holding voltage(Vh).

\begin{center}
\includegraphics[width=1\textwidth]{imgs/breakdown_new_version.png}
\end{center}

\newpage
The pseudocode of algorithm \ref{holding} shows how the Holding voltage(Vh) is calculated by the \textbf{\textit{\enquote{Knickpoint}}} method.

\begin{algorithm}
\caption{Calculate Holding voltage(Vh)}\label{holding}
\begin{algorithmic}[1]
\State \textbf{Input: }\text{voltage vector: $\boldsymbol{V}$, current vector: $\boldsymbol{I}$}
\State $\boldsymbol{V}_\text{normalized} =\frac{\boldsymbol{V}}{V_\text{max}}$
\State $\boldsymbol{I}_\text{normalized} = \frac{\boldsymbol{I}}{I_\text{max}}$
%\State $\boldsymbol{V}_\text{breakdown} = \text{max}(\boldsymbol{V}_\text{normed} - \boldsymbol{I}_\text{normed})$
\State $\boldsymbol{V}_\text{holding} = V_{\argmax_i(V_{\text{normalized}}
- I{\text{normalized}}})$
\end{algorithmic}
\end{algorithm}

\begin{center}
\includegraphics[page=2]{imgs/imgs/Durchbruch/Plots_db.pdf}
\end{center}

The 2nd option fits the I-V curve in the same range you specified for calculating the ON resistance. The intersection point of the tangente and x = 0 denotes the holding voltage(Vh). The picture below demonstrates this method, which is called \textbf{\textit{Intersection w. x-axis}}.

\begin{center}
\includegraphics[page=2]{imgs/Plots_breakdown.pdf}
\end{center}

\newpage
\subsection{Snapback}
In order to include the snapback in your evaluation, mark the appropriate checkbox. 
\begin{center}
\includegraphics[width=1\textwidth]{imgs/rdyn_new_version_snapback.png}
\end{center}
The snapback value, which corresponds to the trigger voltage is currently determined by the peak of the voltage. As the latter is not identical to the maximal voltage value the snapback has to be found by the criterion:\\$x_{t-1} < x_t > x_{t+1}$. Unfortunately, a lot of points satisfy this condition. To filter smaller peaks out, you can specify a minimal snapback value.

Additionaly to the trigger voltage the holding voltage will be evaluated as well.
All extracted values will be marked in the I-V plot which can be seen in the picture below. The values of V$_{\text{trig}}$ and V$_{\text{hold}}$ will be listed in a table as well.


\begin{center}
\includegraphics[page=2]{imgs/imgs/snapback/Plots.pdf}
\end{center}

\section{Save Setting File}
\label{sec:save_settings}
There are two possibilities to save the settings you made in the GUI. The simpler one is to mark the checkbox \textbf{\textit{Save setting file}} at the end of the GUI. A file state.mat is automatically saved to the folder you specify for saving the evaluation.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/start2.png}
\end{center}

\newpage
If you want to save your setting file to another directory you can specify a path in section \textbf{\textit{Save Setting File}}.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/save_settings.png}
\end{center}

\section{Report Directory}

\begin{center}
\includegraphics[width=1\textwidth]{imgs/report_path.png}
\end{center}

\textbf{\textit{Report Directory}} is an additional save-option to facilitate the overview of the PDF results. This will gather all the measurement reports of the different tests into the same given directory. 


\section{Start Program}
In the last section you can finally start the program. Before starting you need to specify a directory where you want the evaluation to be saved.

If you want to save all plots which are generated during evaluation as png files, you need to mark the checkbox \textbf{\textit{Save plots as png files}}.

If you want to generate a test journal in Excel format where you get an overview of the test results, you need to mark the checkbox \textbf{\textit{Generate Test journal}}.
\begin{center}
\includegraphics[width=1\textwidth]{imgs/start.png}
\end{center}

Here an example of a \textbf{\textit{Test Journal}} :

\begin{center}
\includegraphics[width=0.7\textwidth]{imgs/test_journal.png}
\end{center}

After running the program you will get a folder where the report and the plots are saved. Additionally, the file \enquote{Statistics\_Summary.csv} is created. The latter contains all statistical parameters which were calculated during evaluation.
\begin{center}
\includegraphics[width=.5\textwidth]{imgs/statistics.png}
\end{center}

The evaluation folder (in this case named Path\_1) contains a lot of Latex specific files. The relevant files are \textbf{Measurement\_Report.pdf}, \textbf{Plots.pdf} and \textbf{TLP\_eval.csv}.
The folder \enquote{PNGPlots} is created only if \textbf{\textit{Save plots as png files}} has been selected. As the wafermaps are not generated with Latex but with Matlab, these files are saved seperately in \enquote{WafermapPlots}.
\begin{center}
\includegraphics[width=.8\textwidth]{imgs/eval_folder.png}
\end{center}


\section{Miscellaneous Information}
\subsection{Version Number}
For ESD Analyze Software we use version system in order to make the further development transparent. After every update a new unique Version number will be assigned. Through Standard versioning system it’s possible to keep the record of each and every version and hence, it’s possible to look at the change log of each updated version. 
\begin{center}
\includegraphics[width=.5\textwidth]{imgs/versionNum.png}
\end{center}
In Above figure one Example Version V1.1.0 is shown. Here the Syntax for version numbers are following. V(Stands for version)1(Major).1(Minor).0(Patch)

\textbf{\textit{Major:}} It will be only updated if previous version is completely different from future version

Example: If GUI surface is completely changed then major will be incremented to 2

\textbf{\textit{Minor:}} It will be only updated if there are new features updated in the future version

Example: If SOA evaluation is implemented in next version then Minor will be updated

\textbf{\textit{Patch:}} It will be only updated if there are some bug fixes performed

Example: If table in the report is NOT shown properly and bug is fixed patch will be increased 

\subsection{Release Notes}
\paragraph{V 1.1.2 (release 17.02.2020)}

\begin{enumerate}
\setcounter{enumi}{0}

\item Extraction Criteria improved (Several bug fixes, especially “Number of min. fails” for spot outliers)

\begin{center}
\includegraphics[width=0.5\textwidth]{imgs/Release_Notes_1.png}
\end{center}

\item $R_\text{on}$ is offered for two regions ( $R_\text{on1}$ and   $R_\text{on2}$)

\begin{center}
\includegraphics[width=0.7\textwidth]{imgs/Release_Notes_2.png}
\end{center}

\begin{center}
\includegraphics[width=0.7\textwidth]{imgs/Release_Notes_3.png}
\end{center}

\item Contact Fail will be detected properly (Linked to TLP current)

\item “Edit boxes” are with comments, to make it more user friendly

\item TLP-HBM correlation fixed (V@I, I@V)

\item Quick Guide and User Manual revised according to actual version
\end{enumerate}

\paragraph{V 1.0.13 (release 05.06.2019)}

\begin{enumerate}
\setcounter{enumi}{0}

\item User Manual Updated for Windows 10

\item Quick Guide revised according to actual version

\item Load Setting File bug fixed (HBM Type maybe not with saved setting files of old Version)

\item Save Report or Save setting-File bug fixed, Open the same path last used, same as the other one

\item Click in an edit box will mark or delete the example text

\item Large Report table will not get cut anymore

\item Default in Test journal fixed, now results over 1.2kV in green

\item Error List Button: Excel List with common errors (not much filled yet, but in progress)

\item Contact-fail Criterion: 1e-12 A(By default, but it can be changed by user)
\end{enumerate}

\paragraph{V 1.0.12 (Not released)}

This version doesn’t exist because of change of Software development from DALI to Sophie


\paragraph{V 1.0.11 (release 12.06.2018)}


\begin{enumerate}
\setcounter{enumi}{0}

\item “Data processing” window will be automatically closed in case of error and won’t be frozen anymore

\item Warning window will appear in case of wrong filter or a wrong path

\item Filter improvement , Device 1 + pos , Device 2 + neg

+ : AND

, : OR

“ : delimiter the filter name

\item Improvement of the CSV-Eval file (Summary Excel Table): Clear separation between the data (in Excel Table)

\item Improvement of List of Testjobs Table: Add of Rdyn,Vholdx,DeviceName...

\item NaN Values will be automatically removed from the reports
\end{enumerate}

\paragraph{V 1.0.10 (release 07.05.2018)}


\begin{enumerate}
\setcounter{enumi}{0}

\item Improvement of the add-path filter: it filters the folders according to the given filter

\item Add of new criterion “No-spots”: can be used when we don’t have any spot in measurements

\item Add of a new folder name called “Report Name”: This can be edited in the Data list table

\item Add of a “Report Directory” section: It allows us to gather all the PDFs results in a specific folder

\item Improvement of the big data analyze: no more bugs by an analyze of huge measurements

\item Improvement in wafermap: Color-bar issue solved

\item User Manual and Quick Guide have been updated
\end{enumerate}

\paragraph{V 1.0.9 (release 20.03.2018)}


\begin{enumerate}
\setcounter{enumi}{0}

\item Add of new criterion (2 Spots-Prevalue Variation), it takes both spots into account

\item Improvement of the GUI display (“Settings” section)

\end{enumerate}



\newpage

\begin{center}
\includegraphics[width=.8\textwidth]{imgs/tech_support_cheat_sheet.png}
\end{center}

\end{document}
