\contentsline {section}{\numberline {1}Installation}{3}{section.2}%
\contentsline {section}{\numberline {2}Overview}{6}{section.7}%
\contentsline {section}{\numberline {3}Data List}{7}{section.8}%
\contentsline {subsection}{\numberline {3.1}Restore settings}{7}{subsection.9}%
\contentsline {subsection}{\numberline {3.2}Specifiy measurement data}{7}{subsection.10}%
\contentsline {subsection}{\numberline {3.3}Filter}{8}{subsection.13}%
\contentsline {subsection}{\numberline {3.4}Folder Name}{8}{subsection.14}%
\contentsline {section}{\numberline {4}Settings}{10}{section.15}%
\contentsline {subsection}{\numberline {4.1}HBM Mode}{11}{subsection.18}%
\contentsline {subsection}{\numberline {4.2}Criterions}{11}{subsection.19}%
\contentsline {subsubsection}{\numberline {4.2.1}Import from Measurement Data}{11}{subsubsection.20}%
\contentsline {subsubsection}{\numberline {4.2.2}Spot 1 Delta Mean/Spot 2 Delta Mean}{11}{subsubsection.21}%
\contentsline {subsubsection}{\numberline {4.2.3}Two Spots Delta}{12}{subsubsection.23}%
\contentsline {subsubsection}{\numberline {4.2.4}Spot1 Delta Prevalue/Spot2 Delta Prevalue}{13}{subsubsection.25}%
\contentsline {subsubsection}{\numberline {4.2.5}Two Spots Delta Prevalue}{14}{subsubsection.27}%
\contentsline {subsubsection}{\numberline {4.2.6}Voltage Peak}{14}{subsubsection.29}%
\contentsline {subsubsection}{\numberline {4.2.7}Spot1 Min Limit/Spot2 Min Limit}{15}{subsubsection.31}%
\contentsline {subsubsection}{\numberline {4.2.8}Spot1 Max Limit/Spot2 Max Limit}{15}{subsubsection.33}%
\contentsline {subsubsection}{\numberline {4.2.9}Spot12 Min/Spot12 Max Limit}{16}{subsubsection.35}%
\contentsline {subsubsection}{\numberline {4.2.10}Spot1-Prevalue variation/Spot2-Prevalue variation}{16}{subsubsection.37}%
\contentsline {subsubsection}{\numberline {4.2.11}Two Spots Prevalue variation}{17}{subsubsection.39}%
\contentsline {subsubsection}{\numberline {4.2.12}No Spots}{17}{subsubsection.41}%
\contentsline {section}{\numberline {5}Wafermap}{18}{section.42}%
\contentsline {subsection}{\numberline {5.1}Wafer Size}{18}{subsection.43}%
\contentsline {subsection}{\numberline {5.2}Colorbar Range}{18}{subsection.46}%
\contentsline {subsection}{\numberline {5.3}Target Value and Critical Area}{18}{subsection.47}%
\contentsline {section}{\numberline {6}Correlations}{19}{section.49}%
\contentsline {section}{\numberline {7}Dynamic Resistance}{21}{section.53}%
\contentsline {subsection}{\numberline {7.1}Breakdown Voltage}{22}{subsection.54}%
\contentsline {subsection}{\numberline {7.2}Snapback}{24}{subsection.56}%
\contentsline {section}{\numberline {8}Save Setting File}{24}{section.57}%
\contentsline {section}{\numberline {9}Report Directory}{25}{section.58}%
\contentsline {section}{\numberline {10}Start Program}{25}{section.59}%
\contentsline {section}{\numberline {11}Miscellaneous Information}{26}{section.60}%
\contentsline {subsection}{\numberline {11.1}Version Number}{26}{subsection.61}%
