function [Vbreakdown,tdata,C] = V_hold_intersection_V_bereich(app,V_max,V_min)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Initialize Variables
s=size(app.V,2);
Vbreakdown = zeros(1,s);
app.V_hold_snap_markers=gobjects(1,size(app.measure_params,1));

% Check if the line selector is used and if so get the selected curves
if ~isempty(app.lineDialog)
    activeCurves = app.lineDialog.ActiveCurves;
else
    activeCurves = logical(ones(length(app.X),1));
end
%% For loop to calculate the V_hold for every curve
for i=1:s
    if ~isempty(app.V{1,i})&activeCurves(i)
        if all(app.I{1,i} < 0)
            %threshold = -0.10;
            threshold = -0.05;
            signe=-1;
        else
            %threshold = 0.10;
            threshold = 0.05;
            signe=1;
        end
        vector_temp=zeros(1,0);
        ind2=1;
        
        while (abs(app.V{1,i}(ind2))<=abs(V_min))
            vector_temp(1,ind2)=ind2;
            ind2=ind2+1;
        end
        start_pt = ind2-1;
        fit_max=V_max;   % Obere LIMIT Strombereich [A]
        fit_min=V_min;
        if abs(fit_min) > abs(threshold)
            start_index = find(abs(app.V{1,i})>abs(fit_min),1);
        end
        if abs(fit_max) < abs(app.V{1,i}(end-2))
            stop_index_u= find (abs(app.V{1,i})>abs(fit_max),1);
            stop_index=stop_index_u-1;
        else
            stop_index_u= find (abs(app.V{1,i})>abs(app.V{1,i}(end-2)),1);
            stop_index=stop_index_u-1;
        end
        for j=stop_index:size(app.V{1,i},1)
            if stop_index > start_pt
                vector_temp=[vector_temp j];
            end
        end
        [xData, yData] = prepareCurveData( app.V{1,i}, app.I{1,i} );
        
        ft = fittype( 'poly1' );
        excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
        opts = fitoptions( 'Method', 'LinearLeastSquares' );
        opts.Exclude = excludedPoints;
        
        [fitresult, ~] = fit( xData, yData, ft, opts );
        %V breakdown calculate
        Vbreakdown(:,i)=fzero(fitresult,0);
        %     txt = ['V_{hold}' num2str(i) ' = ' num2str(round(Vbreakdown(i),2)) 'V'];
        % text (app.axes1,0.05, 0.65-i *0.06,txt,'Units','normalized','Color',C{i},'Fontsize',18,'fontweight','bold');
        C{i} = app.X(1,i).Color; % Kann nur im IV mode aufgerufen werden
        
        % Generate Names:
        app.measure_params{i,'V_Hold'} = "Vh_"+string(Vbreakdown(i))+"V";
        
        if strcmp(app.LIST_SOA.Value,'IV Curve')
            app.V_hold_snap_markers(:,i)=plot (app.axes1,Vbreakdown(i), 0 ,'Color',C{i},...
                'marker','*','Markersize',16,'LineWidth',1.5);
        else
            app.V_hold_snap_markers(:,i)=plot (app.UIAxes,Vbreakdown(i), 0 ,'Color',C{i},...
                'marker','*','Markersize',16,'LineWidth',1.5);
        end
        
    end
end
% app.Vh_legende_string = rmmissing(plot_name);
if ~isempty(app.regular_legend)
    legende = legend(app.axes1,app.regular_legend);
    set(legende,'Interpreter', 'none');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
end
%% Build Table
% Delete empty rows
num = 1:i;

t_d = [num.',Vbreakdown.'];
index = t_d(:,2) ~= 0;
t_d = t_d(index,:);

% generate table based on data
tdata = table(t_d(:,1),t_d(:,2),'VariableNames',{'Dies','V_Holding'});

% generate Color array
C = C(index(1:length(C)));
end