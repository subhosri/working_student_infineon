function [Vbreakdown ,Vhold_x,Vhold_1,tdata,C] = V_hold_snapback_SOAMode(app,value_dropdown)
% Aufgerufen in SOA Mode appdesigner
% Calculate Holding voltage for snapback in the SOA MOde

% Initialize Variables
s=size(app.I,2);
Vhold_x = zeros(1,s);
Vhold_1 = zeros(1,s);
Vbreakdown = zeros(1,s);
app.V_hold_snap_markers = gobjects(1,size(app.measure_params,1));

% Check if the line selector is used and if so get the selected curves
if ~isempty(app.lineDialog)
    activeCurves = app.lineDialog.ActiveCurves;
else
    activeCurves = logical(ones(length(app.X),1));
end

%% For loop to calculate the V_hold for every curve
for i=1:s
    if ~isempty(app.V{1,i})&activeCurves(i)
        Vnormed = app.V{1,i}/max(app.V{1,i});
        Inormed = app.I{1,i}/max(app.I{1,i});
        
        if all(app.I{1,i} < 0)
            l = size(app.I{1,i}(app.I{1,i}>-0.3));
            [~, knickpoint_1] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
        else
            [~, knickpoint_1] = max(Vnormed - Inormed);
        end
        
        Vbreakdown(:,i) = app.V{1,i}(knickpoint_1);
%         l = size(app.I{1,i}(app.I{1,i}>-0.3));
%         [~, knickpoint_2] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
        if all(app.V{1,i} < 0)
            l = size(app.I{1,i}(app.I{1,i}>-0.3));
            [~, knickpoint_2] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
        else
            stop_idx = find (abs(app.I{1,i})>=abs(app.I{1,i}(end-2)),1);
            [~, ind2] = min(app.V{1,i}(knickpoint_1: stop_idx));
            knickpoint_2 = knickpoint_1 + ind2 - 1;
        end
        Vhold_1(:,i) = app.V{1,i}(knickpoint_2);
        
        %% Detect start Point
        start_pt = knickpoint_2-1;
        
        %% Fit
        
        vector_temp=linspace(1,start_pt ,start_pt);
        fitmax=app.I{1,i}(length(app.I{1,i}));
        if abs(fitmax) >= abs(app.I{1,i}(end-2))
            stop_index_u = find (abs(app.I{1,i})>=abs(app.I{1,i}(end-2)),1);
            stop_index = stop_index_u-1;
            
            for j=stop_index:length(app.V{1,i})
                vector_temp=[vector_temp j];
            end
        end
        
        [xData, yData] = prepareCurveData( app.V{1,i}, app.I{1,i} );
        
        
        % Set up fittype and options.
        ft = fittype( 'poly1' );
        excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
        opts = fitoptions( 'Method', 'LinearLeastSquares' );
        opts.Exclude = excludedPoints;
        
        % Fit model to data.
        [fitresult, ~] = fit( xData, yData, ft, opts );
        
        %V breakdown calculate
        Vhold_x(:,i) = fzero(fitresult,0);
        y = interp1(xData, yData, Vhold_x(i)); %to find the y value corresponding value of Vhold
        %% Ploting
        %C{i} = app.X(1,i).Color; % Kann nur im IV mode aufgerufen werden
        C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
        %app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V";
        switch value_dropdown
            case 'Vhold_X'
                if strcmp(app.LIST_SOA.Value,'IV Curve')
                    app.V_hold_snap_markers(:,i)= plot (app.axes1,Vhold_x(i),y ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                    if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                        app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V"+" and Vt"+"_"+string(Vbreakdown(i))+"V";
                    else
                        app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V";
                    end
                else
                    app.V_hold_snap_markers(:,i)= plot (app.UIAxes,Vhold_x(i), y ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                    %app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V";
                    if app.V_triggerCheckBox.Value == 1 && app.SnapbackCheckBox.Value == 1
                        app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V"+" and Vt"+"_"+string(Vbreakdown(i))+"V";
                    else
                        app.measure_params{i,'V_Hold'} = "Vh"+"_"+string(Vhold_x(i))+"V";
                    end
                end
                tdata = table((1:s).',Vhold_x.','VariableNames',{'Dies','V_Holding_X'});


                
            case 'Vhold_1'
                if strcmp(app.LIST_SOA.Value,'IV Curve')
                    app.V_hold_snap_markers(:,i)=plot (app.axes1,Vhold_1(i), app.I{1,i}(knickpoint_2) ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                else
                    app.V_hold_snap_markers(:,i)=plot (app.UIAxes,Vhold_1(i), app.I{1,i}(knickpoint_2) ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                end
                tdata = table((1:s).',Vhold_1.','VariableNames',{'Dies','V_Holding_1'});
                
        end
    else
        app.measure_params{i,'V_Hold'} = "VSnap doesn't exist"; 
        


    end
end
%% Modifiy legend
% app.Vh_legende_string = rmmissing(plot_name)';
if ~isempty(app.regular_legend)
    legende = legend(app.axes1,app.regular_legend);
    set(legende,'Interpreter', 'none');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
end

%% Build Table
% Delete empty rows
% num = 1:i;
% t_d = [num.',Vbreakdown.'];
% index = t_d(:,2) ~= 0;
% t_d = t_d(index,:);
% 
% % generate table based on data
% tdata = table(t_d(:,1),t_d(:,2),'VariableNames',{'Dies','V_Holding'});
% 
% % generate Color array
% C = C(index(1:length(C)));
end