function [Vbreakdown,tdata,C] = Vhold_SoaMode(app,value_dropdown)
%Calculate Vhold for Soa mode
%
% Initialize Variables
s=size(app.I,2);
Vbreakdown = zeros(1,s);
Vhold_x = zeros(1,s);
app.V_hold_snap_markers=gobjects(1,size(app.measure_params,1));

% Check if the line selector is used and if so get the selected curves
if ~isempty(app.lineDialog)
    activeCurves = app.lineDialog.ActiveCurves;
else
    activeCurves = logical(ones(length(app.X),1));
end
%% For loop to calculate the V_hold for every curve
count = 1;
for i = 1: s
     
    if ~isempty(app.V{1,i})&activeCurves(i)
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
        data_cell = readcell(Path_txt,'Delimiter',',');
        [xIn,~] = find(strcmp(data_cell,'Index'));
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);
        c = data_matrix(:,3); % data_matrix for current values
        [neg ~] = find(c(:)<0)
        neg_no = size(neg,1)
        ratio_neg = size(neg,1)/size(c,1)
        per_neg = ratio_neg * 100
        if ~isempty(app.V{1,i})&activeCurves(i)
            if app.I{1,i}<0
                V = abs(app.V{1,i});
                I = abs(app.I{1,i});
                Vnormed = V/max(V);
                Inormed = I/max(I);
                [~, ind2] = max(Vnormed - Inormed);
                curve_id = app.CurveNoEditField.Value;
                arr = str2double(split(curve_id, ','));%conversion of curve_id from string to numeric values and stored in array
                mem = ismember(i, arr); %this checks if i is an element of the array
                volt = app.V_t_correct.Value;
                vol_arr = str2double(split(volt, ','));
                if ~isempty(volt_arr) && mem==1
                        %i = app.CurveNoEditField.Value
                        
                        Vbreakdown(:,i) = volt_arr(count);
                        [~, idx] = min(abs(V- Vbreakdown(:,i)));
                        Vbreakdown(:,i) = -V(idx)
                        count = count + 1;
                else
                    Vbreakdown(:,i) = -V(ind2);
                end

                %Vbreakdown(:,i) = -V(ind2);
            else
                Vnormed = app.V{1,i}/max(app.V{1,i});
                Inormed = app.I{1,i}/max(app.I{1,i});
                [~, ind2] = max(Vnormed - Inormed);
                curve_id = app.CurveNoEditField.Value;
                arr = str2double(split(curve_id, ','));%conversion of j from string to numeric values and stored in array
                mem = ismember(i, arr); %this checks if i is an element of the array
                volt = app.V_t_correct.Value;
                vol_arr = str2double(split(volt, ','));
                if ~isempty(vol_arr) && mem==1 || numel(volt) >= 1  && isempty(curve_id)==1
                    %i = app.CurveNoEditField.Value
                    %for j=1:length(vol_arr)
                        Vbreakdown(:,i) = vol_arr(count)
                        [~, idx] = min(abs(app.V{1,i}- Vbreakdown(:,i)));
                        Vbreakdown(:,i) = app.V{1,i}(idx)
                        count = count + 1;
                    %end
                else
                       Vbreakdown(:,i) = app.V{1,i}(ind2);
                end
               
%                 if ~isempty(vol_arr) && mem==1
%                         j=1;
%                         %i = app.CurveNoEditField.Value
%                         %for j=1:length(vol_arr)
%                             Vbreakdown(:,i) = vol_arr(j)
%                             [~, idx] = min(abs(app.V{1,i}- Vbreakdown(:,i)));
%                             Vbreakdown(:,i) = app.V{1,i}(idx)
%                             j=j+1;
%                         %end
%                 else
%                     Vbreakdown(:,i) = app.V{1,i}(ind2);
%                 end

            end
            % txt = ['V_{hold}' num2str(i) ' = ' num2str(round(Vbreakdown(i),2)) 'V'];
            % text (app.axes1,0.05, 0.65-i *0.06,txt,'Units','normalized','Color',C{i},'Fontsize',18,'fontweight','bold');
            %C{i} = app.X(1,i).Color; % Kann nur im IV mode aufgerufen werden
            C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
            % for intersection with x axes
            Vnormed = app.V{1,i}/max(app.V{1,i});
            Inormed = app.I{1,i}/max(app.I{1,i});

            if all(app.I{1,i} < 0)
                l = size(app.I{1,i}(app.I{1,i}>-0.3));
                [~, knickpoint_1] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
            else
                [~, knickpoint_1] = max(Vnormed - Inormed);
            end
            if all(app.V{1,i} < 0)
                l = size(app.I{1,i}(app.I{1,i}>-0.3));
                [~, knickpoint_2] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
            else
                stop_idx = find (abs(app.I{1,i})>=abs(app.I{1,i}(end-2)),1);
                [~, ind2] = min(app.V{1,i}(knickpoint_1: stop_idx));
                knickpoint_2 = knickpoint_1 + ind2 - 1;
            end
            %% Detect start Point
            %start_pt = knickpoint_2-1;

            vector_temp=zeros(1,0);
            ind2=1;
            imin=0.1;
            while (abs(app.I{1,i}(ind2))<=abs(imin))
                vector_temp(1,ind2)=ind2;
                ind2=ind2+1;
            end

            start_pt = ind2-1;
            vector_temp=zeros(1,start_pt);
            %
            for k=1:start_pt
                vector_temp(1,k)=k;
            end

            fitmax=app.I{1,i}(length(app.I{1,i}));
            if abs(fitmax) > abs(app.I{1,i}(end-2))
                stop_index_u= find (abs(app.I{1,i})>=abs(app.I{1,i}(end-2)),1);
                stop_index=stop_index_u-1;

                for j=stop_index:length(app.V{1,i})
                    if stop_index > start_pt
                        vector_temp=[vector_temp j];
                    end
                end
            end
            [xData, yData] = prepareCurveData(app.V{1,i}, app.I{1,i});

            % Set up fittype and options.

            ft = fittype( 'poly1' );
            excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
            opts = fitoptions( 'Method', 'LinearLeastSquares' );
            opts.Exclude = excludedPoints;

            % Fit model to data.
            [fitresult, ~] = fit( xData, yData, ft, opts );
            Vhold_x(:,i) = fzero(fitresult,0);
        
            fit_neu = feval(fitresult,xData)
            %% Fit

            %         vector_temp=linspace(1,start_pt ,start_pt);
            %         fitmax=app.I{1,i}(length(app.I{1,i}));
            %         if abs(fitmax) >= abs(app.I{1,i}(end-2))
            %             stop_index_u = find (abs(app.I{1,i})>=abs(app.I{1,i}(end-2)),1);
            %             stop_index = stop_index_u-1;
            %
            %             for j=stop_index:length(app.V{1,i})
            %                 vector_temp=[vector_temp j];
            %             end
            %         end
            %         % Set up curve fitting
            %         [xData, yData] = prepareCurveData( app.V{1,i}, app.I{1,i} );
            %         %if per_neg > 60
            %             ft = fittype( 'poly1' );
            %             excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
            %             opts = fitoptions( 'Method', 'LinearLeastSquares' );
            %             opts.Exclude = excludedPoints;
            %
            %             % Fit model to data.
            %
            %             [fitresult, ~] = fit( xData, yData, ft, opts );
            %
            %             %V breakdown calculate
            %             Vhold_x(:,i) = fzero(fitresult,0);
            %         %else
            %             r_fit=fit(xData,yData,'poly1');
            %             r_fitCoeff = coeffvalues(r_fit);
            %             a_1= r_fitCoeff(1,1);
            %             a_0= r_fitCoeff(1,2);
            %             p1 = 1.401
            %             p2 = -2.124
            %             fit_neu=p1*xData + p2;
            %             %fit_neu=a_0+a_1*xData

            %end


            % Set up fittype and options.
            %         ft = fittype( 'poly1' );
            %         excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
            %         opts = fitoptions( 'Method', 'LinearLeastSquares' );
            %         opts.Exclude = excludedPoints;
            %
            %         % Fit model to data.
            %
            %         [fitresult, ~] = fit( xData, yData, ft, opts );
            %
            %         %V breakdown calculate
            %         Vhold_x(:,i) = fzero(fitresult,0);



            % Generate Names:
            %app.measure_params{i,'V_Hold'} = "Vh_"+string(Vbreakdown(i))+"V";

            % Plot markers in the Curves (Depending on which Mode the GUI is)
            value_dropdown = app.OptionDropDown.Value;
            switch value_dropdown
                case 'Knickpoint'
                    %app.measure_params{i,'V_Hold'} = "Vt1_"+string(Vbreakdown(i))+"V";
                    if strcmp(app.LIST_SOA.Value,'IV Curve')
                       
                        if ~isempty(vol_arr) && mem==1 || numel(volt) >= 1 && isempty(curve_id)==1
                            [~, idx] = min(abs(app.V{1,i}- Vbreakdown(:,i)));
                            app.V_hold_snap_markers(:,i)=plot (app.axes1,app.V{1,i}(idx),app.I{1,i}(idx),'Color',C{i},...
                            'marker','*','Markersize',16,'LineWidth',1.5);
                        else

                             app.V_hold_snap_markers(:,i)=plot (app.axes1,Vbreakdown(i),app.I{1,i}(knickpoint_1),'Color',C{i},...
                            'marker','*','Markersize',16,'LineWidth',1.5);
                        end
                        
                        if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                               
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ string(Vbreakdown(i)) +"V"+" and"+" Vh"+"_"+string(Vhold_x(i))+"V"
                        else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ string(Vbreakdown(i)) +"V";
                        end
                 
                    else
                        if ~isempty(vol_arr) && mem==1 || numel(volt) >= 1 && isempty(curve_id)==1
                            [~, idx] = min(abs(app.V{1,i}- Vbreakdown(:,i)));
                            app.V_hold_snap_markers(:,i)=plot (app.UIAxes,app.V{1,i}(idx),app.I{1,i}(idx),'Color',C{i},...
                            'marker','*','Markersize',16,'LineWidth',1.5);
                        else
                            app.V_hold_snap_markers(:,i)=plot (app.UIAxes,Vbreakdown(i), app.I{1,i}(knickpoint_1),'Color',C{i},...
                            'marker','*','Markersize',16,'LineWidth',1.5);
                        end
%                         app.V_hold_snap_markers(:,i)=plot (app.UIAxes,Vbreakdown(i), app.I{1,i}(knickpoint_1),'Color',C{i},...
%                             'marker','*','Markersize',16,'LineWidth',1.5);
                        if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ string(Vbreakdown(i)) +"V"+" and"+" Vh"+"_"+string(Vhold_x(i))
                        else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ string(Vbreakdown(i)) +"V";
                        end
                    end
                case 'Intersection w. X axe'
                    %                 ind = find(fit_neu>0);
                    %                 if per_neg > 60
                    %                     idx=max(ind)
                    %                 else
                    %                     idx=min(ind)
                    %                 end

                    if strcmp(app.LIST_SOA.Value,'IV Curve')
                        if per_neg > 60
                            ind = find(fit_neu>0);
                            limit = max(ind) - 5;
                            x_limit = xData(xData<xData(limit,:))
                            y_limit = fit_neu(fit_neu<fit_neu(limit,:))
                            j = app.CurveNoEditField.Value;
                            arr = str2double(split(j, ','));%conversion of j from string to numeric values and stored in array
                            mem = ismember(i, arr); %this checks if i is an element of the array
                            if ~isempty(vol_arr) ~=0 && mem==1
                                x = vol_arr(count-1)
                            else
                                x = interp1(fit_neu,xData,0);
                            end
                            %x = interp1(fit_neu,xData,0)
                            if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V"+" and"+"Vh"+"_"+string(Vhold_x(i))
                            else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            end
                            %app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            app.V_hold_snap_markers(:,i)= plot (app.axes1,x_limit,y_limit ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);

                            app.V_hold_snap_markers(:,i)= plot (app.axes1,x,0,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                            %app.V_hold_snap_markers(:,i)= plot (app.axes1,Vhold_x(i), 0 ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                        else
                            %                          ind = find(fit_neu>0);
                            %                          idx=min(ind)
                            curve_id = app.CurveNoEditField.Value;
                            arr = str2double(split(curve_id, ','));%conversion of j from string to numeric values and stored in array
                            mem = ismember(i, arr); %this checks if i is an element of the array
                            if ~isempty(vol_arr) ~=0 && mem==1
                                x = vol_arr(count-1)
                            else
                                x = interp1(fit_neu,xData,0);
                            end
                            %x = interp1(fit_neu,xData,0);
                            
                            if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V"+" and"+"Vh"+"_"+string(Vhold_x(i))
                            else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            end
                            %app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";

                            %x = interp1(fit_neu,xData,0)
                            app.V_hold_snap_markers(:,i)= plot (app.axes1,xData, fit_neu ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);

                            app.V_hold_snap_markers(:,i)= plot (app.axes1,x,0,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);

                        end

                        %app.V_hold_snap_markers(:,i)= plot (app.axes1,Vhold_x(i), 0 ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                        %                     app.V_hold_snap_markers(:,i)= plot (app.axes1,xData, fit_neu ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);
                        %
                        %                     app.V_hold_snap_markers(:,i)= plot (app.axes1,xData(idx),fit_neu(idx),'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);

                    else
                        if per_neg > 60
                            ind = find(fit_neu>0);
                            limit = max(ind) - 5;
                            x_limit = xData(xData<xData(limit,:))
                            y_limit = fit_neu(fit_neu<fit_neu(limit,:))
                            j = app.CurveNoEditField.Value;
                            arr = str2double(split(j, ','));%conversion of j from string to numeric values and stored in array
                            mem = ismember(i, arr); %this checks if i is an element of the array
                            if ~isempty(vol_arr) ~=0 && mem==1
                                x = vol_arr(count-1)
                            else
                                x = interp1(fit_neu,xData,0);
                            end
                            %x = interp1(fit_neu,xData,0)
                            if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V"+" and"+"Vh"+"_"+string(Vhold_x(i))
                            else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            end
                            %app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            app.V_hold_snap_markers(:,i)= plot (app.UIAxes,x_limit,y_limit ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);

                            app.V_hold_snap_markers(:,i)= plot (app.UIAxes,x,0,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                            %app.V_hold_snap_markers(:,i)= plot (app.axes1,Vhold_x(i), 0 ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                        else
                            %                          ind = find(fit_neu>0);
                            %                          idx=min(ind)
                            curve_id = app.CurveNoEditField.Value;
                            arr = str2double(split(curve_id, ','));%conversion of j from string to numeric values and stored in array
                            mem = ismember(i, arr); %this checks if i is an element of the array
                            if ~isempty(vol_arr) ~=0 && mem==1
                                x = vol_arr(count-1)
                            else
                                x = interp1(fit_neu,xData,0);
                            end
                            %x = interp1(fit_neu,xData,0);
                            
                            if  app.SnapbackCheckBox.Value == 1 && app.V_triggerCheckBox.Value == 1 
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V"+" and"+"Vh"+"_"+string(Vhold_x(i))
                            else
                                app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";
                            end
                            %app.measure_params{i,'V_Hold'} = "Vt1_"+ x +"V";

                            %x = interp1(fit_neu,xData,0)
                            app.V_hold_snap_markers(:,i)= plot (app.UIAxes,xData, fit_neu ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);

                            app.V_hold_snap_markers(:,i)= plot (app.UIAxes,x,0,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);

                        end

                        %app.V_hold_snap_markers(:,i)= plot (app.axes1,Vhold_x(i), 0 ,'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);
                        %                     app.V_hold_snap_markers(:,i)= plot (app.axes1,xData, fit_neu ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',0.5);
                        %
                        %                     app.V_hold_snap_markers(:,i)= plot (app.axes1,xData(idx),fit_neu(idx),'Color',C{i},'marker','*','Markersize',16,'LineWidth',1.5);

                        %app.V_hold_snap_markers(:,i)= plot (app.UIAxes,Vhold_x(i), 0 ,'Color',C{i},'marker','*','Markersize',5,'LineWidth',1.5);
                    end
            end
        end
   else
         app.measure_params{i,'V_Hold'} = "Vh can't be calculated";

    end
    
end
%% Modifiy legend
% app.Vh_legende_string = rmmissing(plot_name)';
% if ~isempty(app.regular_legend)
%     legende = legend(app.axes1,app.regular_legend);
%     set(legende,'Interpreter', 'none');
%     set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
% end

%% Build Table
% Delete empty rows
num = 1:i;
t_d = [num.',Vbreakdown.'];
index = t_d(:,2) ~= 0;
t_d = t_d(index,:);

% generate table based on data
tdata = table(t_d(:,1),t_d(:,2),'VariableNames',{'Dies','V_Holding'});

%generate Color array
C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
%C = C(index(1:length(C)));
end