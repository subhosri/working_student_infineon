function [Vt_2,It_2,C] = Vt2_SOAMode(app)


s=size(app.I,2)
Vt_2 = zeros(1,s);
It_2 = zeros(1,s);
app.V_hold_snap_markers=gobjects(2,size(app.measure_params,1));

% Check if the line selector is used and if so get the selected curves
if ~isempty(app.lineDialog)
    activeCurves = app.lineDialog.ActiveCurves;
else
    activeCurves = logical(ones(length(app.X),1));
end

Value_criterion_mode = get(app.criterion_mode,'Value');
Items_criterion_mode = get(app.criterion_mode,'Items');
is_cr = cellfun(@(x)isequal(x,Value_criterion_mode),Items_criterion_mode);
criterion_index= find(is_cr);
%% For loop to calculate the V_hold for every curve
It_2 = zeros(1,s);
Vt_2 = zeros(1,s);
% plot_name = string(zeros(1,s));
% Parameter used for the Criteria:
extraction_threshold = app.ExtractionThrEditField.Value;
Min_Max_Limit = app.absLimitAEditField.Value;
min_fail = str2double(app.minfailDropDown.Value);
Max_Variation = app.LimitMaxVarEditField.Value;
count = 1;
count_v = 1;
for i = 1:s
    %if ~isempty(app.V{1,i})&activeCurves(i)
    if ~isempty(app.V{1,i})
        C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]}
        %C{i} = app.X(1,i).Color; % Kann nur im IV mode aufgerufen werden
        %for correction manually
        curve_id = app.CurveNo_It_2.Value;
        curve_id_v = app.CurveNo_Vt2.Value;

        arr = str2double(split(curve_id, ','));%conversion of j from string to numeric values and stored in array
        mem = ismember(i, arr); %this checks if i is an element of the array
        current = app.It2_correct.Value;
        current_arr = str2double(split(current, ','));

        arr_v = str2double(split(curve_id_v, ','));%conversion of j from string to numeric values and stored in array
        mem_v = ismember(i, arr_v); %this checks if i is an element of the array
        voltage = app.Vt2_correct.Value;
        voltage_arr = str2double(split(voltage, ','));

        if ~isempty(current_arr) && mem==1 || numel(current) >= 1 && isempty(curve_id) >=1 
           It_2(i) = current_arr(count);
           count = count + 1;
           [~, idx] = min(abs(app.I{1,i}- It_2(i)));
           It_2(i) = app.I{:,i}(idx);
           Vt_2(i) = app.V{:,i}(idx);
        else
            [It_2(i), Vt_2(i), ~,crit_spot,spot_used] = Criteria_SOA(app.data_list{i}, criterion_index, extraction_threshold,...
            Min_Max_Limit, min_fail, Max_Variation,app.spotLimit(i,:));
        end

         if ~isempty(voltage_arr) && mem_v==1 || numel(voltage) >= 1 && isempty(curve_id_v) >=1 
           Vt_2(i) = voltage_arr(count_v);
           count_v = count_v + 1;
           [~, idx] = min(abs(app.V{1,i}- Vt_2(i)));
           Vt_2(i) = app.V{:,i}(idx);
           It_2(i) = app.I{:,i}(idx);
         elseif isempty(current_arr)
            [It_2(i), Vt_2(i), ~,crit_spot,spot_used] = Criteria_SOA(app.data_list{i}, criterion_index, extraction_threshold,...
            Min_Max_Limit, min_fail, Max_Variation,app.spotLimit(i,:));
        end

        % Generate Names:
        app.measure_params{i,'IT2'} = "It2_"+add_prefix(It_2(i))+"A";
        app.measure_params{i,'VT2'} = "Vt2_"+add_prefix(Vt_2(i))+"V";
        %app.measure_params{i,'VatI'} = "VatI_"+add_prefix(VatI(i))+"V";
        %app.measure_params{i,'IatV'} = "IatV_"+add_prefix(IatV(i))+"A";

        
%         app.V_hold_snap_markers = [];
        % Plot markers in the Curves (Depending on which Mode the GUI is)
        if strcmp(app.LIST_SOA.Value,'IV Curve')
            app.V_hold_snap_markers(:,i)=plot (app.axes1,Vt_2(i), It_2(i),'Color',C{i},...
                'marker','*','Markersize',16,'LineWidth',1.5);
        elseif  strcmp(app.LIST_SOA.Value,'SPOT')
            if spot_used(1)
                app.V_hold_snap_markers(1,i)=plot (app.axes1,app.Spot1{i}(crit_spot),It_2(i),'Color',C{i},...
                    'marker','*','Markersize',16,'LineWidth',1.5);
            end
            if spot_used(2)
                app.V_hold_snap_markers(2,i)=plot (app.axes1,app.Spot2{i}(crit_spot),It_2(i),'Color',C{i},...
                    'marker','*','Markersize',16,'LineWidth',1.5);
            end
        elseif strcmp(app.LIST_SOA.Value,'Addendum Plot')
            app.V_hold_snap_markers(:,i)=plot (app.APaxes1,Vt_2(i), It_2(i),'Color','r',...
                'marker','*','Markersize',16,'LineWidth',1.5);
        else
            app.V_hold_snap_markers(:,i)=plot (app.UIAxes,Vt_2(i), It_2(i),'Color',C{i},...
                'marker','*','Markersize',16,'LineWidth',1.5);
        end
    end
end
%% Modifiy legend
% app.Vh_legende_string = rmmissing(plot_name)';
if ~isempty(app.regular_legend)
    if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value,'SPOT')
        legende = legend(app.axes1,app.regular_legend);
    elseif strcmp(app.LIST_SOA.Value,'Addendum  Plot')
        %legende = legend(app.APaxes1,app.regular_legend);
        title1 = sgtitle(app.IV_SPOT_Plot,app.regular_legend(1))
    else
        legende = legend(app.UIAxes,app.regular_legend);
    end
    set(legende,'Interpreter', 'none');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
end

%% Build Table
% Delete empty rows
num = 1:i;
t_d = [num.',Vt_2.'];
index = t_d(:,2) ~= 0;

% generate Color array
%C = C(index(1:length(C)));





end