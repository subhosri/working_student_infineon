function [chip_2,Wafer_Final_vt2,Wafer_Final_Vbreakdown,Wafer_Final_it2,Wafer_Final_rdyn] = Wafermap_emtyfile(subdie_x, subdie_y, offset_x, offset_y, ALLSAMP, sample_values,die_x, die_y,Wafer_Final_vt2,Wafer_Final_it2,Wafer_Final_Vbreakdown,Wafer_Final_rdyn)
% ------------------------------------------------------------------------
% Used to generate a empty wafermap
% ------------------------------------------------------------------------
ALLSAMP =setdiff(ALLSAMP,sample_values.Samples);
num_of_samples = size (ALLSAMP,2);

x_y = zeros(num_of_samples, 2);
for k = 1:num_of_samples
    xfind = cell2mat(strfind(ALLSAMP(k), 'X'));
    yfind = cell2mat(strfind(ALLSAMP(k), 'Y'));
    if isempty(xfind(end)) || isempty(yfind(end))
        warn_dlg = warndlg (['Die-Koordinate ist nicht korrekt: ' ALLSAMP(k)], 'Data Conflict');
        waitfor(warn_dlg);
        fclose all;
        error('Falsche Koordinaten');
    end
    string_parts = strtrim(strsplit(char(ALLSAMP(k)), {'X', 'Y'}));
    string_parts = strrep(string_parts, '_', '');
    %Check the String for rests of the name
    if ~isempty(strfind(string_parts{2},'\'))
        x_y(k, 1:2) = [str2double(cell2mat(string_parts(:, 3))), str2double(cell2mat(string_parts(:, 4)))];
    else
        x_y(k, 1:2) = [str2double(cell2mat(string_parts(:, 2))), str2double(cell2mat(string_parts(:, 3)))];
    end
end
cnt_dies = zeros(num_of_samples, 1);
for k = 1:num_of_samples
    % zähle wie viele Subdies in einem Die vorkommen
    % cnt_dies: "Wie oft kommt jedes Die vor"
    t = sum(x_y(1:k, 1) == x_y(k, 1) & x_y(1:k, 2) == x_y(k, 2));
    cnt_dies(k) = t;
end
x_size = die_x * subdie_x;
y_size = die_y * subdie_y;
chip_2 = switch_notch_position2(1, num_of_samples, x_y, subdie_x, subdie_y, cnt_dies, offset_x, offset_y, x_size, y_size);
for i = 1:num_of_samples
    Wafer_Final_vt2(chip_2(i, 2), chip_2(i, 1)) = NaN;
    Wafer_Final_it2(chip_2(i, 2), chip_2(i, 1)) = NaN;
    Wafer_Final_Vbreakdown(chip_2(i, 2), chip_2(i, 1)) = NaN;
    Wafer_Final_rdyn(chip_2(i,2), chip_2(i,1)) = NaN;
    rdyn_NaN(i,1) = NaN;
    
end
end

