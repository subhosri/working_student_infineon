function add_legend_Addendum_Plot (Results_String,app,Index)
if not(isempty(Index))
    Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:)
    app.regular_legend = concatenate_legend(Results_String,Index)
    %title1 = title(app.APaxes1,app.regular_legend(1))
%     Val_IvsVpeak= get(app.IvsV_peakCheckBox, 'Value');
%     val_spot1_iv = get(app.Spot1_IV_with_spot, 'Value');
%     val_spot2_iv = get(app.Spot2_IV_with_spot, 'Value');
    title1 = sgtitle(app.IV_SPOT_Plot,app.regular_legend(1)); %sgtitle plots the title on top of all subplots
    %title1 = title(app.APaxes2,app.regular_legend(1));
    %set(title1,'Visibile', 'off');
    set(title1,'Interpreter', 'none');
    set(title1,'Tag', 'title');
    title1.FontSize = app.FontSizeEditField.Value;

%     if Val_IvsVpeak == 1
%         %title1 = title(app.APaxes1,app.regular_legend(1),'Position', [1.000 0.3000 0]);
%         set(title1,'Interpreter', 'none')
%         title1.FontSize = app.FontSizeEditField.Value;
%     %elseif Val_IvsVpeak == 0 
%     elseif val_spot1_iv == 0 &&  val_spot2_iv == 0
%         title1 = title(app.APaxes2,app.regular_legend(1))
%         %title1 = sgtitle(app.APaxes1,app.regular_legend(1))
%         %title1 = title(app.APaxes2,app.regular_legend(1),'Position', [1.000 0.3000 0])
%         set(title1,'Interpreter', 'none')
%         title1.FontSize = app.FontSizeEditField.Value;
%     end

%     k = strfind(Results_String,'Spot')%finds the term spot from Results_String
%     type_d = isa(k,'double')
%     type_c = isa(k,'cell')
%     if type_d == 1
%         if isempty(k) == 0
%             %legende1= sgtitle(app.APaxes2,app.regular_legend);
%             %legende1= legend(app.APaxes2,app.regular_legend);
%             title1 = title(app.APaxes2,app.regular_legend(1),'Position', [0.3162 1.0343 0.5000])
%             %legende1.FontSize = app.FontSizeEditField.Value;
% 
%             set(legende1,'Interpreter', 'none');
%             set(legende1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%         elseif isempty(k) == 1
%             %legende1= sgtitle(app.APaxes2,app.regular_legend);
%             %legende1= legend(app.APaxes1,app.regular_legend);
%             title1 = title(app.APaxes2,app.regular_legend(1),'Position', [0.3162,1.0343, 0.5000])
%             %legende1.FontSize = app.FontSizeEditField.Value;
%         end
%         
%     elseif type_c == 1 
%         k_mat = cell2mat(k)
%         if isempty(k_mat) == 0
%             %legende1= legend(app.APaxes2,app.regular_legend);
%             title1 = title(app.APaxes2,app.regular_legend(1),'Position', [5.0000 30.1502 0])
% %             legende1.FontSize = app.FontSizeEditField.Value;
% %             set(legende1,'Interpreter', 'none');
% %             set(legende1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%               
%         elseif isempty(k_mat) == 1
%             %legende1= legend(app.APaxes1,app.regular_legend);
%             title1 = title(app.APaxes2,app.regular_legend(1),'Position', [0.3162,1.0343, 0.5000])
%             %legende1.FontSize = app.FontSizeEditField.Value;
% 
%         end
%     elseif type_d == 1 && k == 0
% %         legende1= legend(app.APaxes1,app.regular_legend);
% %         legende1.FontSize = app.FontSizeEditField.Value;
%           title1 = title(app.APaxes2,app.regular_legend(1),'Position', [0.3162,1.0343, 0.5000])
% %         title1.FontSize = app.FontSizeEditField.Value;
% %         set(title1,'Interpreter', 'none');
% %         set(title1.BoxFace, 'ColorType','truecoloralpha',
% %         'ColorData',uint8(255*[.5;.5;.5;0])))
% 
%     end


end