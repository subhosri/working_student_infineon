function add_legend_IV_Curve (Results_String,app,Index)
% Called in the SOA_mode2_App.mlapp to add a legend in the IV_Curve mode
% Inputs:
% -----------------------------------------------------------------------
% Results_String: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Index: list holding which Curves are visible
% -----------------------------------------------------------------------
if not(isempty(Index))
    Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:)
    app.regular_legend = concatenate_legend(Results_String,Index)
    %legende= legend(app.regular_legend);
    legende= legend(app.axes1,app.regular_legend);
    legende.FontSize = app.FontSizeEditField.Value;
    
    set(legende,'Interpreter', 'latex');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
    Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
    Val_R_on2 = get(app.Ron2_checkbox_L,'Value');
    if Val_R_on1 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C{i} = app.X(1,i).Color;
                h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
                delete(h);
            end
        end
    end
    if Val_R_on2 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C{i} = app.X(1,i).Color;
                h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
                delete(h);
            end
        end
    end
else
    legend(app.axes1,'off');
    Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
    Val_R_on2 = get(app.Ron2_checkbox_L,'Value');
    if Val_R_on1 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C{i} = app.X(1,i).Color;
                h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
                delete(h);
            end
        end
    end
    if Val_R_on2 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C{i} = app.X(1,i).Color;
                h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
                delete(h);
            end
        end
    end
 
%         set (app.min_current_R_on1_2,'Value',0);
%         set (app.max_current_R_on1_4,'Value',0);
%         set (app.min_current_R_on2_2,'Value',0);
%         set (app.max_voltage_R_on1_3,'Value',0);
%         set (app.min_voltage_R_on1_3,'Value',0);
%         set (app.max_current_R_on1,'Value',0);
%         set (app.min_current_R_on2,'Value',0);
%         set (app.max_current_R_on1_2,'Value',0);
  
   
end

