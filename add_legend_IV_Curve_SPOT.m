function add_legend_IV_Curve_SPOT (Results_String,app,Index)
% -----------------------------------------------------------------------
% Called in the SOA_mode2_App.mlapp to add a legend in the
% IV_Curve_SPOT mode
% -----------------------------------------------------------------------
% Inputs:
% Results_String: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Index: list holding which Curves are visible
% -----------------------------------------------------------------------
if not(isempty(Index))
    %Val_WaferNb = get(app.CheckBox_waferNb,'Value');
    Val_spot1 = get(app.Spot1CheckBox,'Value');
    Val_spot2 = get(app.Spot2CheckBox,'Value');
    C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
    app.X =  gobjects(2,size(app.measure_params,1));
    if Val_spot1 == 0 && Val_spot2 == 0 
        Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:);
        app.regular_legend = concatenate_legend(Results_String,Index);
        legende= legend(app.UIAxes,app.regular_legend);
        %legende1= legend(app.UIAxes2,app.regular_legend);
        legende.FontSize = app.FontSizeEditField.Value;
        %legende1.FontSize = app.FontSizeEditField.Value;

        set(legende,'Interpreter', 'none');
        set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
        %set(legende1,'Interpreter', 'none');
        %set(legende1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
        Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
        Val_R_on2 = get(app.Ron2_checkbox_L,'Value');

        if Val_R_on1 == 0
            for i = 1:size(app.measure_params,1)
                Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
                data_cell = readcell(Path_txt,'Delimiter',',');
                [xIn,~] = find(strcmp(data_cell,'Index'));
                data_matrix = cell2mat(data_cell(xIn+1:end,:));
                data_matrix = data_matrix(:,2:end);
                if ~isempty(data_matrix)
                    %C{i} = app.X(1,i).Color;
                    h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
                    delete(h);
                end
            end
        end
        if Val_R_on2 == 0
            for i = 1:size(app.measure_params,1)
                Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
                data_cell = readcell(Path_txt,'Delimiter',',');
                [xIn,~] = find(strcmp(data_cell,'Index'));
                data_matrix = cell2mat(data_cell(xIn+1:end,:));
                data_matrix = data_matrix(:,2:end);
                if ~isempty(data_matrix)
                    %C{i} = app.X(1,i).Color;
                    h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
                    delete(h);
                end
            end
        end
    else
        Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:);
        app.regular_legend = concatenate_legend(Results_String,Index);
        %legende= legend(app.UIAxes,app.regular_legend);
        legende1= legend(app.UIAxes2,app.regular_legend);
        %legende.FontSize = app.FontSizeEditField.Value;
        legende1.FontSize = app.FontSizeEditField.Value;

        %set(legende,'Interpreter', 'none');
        %set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
        set(legende1,'Interpreter', 'none');
        set(legende1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
        Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
        Val_R_on2 = get(app.Ron2_checkbox_L,'Value');

        if Val_R_on1 == 0
            for i = 1:size(app.measure_params,1)
                Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
                data_cell = readcell(Path_txt,'Delimiter',',');
                [xIn,~] = find(strcmp(data_cell,'Index'));
                data_matrix = cell2mat(data_cell(xIn+1:end,:));
                data_matrix = data_matrix(:,2:end);
                if ~isempty(data_matrix)
                    %C{i} = app.X(1,i).Color;
                    h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
                    delete(h);
                end
            end
        end
        if Val_R_on2 == 0
            for i = 1:size(app.measure_params,1)
                Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
                data_cell = readcell(Path_txt,'Delimiter',',');
                [xIn,~] = find(strcmp(data_cell,'Index'));
                data_matrix = cell2mat(data_cell(xIn+1:end,:));
                data_matrix = data_matrix(:,2:end);
                if ~isempty(data_matrix)
                    %C{i} = app.X(1,i).Color;
                    h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
                    delete(h);
                end
            end
        end

%         Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:);
%         app.regular_legend = concatenate_legend(Results_String,Index);
%         for i = 1:size(app.measure_params,1)
%             Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
%             data_cell = readcell(Path_txt,'Delimiter',',');
%             [xIn,~] = find(strcmp(data_cell,'Index'));
%             data_matrix = cell2mat(data_cell(xIn+1:end,:));
%             data_matrix = data_matrix(:,2:end);
%             if max(data_matrix(:,9)) == 0 && min(data_matrix(:,9)) == 0
%                 Spot1_Val_Final = strcat('Spot1_',app.measure_params{:,'Spot 1'},'\');
%                 leg = concatenate_legend (Results_String,Index);
%                 Outputleg1 = strcat(Spot1_Val_Final,leg);
%                 Outputleg = [Outputleg1];
%                 Outputleg(not(app.measure_params{:,'Validity_TLP'}),:) = "";
%                 Outputleg(app.measure_params{:,'Nr_Spot'}<2,2) = "";
%                 Outputleg(app.measure_params{:,'Nr_Spot'}<1,1) = "";
%                 Outputleg = reshape(Outputleg',[],1);
% 
%                 % delete the empty parts of the vector
%                 Outputleg(Outputleg == "") = [];
% 
%                 % add legend to the plot
%                 legende_s1= legend(app.UIAxes2,Outputleg);
%                 legende_s1.FontSize = app.FontSizeEditField.Value;
%                 
%                 set(legende_s1,'Interpreter', 'none');
%                 set(legende_s1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%                 
%             else
%                 Spot1_Val_Final = strcat('Spot1_',app.measure_params{:,'Spot 1'},'\');
%                 Spot2_Val_Final = strcat('Spot2_',app.measure_params{:,'Spot 2'},'\');
% 
%                 % Build general base for the legend
%                 leg = concatenate_legend (Results_String,Index);
% 
%                 % Add spot values to the general legend base
%                 Outputleg1 = strcat(Spot1_Val_Final,leg);
%                 Outputleg2 = strcat(Spot2_Val_Final,leg);
% 
%                 % Combine legends to a nx2 matrix
%                 Outputleg = [Outputleg1,Outputleg2];
% 
%                 % delete non existing Spots form where the TLP file is empty
%                 Outputleg(not(app.measure_params{:,'Validity_TLP'}),:) = "";
% 
%                 % delete all Strings of non existing Spots
%                 Outputleg(app.measure_params{:,'Nr_Spot'}<2,2) = "";
%                 Outputleg(app.measure_params{:,'Nr_Spot'}<1,1) = "";
% 
%                 % reshape matrix to a vector
%                 Outputleg = reshape(Outputleg',[],1);
% 
%                 % delete the empty parts of the vector
%                 Outputleg(Outputleg == "") = [];
% 
%                 % add legend to the plot
%                 legende2= legend(app.UIAxes2,Outputleg);
%                 legende2.FontSize = app.FontSizeEditField.Value;
% 
%                 set(legende2,'Interpreter', 'none');
%                 set(legende2.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%                 
%             end
% 
%         end
%         
%         legende= legend(app.UIAxes,app.regular_legend);
%         %legende1= legend(app.UIAxes2,app.regular_legend);
%         legende.FontSize = app.FontSizeEditField.Value;
%         %legende1.FontSize = app.FontSizeEditField.Value;
% 
%         set(legende,'Interpreter', 'none');
%         set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%         %set(legende1,'Interpreter', 'none');
%         %set(legende1.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%         Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
%         Val_R_on2 = get(app.Ron2_checkbox_L,'Value');
% 
%         if Val_R_on1 == 0
%             for i = 1:size(app.measure_params,1)
%                 Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
%                 data_cell = readcell(Path_txt,'Delimiter',',');
%                 [xIn,~] = find(strcmp(data_cell,'Index'));
%                 data_matrix = cell2mat(data_cell(xIn+1:end,:));
%                 data_matrix = data_matrix(:,2:end);
%                 if ~isempty(data_matrix)
%                     C{i} = app.X(1,i).Color;
%                     h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
%                     delete(h);
%                 end
%             end
%         end
%         if Val_R_on2 == 0
%             for i = 1:size(app.measure_params,1)
%                 Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
%                 data_cell = readcell(Path_txt,'Delimiter',',');
%                 [xIn,~] = find(strcmp(data_cell,'Index'));
%                 data_matrix = cell2mat(data_cell(xIn+1:end,:));
%                 data_matrix = data_matrix(:,2:end);
%                 if ~isempty(data_matrix)
%                     C{i} = app.X(1,i).Color;
%                     h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
%                     delete(h);
%                 end
%             end
%         end
% 
     end
 else
    legend(app.UIAxes,'off')
    legend(app.UIAxes2,'off')
    Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
    Val_R_on2 = get(app.Ron2_checkbox_L,'Value');
    if Val_R_on1 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
                %C{i} = app.X(1,i).Color;
                h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0);
                delete(h);
            end
        end
    end
    if Val_R_on2 == 0
        for i = 1:size(app.measure_params,1)
            Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
            data_cell = readcell(Path_txt,'Delimiter',',');
            [xIn,~] = find(strcmp(data_cell,'Index'));
            data_matrix = cell2mat(data_cell(xIn+1:end,:));
            data_matrix = data_matrix(:,2:end);
            if ~isempty(data_matrix)
                C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]};
                %C{i} = app.X(1,i).Color;
                h = findobj(app.UIAxes,'Color', C{i} ,'LineStyle',':','LineWidth', 0.5 ,'Marker','*');
                delete(h);
            end
        end
    end







end
end