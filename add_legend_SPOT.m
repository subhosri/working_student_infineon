function add_legend_SPOT (Results_String,app,Index)
% -----------------------------------------------------------------------
% Called in the SOA_mode2_App.mlapp to add a legend in the SPOT mode
% -----------------------------------------------------------------------
% Inputs:
% Results_String: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Index: list holding which Curves are visible
% -----------------------------------------------------------------------
% if not(isempty(Index))
%     % Load the Spot values form the measure_params table
%     for i = 1:size(app.measure_params,1)
%         Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
%         data_cell = readcell(Path_txt,'Delimiter',',');
%         [xIn,~] = find(strcmp(data_cell,'Index'));
%         data_matrix = cell2mat(data_cell(xIn+1:end,:));
%         data_matrix = data_matrix(:,2:end);
%         if max(data_matrix(:,9)) == 0 && min(data_matrix(:,9)) == 0
%             Spot1_Val_Final = strcat('Spot1_',app.measure_params{:,'Spot 1'},'\');
%             leg = concatenate_legend (Results_String,Index);
%             Outputleg1 = strcat(Spot1_Val_Final,leg);
%             Outputleg = [Outputleg1];
%             Outputleg(not(app.measure_params{:,'Validity_TLP'}),:) = "";
%             Outputleg(app.measure_params{:,'Nr_Spot'}<2,2) = "";
%             Outputleg(app.measure_params{:,'Nr_Spot'}<1,1) = "";
%             Outputleg = reshape(Outputleg',[],1);
% 
%             % delete the empty parts of the vector
%             Outputleg(Outputleg == "") = [];
% 
%             % add legend to the plot
%             legende= legend(app.axes1,Outputleg);
%             legende.FontSize = app.FontSizeEditField.Value;
%             
%             set(legende,'Interpreter', 'none');
%             set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
%             
%         else
%             Spot1_Val_Final = strcat('Spot1_',app.measure_params{:,'Spot 1'},'\');
%             Spot2_Val_Final = strcat('Spot2_',app.measure_params{:,'Spot 2'},'\');
% 
%             % Build general base for the legend
%             leg = concatenate_legend (Results_String,Index);
% 
%             % Add spot values to the general legend base
%             Outputleg1 = strcat(Spot1_Val_Final,leg);
%             Outputleg2 = strcat(Spot2_Val_Final,leg);
% 
%             % Combine legends to a nx2 matrix
%             Outputleg = [Outputleg1,Outputleg2];
%             
%             % delete non existing Spots form where the TLP file is empty
%             Outputleg(not(app.measure_params{:,'Validity_TLP'}),:) = "";
% 
%             % delete all Strings of non existing Spots
%             Outputleg(app.measure_params{:,'Nr_Spot'}<2,2) = "";
%             Outputleg(app.measure_params{:,'Nr_Spot'}<1,1) = "";
% 
%             % reshape matrix to a vector
%             Outputleg = reshape(Outputleg',[],1);
% 
%             % delete the empty parts of the vector
%             Outputleg(Outputleg == "") = [];
% 
%             % add legend to the plot
%             legende= legend(app.axes1,Outputleg);
%             legende.FontSize = app.FontSizeEditField.Value;
%  
%             set(legende,'Interpreter', 'none');
%             set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
%           
%         end
% 
%     end
% %     Spot1_Val_Final = strcat('Spot1_',app.measure_params{:,'Spot 1'},'\');
% %     Spot2_Val_Final = strcat('Spot2_',app.measure_params{:,'Spot 2'},'\');
% %     
% %     % Build general base for the legend
% %     leg = concatenate_legend (Results_String,Index);
% %     
% %     % Add spot values to the general legend base
% %     Outputleg1 = strcat(Spot1_Val_Final,leg);
% %     Outputleg2 = strcat(Spot2_Val_Final,leg);
% %     
% %     % Combine legends to a nx2 matrix
% %     Outputleg = [Outputleg1,Outputleg2];
% %      Outputleg = [Outputleg1];
% %     % delete non existing Spots form where the TLP file is empty
% %     Outputleg(not(app.measure_params{:,'Validity_TLP'}),:) = "";
% %     
% %     % delete all Strings of non existing Spots
% %     Outputleg(app.measure_params{:,'Nr_Spot'}<2,2) = "";
% %     Outputleg(app.measure_params{:,'Nr_Spot'}<1,1) = "";
% %     
% %     % reshape matrix to a vector
% %     Outputleg = reshape(Outputleg',[],1);
% %     
% %     % delete the empty parts of the vector
% %     Outputleg(Outputleg == "") = [];
% %     
% %     % add legend to the plot
% %     legende= legend(app.axes1,Outputleg);
% %     legende.FontSize = app.FontSizeEditField.Value;
% %     
% %     set(legende,'Interpreter', 'none');
% %     set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
% else
%     legend(app.axes1,'off')
% end
% end


if not(isempty(Index))
    Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:)
    app.regular_legend = concatenate_legend(Results_String,Index)
    legende= legend(app.axes1,app.regular_legend);
    legende.FontSize = app.FontSizeEditField.Value;
    
    set(legende,'Interpreter', 'none');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]))
else
    legend(app.axes1,'off');
end