function add_legend_Waveform_Plot (Results_String,app,Index)
if not(isempty(Index))
    Results_String = Results_String(app.measure_params{:,'Validity_TLP'},:)
    app.regular_legend = concatenate_legend(Results_String,Index)
    %title1 = title(app.APaxes1,app.regular_legend(1))
%     Val_IvsVpeak= get(app.IvsV_peakCheckBox, 'Value');
%     val_spot1_iv = get(app.Spot1_IV_with_spot, 'Value');
%     val_spot2_iv = get(app.Spot2_IV_with_spot, 'Value');
    title1 = sgtitle(app.IV_SPOT_Plot,app.regular_legend(1)); %sgtitle plots the title on top of all subplots
    %title1 = title(app.APaxes2,app.regular_legend(1));
    %set(title1,'Visibile', 'off');
    set(title1,'Interpreter', 'none');
    set(title1,'Tag', 'title');
    title1.FontSize = app.FontSizeEditField.Value;
end