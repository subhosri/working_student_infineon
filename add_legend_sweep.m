function [Output_leg] = add_legend_sweep (Results_String,app,Index)
% -----------------------------------------------------------------------
% Called in the SOA_mode2_App.mlapp to add a legend in the sweep mode
% Inputs:
% -----------------------------------------------------------------------
% Results_String: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Index: list holding which Curves are visible
% -----------------------------------------------------------------------
if not(isempty(Index))
    %Combine the parts of the legends by using the concatenate_legend function
    Output_leg = concatenate_legend (Results_String,Index);
    
    %Add prefix pre and post
    Outputleg1 = strcat('pre_sweep_',Output_leg);
    Outputleg2 = strcat('post_sweep_',Output_leg);
    
    %Combine vectors
    Outputleg = [Outputleg1,Outputleg2];
    
    %delete non existing pre and post sweeps
    Outputleg(~app.measure_params{:,'Validity_pre'},1) = "";
    Outputleg(~app.measure_params{:,'Validity_post'},2) = "";
    
    %reshape matrix to a vector based on which curves are shown
    if strcmp(app.ShowSweepDropDown.Value,'PRE')
        Outputleg = Outputleg(:,1);
    elseif strcmp(app.ShowSweepDropDown.Value,'POST')
        Outputleg = Outputleg(:,2);
    else % Both
       Outputleg = reshape(Outputleg',[],1); 
    end
    
    %delete the empty parts of the vector
    Outputleg(Outputleg == "") = [];
    
    %add legend to the plot
    legende= legend(app.axes1, Outputleg);
    legende.FontSize = app.FontSizeEditField.Value;
    
    set(legende,'Interpreter', 'none');
    set(legende.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[.5;.5;.5;0]));
else
    legend(app.axes1,'off')
end
end