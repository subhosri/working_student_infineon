function [res_str] = add_prefix(raw_num)
% ----------------------------------------------------------
% The function determines the prefix for each number
% the range is for all values smaller than one like this:
% 99 - 0.9
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% Input: raw_num
% Output: res_str
% ----------------------------------------------------------

    abs_num = abs(raw_num);
    if abs_num >= 1e3 && abs_num < 1e6
        factor = 1e-3; % kilo
        prefix = "k";
    elseif abs_num >= 1e6 && abs_num < 1e9
        factor = 1e-6; % mega
        prefix = "M";
    elseif abs_num >= 1e9
        factor = 1e-9; % giga
        prefix = "G";
    elseif abs_num < 1e-1 && abs_num > 1e-4
        factor = 1e3; % milli
        prefix = "m";
    elseif abs_num <= 1e-4 && abs_num > 1e-7
        factor = 1e6; % micro
        prefix = "u";
    elseif abs_num <= 1e-7 && abs_num > 1e-10
        factor = 1e9; % nano
        prefix = "n";
    elseif abs_num <= 1e-10
        factor = 1e12; % pico
        prefix = "p";
    else
        factor = 1; % normal
        prefix = '';
    end
    fSpec = "%.2f"; %Rround at 2 digits after the decimal point
    res_str=char(compose(fSpec,raw_num*factor)+prefix); % If the value is NaN it is returned
end