function   add_titel_to_plot(app,Index_titel,Results_String_Titel)
% -----------------------------------------------------------------------
% Called in the SOA_mode2_App.mlapp to add a title to the plot in the 
% IV_Curve, SPOT and sweep mode
% -----------------------------------------------------------------------
% Inputs:
% Results_String_Title: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Titel_Index: list holding which Curves are visible
% -----------------------------------------------------------------------
Titel_Name = concatenate_titel (Results_String_Titel,Index_titel);
Title= title(app.axes1,Titel_Name);
set(Title,'Interpreter', 'none');
end