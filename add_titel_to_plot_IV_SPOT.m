function   add_titel_to_plot_IV_SPOT(app,Index_titel,Results_String_Titel)
% -----------------------------------------------------------------------
% Called in the SOA_mode2_App.mlapp to add a title to the plot in the 
% IV_SPOT mode
% -----------------------------------------------------------------------
% Inputs:
% Results_String_Title: all selected paramters based on the checkboxes in the
% SOA_mode2_App
% app: calling App
% Titel_Index: list holding which Curves are visible
% -----------------------------------------------------------------------
Titel_Name = concatenate_titel (Results_String_Titel,Index_titel);
Title= title(app.UIAxes,Titel_Name);
set(Title,'Interpreter', 'none');

end