function  [X_Subdie,Y_Subdie,Xdie,Ydie,Xoffset,Yoffset]=autodection_wafermap(Samples)
% -----------------------------------------------------------------------
% Called in the TLP_GUI_App_3.mlapp to determine the size of the used
% wafermap so it can be plotted in the report 
% -----------------------------------------------------------------------
% Inputs:
% Samples: Path of the all used Samples
% -----------------------------------------------------------------------
% Output:
% X_Subdie: Number of Subdies in x direction
% Y_Subdie: Number of Subdies in y direction
% Xdie: Size of die in x direction
% Ydie: Size of die in y direction
% Xoffset: Offset(minimum negative cordinate) in x direction
% Yoffset: Offset(minimum negative cordinate) in y direction
% -----------------------------------------------------------------------
same_wafer = false; %umstellen ob es sich bei mehreren pfaden um den gleichen Wafer handelt oder nicht


% Waitbar init
h = waitbar(0,'Auto Detection progress...','Position',[290 310 620 410], 'OuterPosition',[320 300 275 120], 'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
titleHandle = get(findobj(h,'Type','axes'),'Title');
set(titleHandle,'FontSize',15)
setappdata(h,'canceling',0)
waitObject = onCleanup(@() delete(h));
waitbar(1 / 4)

%Initialization
Anzahl_Subdies=1;
Xdie='0';
Ydie='0';
Xoffset='0';
Yoffset='0';
Subdies=[];

%Split Samples in Koordinaten von Dies und anzahl von Sub-dies
for j=1:length(Samples)
    sPath=Samples{j};
    for k=1:length(sPath)
        fid1 = fopen(sPath{k}, 'r');
        output = textscan(fid1,'%s%s','delimiter',',');
        c1 = output{1};
        c2 = output{2};
        IndexDie = strfind(c1, 'Die');
        Die{j,k} = char(c2(not(cellfun('isempty', IndexDie))));
        
        %% If useser ends process by clicking X at the waitbar
        if getappdata(h,'canceling')
            wh = findall(0, 'tag', 'TMWWaitbar');
            delete(wh)
            errordlg ('Programm wurde abgebrochen.');
            return;
        end
        fclose(fid1);
    end
end
%     Anzahl_Subdies=length(unique(Subdies));

waitbar(2 / 4)
if same_wafer
    %convert matrix to vector and remove empty elements
    Die=Die(:);
    Die=Die(~cellfun('isempty',Die));
    
    % Extract X Y coordinates
    for k = 1:length(Die)
        xfind = strfind(Die(k), 'X');
        yfind = strfind(Die(k), 'Y');
        if isempty(xfind{1}) || isempty(yfind{1})
            warn_dlg = warndlg (['Die-Koordinate ist nicht korrekt: ' Die(k)], 'Data Conflict');
            waitfor(warn_dlg);
            fclose all;
            error('Falsche Koordinaten');
        end
        string_parts = strtrim(strsplit(char(Die(k)), {'X', 'Y'}));
        string_parts = strrep(string_parts, '_', '');
        x_y(k, 1:2) = [str2double(cell2mat(string_parts(:, 2))), str2double(cell2mat(string_parts(:, 3)))];
    end
    waitbar(3 / 4)
    % Extract anzahl von subdies by counting the xy repetitions
    cnt_dies = zeros(length(Die), 1);
    for k = 1:length(Die)
        % z�hle wie viele Subdies in einem Die vorkommen
        % cnt_dies: "Wie oft kommt jedes Die vor"
        t = sum(x_y(1:k, 1) == x_y(k, 1) & x_y(1:k, 2) == x_y(k, 2));
        cnt_dies(k) = t;
    end
    
    waitbar(4 / 4)
    Anzahl_Subdies=max(cnt_dies);
    if Anzahl_Subdies>5
        X_Subdie=ceil(Anzahl_Subdies/5);
        Y_Subdie=5;
    else
        X_Subdie=Anzahl_Subdies;
        Y_Subdie=1;
    end
    
    XMax=max(x_y(:,1));
    XMin=min(x_y(:,1));
    YMax=max(x_y(:,2));
    YMin=min(x_y(:,2));
else
    % Extract X Y coordinates
    for l = 1:size(Die,1)
        for k = 1:max(find(~cellfun(@isempty,Die(l,:))))
            if isempty(strfind(Die{l,k}, 'X')) || isempty(strfind(Die{l,k}, 'Y'))
                warn_dlg = warndlg (['Die-Koordinate ist nicht korrekt: ' Die(k)], 'Data Conflict');
                waitfor(warn_dlg);
                fclose all;
                error('Falsche Koordinaten');
            end
            string_parts = strtrim(strsplit(char(Die{l,k}), {'X', 'Y'}));
            string_parts = strrep(string_parts, '_', '');
            x_pos(l,k) = str2double(cell2mat(string_parts(:, 2)));
            y_pos(l,k) = str2double(cell2mat(string_parts(:, 3)));
        end
    end
    waitbar(3 / 4)
    % Extract anzahl von subdies by counting the xy repetitions
    cnt_dies = zeros(length(Die), 1);
    for l = 1:size(Die,1)
        for k = 1:size(Die,2)
            % z�hle wie viele Subdies in einem Die vorkommen
            % cnt_dies: "Wie oft kommt jedes Die vor"
            t = sum(x_pos(l,1:k) == x_pos(l, k) & y_pos(l,1:k) == y_pos(l,k));
            cnt_dies(l,k) = t;
        end
    end
    
    waitbar(4 / 4)
    Anzahl_Subdies=max(max(cnt_dies));
    if Anzahl_Subdies>5
        X_Subdie=ceil(Anzahl_Subdies/5);
        Y_Subdie=5;
    else
        X_Subdie=Anzahl_Subdies;
        Y_Subdie=1;
    end
    
    XMax=max(max(x_pos));
    XMin=min(min(x_pos));
    YMax=max(max(y_pos));
    YMin=min(min(y_pos));
end
%Matrix die should be always in (a x a+2) form and a would be the max
%of dieX and dieY

if XMin<=0
    Xoffset=num2str(-XMin+1); % (-XMin+1)
    matrix_die_x=XMax-XMin+3; % XMax-XMin+2
else
    matrix_die_x=XMax+1; % XMax+3
end

if YMin<=0
    Yoffset=num2str(-YMin+1); %(-YMin+1)
    matrix_die_y=YMax-YMin+3; %YMax-YMin+3
else
    matrix_die_y=YMax+1; %YMax+3
end

if max(matrix_die_x,matrix_die_y) <= 2
    Xdie=num2str(5);
    Ydie=num2str(7);
else
    Xdie=num2str(max(matrix_die_x,matrix_die_y));
    Ydie=num2str(max(matrix_die_x,matrix_die_y)); %num2str(max(matrix_die_x,matrix_die_y)+2)
end

X_Subdie=num2str(X_Subdie);
Y_Subdie=num2str(Y_Subdie);
delete(h)

end