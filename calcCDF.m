function [CDF_rdyn, CDF_Vt2, CDF_It2, CDF_hbm, cdf,CDF_Vbreakdown] = calcCDF(parameter_values)
% -----------------------------------------------------------------------
% Calculation of the CDF functions
% Called by write_plot_statistics
% -----------------------------------------------------------------------
% Inputs:
% parameter_values: One Voltage Measurment
% I: One corresponding Current Measurment
% -----------------------------------------------------------------------
% Outputs:
% Vbreakdown: Breakdown Voltage
% ind2: index of the breakdown Voltage
% -----------------------------------------------------------------------
    l = length(parameter_values.rdyn);
    cdf = zeros(l, 1);
    cdf(1) = 1/l;
    for k = 2:l
        cdf(k) = cdf(k-1) + 1/l;
    end
    
    CDF_rdyn = sort(parameter_values.rdyn);
    CDF_Vt2 = sort(parameter_values.Vt2);
    CDF_It2 = sort(parameter_values.It2);
    CDF_hbm = sort(parameter_values.hbm);
    CDF_Vbreakdown = sort(parameter_values.Vbreakdown);
end