function [Vtrig, Vhold, iMin, iMax] = calcVtrigVhold(V, I, snapback_values,Vbreakdown,It2,Vhold_1,ind_Vhold1)
%berechnung von Trigger und HaltSpannung
%aufgerufen: write_sample_latex

    Vtrig = [];
    Vhold = [];
    iMax = [];
    iMin = [];
    
    if isnan(snapback_values.eval_snapback)
        min_snapback = 0.1;
        
    else
        min_snapback = snapback_values.edit_snapback;
    end

    Vtrig(1)=Vbreakdown;
    iMax(1)=find(V==Vbreakdown);
    Vhold(1)=Vhold_1;
    iMin(1)=ind_Vhold1;
    neg=1;
    index=find(abs(I) >= abs(It2));
    start=iMin(1)+1;
    stop=index(1)-1;
    if start<stop
        V_neu=V(start:stop);
    else
        V_neu=V(2:stop);
    end
    if all(I < 0)
        I = -I;
        V_neu = -V_neu;
        neg=-1;
    end
      

%% New code (testing)
[Maxima,MaxIdx] = findpeaks(V_neu);    
DataInv = 1.01*max(V_neu) - V_neu;
[~,MinIdx] = findpeaks(DataInv);
Minima = V_neu(MinIdx);
j=1;

% Order minima and maxima
if ~isempty(MinIdx) && ~isempty(MaxIdx)
    if MinIdx(1)<MaxIdx(1)
        MinIdx(1)=[];
        Minima(1)=[];
    end
end
for i=1:(length(Maxima)-1)
     if abs(Vhold(j)-Maxima(i))>= 0.1 && abs(Minima(i)-Maxima(i))>= min_snapback && (stop>(MinIdx(i)+start-1))
        Vhold(j+1)=Minima(i);
        iMin(j+1)=MinIdx(i)+start-1;
        Vtrig(j+1)=Maxima(i);
        iMax(j+1)=MaxIdx(i)+start-1;
        j=j+1;
    end
end
 if isempty(Vhold)
     Vhold(1)=Vhold_1;
     iMin(1)=ind_Vhold1;
%  elseif Minima(end)~=Vhold(end) && abs(Vtrig(end)-Minima(end))>=min_snapback
%      Vhold(j+1)=Minima(end);
%      iMin(j+1)=MinIdx(end)+start-1;
 end
 Vhold=Vhold*neg;
 Vtrig=Vtrig*neg;
end
