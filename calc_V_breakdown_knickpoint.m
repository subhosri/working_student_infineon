function [Vbreakdown, ind2] = calc_V_breakdown_knickpoint(V, I)
% -----------------------------------------------------------------------
% Calculation of the breakdown voltage if a knickpoint exists.
% Called by write_sample_latex
% -----------------------------------------------------------------------
% Inputs:
% V: One Voltage Measurment
% I: One corresponding Current Measurment
% -----------------------------------------------------------------------
% Outputs:
% Vbreakdown: Breakdown Voltage
% ind2: index of the breakdown Voltage
% -----------------------------------------------------------------------


%% Jani for 1.1.3 Beta & future version
    if I<0 % Fraglich ob diese Zeile so funktioniert
        V = abs(V);
        I =abs(I);
    Vnormed = V/max(V);
    Inormed = I/max(I);
    [~, ind2] = max(Vnormed - Inormed);
    Vbreakdown = -V(ind2);
    else
    Vnormed = V/max(V);
    Inormed = I/max(I);
   [~, ind2] = max(Vnormed - Inormed);
    Vbreakdown = V(ind2);
    
    end
    
    %% This code was from DALI(Used till version 1.1.2)  
% %     if all(I < 0)
% %         p = size(I(I>-0.3));
% %        [~, ind2] = min(V(1:p,:) - I(1:p,:));
% %     else
%        [~, ind2] = max(Vnormed - Inormed);
% %    end
%     Vbreakdown = V(ind2);
    
    
        
    
end