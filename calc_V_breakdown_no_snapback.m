function [ Vbreakdown , ind2] = calc_V_breakdown_no_snapback( V,I,imin,It2)
% -----------------------------------------------------------------------
% Calculation of the breakdown voltage if a no snapback exists.
% Called by write_sample_latex
% -----------------------------------------------------------------------
% Inputs:
% V: One Voltage Measurment
% I: One corresponding Current Measurment
% -----------------------------------------------------------------------
% Outputs:
% Vbreakdown: Breakdown Voltage
% ind2: index of the breakdown Voltage
% -----------------------------------------------------------------------

%% Detect start Point
    if isnan(imin)
        imin=0.1;
    end
    
    vector_temp=zeros(1,0);
    ind2=1;
    
    while (abs(I(ind2))<=abs(imin))
       vector_temp(1,ind2)=ind2;
       ind2=ind2+1;
    end   
       
    start_pt = ind2-1;
   
    
       %% Fit
    % excluded values
%     vector_temp=zeros(1,start_pt);  
%     
%     for i=1:start_pt
%         vector_temp(1,i)=i;
%     end
    
%      fitmax=I(length(I));
%      if abs(fitmax) > abs(It2)
%         stop_index_u= find (abs(I)>abs(It2),1);
%         stop_index=stop_index_u-1;
%         
%         for i=stop_index:length(V)
%             if stop_index > start_pt
%                 vector_temp=[vector_temp i];
%             end
%         end
%      end
%     [xData, yData] = prepareCurveData( V, I );
% 
%     % Set up fittype and options.
% 
%     ft = fittype( 'poly1' );
%     excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
%     opts = fitoptions( 'Method', 'LinearLeastSquares' );
%     opts.Exclude = excludedPoints;
% 
%     % Fit model to data.
%     [fitresult, ~] = fit( xData, yData, ft, opts );
%     
%     %V breakdown calculate
%    
%     Vbreakdown=fzero(fitresult,0);
      fitmax=I(length(I));
      if abs(fitmax) > abs(I(end-2))
         
        
          stop_index_u= find (abs(I)>abs(I(end-2)),1);
          stop_index=stop_index_u-1;

          for i=stop_index:length(V)
              if stop_index > start_pt
                  vector_temp=[vector_temp i];
              end
          end
      end
      [xData, yData] = prepareCurveData( V, I );

      % Set up fittype and options.

      ft = fittype( 'poly1' );
      excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
      opts = fitoptions( 'Method', 'LinearLeastSquares' );
      opts.Exclude = excludedPoints;

      % Fit model to data.
      [fitresult, ~] = fit( xData, yData, ft, opts );

      %V breakdown calculate

      Vbreakdown=fzero(fitresult,0);
end