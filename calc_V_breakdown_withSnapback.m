function [ Vbreakdown , knickpoint_1,Vhold_x,Vhold_1,knickpoint_2,tdata] = calc_V_breakdown_withSnapback( V,I,It2)
% -----------------------------------------------------------------------
% Calculation of the breakdown voltage without a Snapback.
% Called by write_sample_latex
% -----------------------------------------------------------------------
% Inputs:
% V: One Voltage Measurment
% I: One corresponding Current Measurment
% -----------------------------------------------------------------------
% Outputs:
% Vbreakdown: Breakdown Voltage
% ind2: index of the breakdown Voltage
% -----------------------------------------------------------------------
% 
% 
%     Vnormed = V/max(V);
%     Inormed = I/max(I);
%     
%     if all(I < 0)
%         l = size(I(I>-0.3));
%        [~, knickpoint_1] = min(V(1:l,:) - I(1:l,:));
%     else
%         [~, knickpoint_1] = max(Vnormed - Inormed);
%     end
%     Vbreakdown = V(knickpoint_1);
% 
%      if all(I < 0)
%         l = size(I(I>-0.3));
%        [~, knickpoint_2] = min(V(1:l,:) - I(1:l,:));
%      else
%         stop_idx = find (abs(I)>=abs(It2),1);
% %         if stop_idx < knickpoint_1
% %             [~, ind2] = min(V(knickpoint_1: end));
% %             knickpoint_2 = knickpoint_1 + ind2 - 1;
% %         else
%             [~, ind2] = min(V(knickpoint_1: stop_idx));
%             knickpoint_2 = knickpoint_1 + ind2 - 1;
% %         end
%      end
% 
%     Vhold_1 = V(knickpoint_2);
%    % Ihold_2 = I(knickpoint_2);
% 
% %% Detect start Point    
%     start_pt = knickpoint_2-1;
%    
% %% Fit
%     % fit_max
%     
%      vector_temp=linspace(1,start_pt ,start_pt);  
%      fitmax=I(length(I));
%     if abs(fitmax) >= abs(It2)
%         stop_index_u = find (abs(I)>=abs(It2),1);
%         stop_index = stop_index_u-1;
%         
%         for i=stop_index:length(V)
%         vector_temp=[vector_temp i];
%         end
%     end
%     
%     [xData, yData] = prepareCurveData( V, I );
% 
% 
%     % Set up fittype and options.
%     ft = fittype( 'poly1' );
%     excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
%     opts = fitoptions( 'Method', 'LinearLeastSquares' );
%     opts.Exclude = excludedPoints;
%     
%     % Fit model to data.
%     [fitresult, ~] = fit( xData, yData, ft, opts );
% 
%     %V breakdown calculate
%     Vhold_x = fzero(fitresult,0);
% 
%     
% %     % Plot fit with data.
% %     figure( 'Name', 'V Breakdown' );
% %     plot( fitresult, xData, yData, excludedPoints );
% %     hold on;
% %     plot (Vhold_1,0,'ro');
% %     hold on;
% %     plot (v_valuesCopy,i_valuesCopy);
% %     hold on;
% %     plot (Vhold_2,0,'rx');
% %     hold on;
% %     plot (Vbreakdown,0,'r+');
% %     legend( 'I vs. V', 'Excluded I vs. V', 'Fit curve','V hold 1','V hold 2','V breakdown' );
% %     % Label axes
% %     xlabel V
% %     ylabel I
% %     grid on
s=size(I,2);
Vhold_x = zeros(1,s);
Vhold_1 = zeros(1,s);
Vbreakdown = zeros(1,s);
%app.V_hold_snap_markers = gobjects(1,size(app.measure_params,1));

%% For loop to calculate the V_hold for every curve
 for i=1:s
    
       Vnormed = V/max(V);
       Inormed = I/max(I);
       [neg ~] = find(I(:)<0)
       neg_no = size(neg,1)
       ratio_neg = size(neg,1)/size(I,1)
       per_neg = ratio_neg * 100
       if per_neg > 60
           l = size(I(I>-0.3));
           [~, knickpoint_1] = min(V(1:l,:) - I(1:l,:));
       else
           [~, knickpoint_1] = max(Vnormed - Inormed);
       end
       Vbreakdown = V(knickpoint_1);
%         l = size(app.I{1,i}(app.I{1,i}>-0.3));
%         [~, knickpoint_2] = min(app.V{1,i}(1:l,:) - app.I{1,i}(1:l,:));
        if per_neg > 60
            l = size(I(I>-0.3));
            [~, knickpoint_2] = min(V(1:l,:) - I(1:l,:));
        else
            stop_idx = find (abs(I)>=abs(It2),1);
            if stop_idx < knickpoint_1
                [~, ind2] = min(V(knickpoint_1: end));
                knickpoint_2 = knickpoint_1 + ind2 - 1;
            else
                [~, ind2] = min(V(knickpoint_1: stop_idx));
                knickpoint_2 = knickpoint_1 + ind2 - 1;
            end
        end
% 
      Vhold_1 = V(knickpoint_2);
        
        %% Detect start Point
        start_pt = knickpoint_2-1;
        
        %% Fit
        
        vector_temp=linspace(1,start_pt ,start_pt);
        fitmax=I(length(I));
        if abs(fitmax) >= abs(I(end-2))
            stop_index_u = find (abs(I)>=abs(I(end-2)),1);
            stop_index = stop_index_u-1;
            
            for j=stop_index:length(V)
                vector_temp=[vector_temp j];
            end
        end
        
        [xData, yData] = prepareCurveData( V, I);
        
        
        % Set up fittype and options.
        ft = fittype( 'poly1' );
        excludedPoints = excludedata( xData, yData, 'Indices', vector_temp );
        opts = fitoptions( 'Method', 'LinearLeastSquares' );
        opts.Exclude = excludedPoints;
        
        % Fit model to data.
        [fitresult, ~] = fit( xData, yData, ft, opts );
        
        %V breakdown calculate
        Vhold_x= fzero(fitresult,0);
        
   
  end
end