function [VV, IA, VVpost, IApost, presweep_exists, postsweep_exists] = check_sweep(sPath_k)
% -----------------------------------------------------------------------
% This function checks for every single sample if a presweep or a
% postsweep exists. In case they exist, the data is extracted from
% .csv-file.
% Called by TLP_analyse_vTG_v07_12
% -----------------------------------------------------------------------
% Inputs:
% sPath_k:
% -----------------------------------------------------------------------
% Outputs:
% VV: Voltage of presweep
% IA: Current of presweep
% VVpost: Voltage of postsweep
% IApost: Current of postsweep
% presweep_exists: True if it exists False if not
% postsweep_exists: True if it exists False if not
% -----------------------------------------------------------------------
    
    presweep_path = fullfile(char(sPath_k(1:length(sPath_k) - 12)), 'pre_sweep.csv');
    postsweep_path = fullfile(char(sPath_k(1:length(sPath_k) - 12)), 'post_sweep.csv');

    VVpost = [];
    IApost = [];
    VV = [];
    IA = [];
    presweep_exists = 0;
    postsweep_exists = 0;
    
    if exist(presweep_path, 'file')
        presweep_exists = 1;
        data_matrix2 = csvread(presweep_path, 1, 0);
        VV = data_matrix2(:, 1);
        IA = data_matrix2(:, 2);
        IA=abs(IA);
    end
    
    if exist(postsweep_path, 'file')
        postsweep_exists = 1;
        data_matrix3 = csvread(postsweep_path, 1, 0);
        VVpost = data_matrix3(:, 1);
        IApost = data_matrix3(:, 2);
        IApost=abs(IApost);
    end

end