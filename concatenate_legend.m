function [legend] = concatenate_legend (Results_String,Index)
% -----------------------------------------------------------------------
% Combines the parameter from Results_string into one big String and returns
% it
% -----------------------------------------------------------------------
% Inputs:
% Results_String: Selected paramter for the string 
% Index: Bool Vector with the curently visible curves
% -----------------------------------------------------------------------
% Outputs:
% legend: returns the concatenated Strings for all visible Curves
% -----------------------------------------------------------------------

if  length(Index)==1
        legend  = Results_String(:,1); 
elseif length(Index)==2
       legend = strcat(Results_String(:,1),' \',Results_String(:,2));
elseif length(Index)==3
       legend =  strcat(Results_String(:,1),' \',Results_String(:,2),' \ ',Results_String(:,3));
elseif length(Index)==4
       legend = strcat(Results_String(:,1),' \',Results_String(:,2),' \ ',Results_String(:,3),' \ ',Results_String(:,4));
elseif length(Index)==5
       legend = strcat(Results_String(:,1),' \',Results_String(:,2),' \ ',Results_String(:,3),' \ ',Results_String(:,4),' \ ',Results_String(:,5));
elseif length(Index)==6
       legend = strcat(Results_String(:,1),' \',Results_String(:,2),' \ ',Results_String(:,3),' \ ',Results_String(:,4),' \ ',Results_String(:,5),' \ ',Results_String(:,6));
else
    legend = strcat(Results_String(:,1),' \',Results_String(:,2),' \ ',Results_String(:,3),' \ ',Results_String(:,4),' \ ',Results_String(:,5),' \ ',Results_String(:,6));
    display("Please choose less parameter for the legend")
end           
end