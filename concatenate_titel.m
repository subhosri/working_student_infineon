function [Titel_Name] = concatenate_titel (Results_String_Titel,Index_titel)
% -----------------------------------------------------------------------
% Combines the parameter from Results_string_Titel into one big String and 
% returns it
% Returns
% -----------------------------------------------------------------------
% Inputs:
% Results_String_Title: Selected paramter for the string 
% Index_Title: Bool Vector with the curently visible curves
% -----------------------------------------------------------------------
% Outputs:
% Titel_Name: returns the concatenated Strings for all visible Curves
% -----------------------------------------------------------------------
if  length(Index_titel)==1
        Titel_Name  = Results_String_Titel(:,1); 
elseif length(Index_titel)==2
       Titel_Name = strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2));
elseif length(Index_titel)==3
       Titel_Name =  strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2),' \ ',Results_String_Titel(:,3));
elseif length(Index_titel)==4
       Titel_Name = strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2),' \ ',Results_String_Titel(:,3),' \ ',Results_String_Titel(:,4));
elseif length(Index_titel)==5
       Titel_Name = strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2),' \ ',Results_String_Titel(:,3),' \ ',Results_String_Titel(:,4),' \ ',Results_String_Titel(:,5));
elseif length(Index_titel)==6
       Titel_Name = strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2),' \ ',Results_String_Titel(:,3),' \ ',Results_String_Titel(:,4),' \ ',Results_String_Titel(:,5),' \ ',Results_String_Titel(:,6));
else
    Titel_Name = strcat(Results_String_Titel(:,1),' \',Results_String_Titel(:,2),' \ ',Results_String_Titel(:,3),' \ ',Results_String_Titel(:,4),' \ ',Results_String_Titel(:,5),' \ ',Results_String_Titel(:,6));
    warning("too many parameters for the title")
end           
end