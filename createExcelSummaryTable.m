function createExcelSummaryTable(Test_Journal,dir_structure,Device)
% -----------------------------------------------------------------------
% create summary table of the Testjournal in the 
% Called by GTS and TLP analyze 
% -----------------------------------------------------------------------
% Inputs:
% Test_Journal: Struct holding paramter of a test run
% dir_structure: Name of the directory structure
% Device: Name of the device (String)
% -----------------------------------------------------------------------

% read data to write
Pin1 = Test_Journal(:,1);
Pin2 = Test_Journal(:,2);
Result = Test_Journal(:,3);
num_Pin = num2cell(linspace(1,size(Test_Journal,1),size(Test_Journal,1)));
Table = [num_Pin' Pin1 Pin2 Result];

% Manage files
file = [dir_structure '\testjournal_' Device '.xlsx'];
% fileName_template = 'Testjournal_Template.xlsx';                          %used for testing version local
% search for application path(installation path)                           %used for standalone version
[~, result] = system('set PATH');
executableFolder = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once'));
fileName_template = [executableFolder '\Testjournal_Template.xlsx'];       %used for standalone version
%copyfile (fileName_template,file);

%write in xls file
xlswrite(file,Table,'Test1','A25');
xlswrite(file,Test_Journal(1,6),'Test1','F07');
xlswrite(file,Test_Journal(1,5),'Test1','F06');
xlswrite(file,Test_Journal(1,4),'Test1','F08');
%remove yellow color
% excel = actxserver('Excel.Application');
% workbook = excel.Workbooks.Open(file);
% for i = (25+size(Test_Journal,1)):200
%     workbook.Worksheets.Item('Test1').Range(strcat('B', num2str(i))).Interior.ColorIndex = 0;
%     workbook.Worksheets.Item('Test1').Range(strcat('C', num2str(i))).Interior.ColorIndex = 0;
% end
% workbook.Save;
% excel.Quit;
% for k = 25 : (24+length(Test_Journal))
%     cellReference1 = sprintf('A%d', k);
%     cellReference2 = sprintf('B%d', k);
%     cellReference3 = sprintf('C%d', k);
%     if Result{k-24}>=0
%        cellReference4 = sprintf('D%d', k);
%     else
%         cellReference4 = sprintf('E%d', k);
%     end
%     xlswrite(file,(k-24),'Test1',cellReference1)
%     xlswrite(file,Pin1(k-24),'Test1',cellReference2)
%     xlswrite(file,Pin2(k-24),'Test1',cellReference3)
%     xlswrite(file,Result(k-24),'Test1',cellReference4)
% end
end

% values = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10'};
% headers = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
% rgb = [255 0 0]; %# if you have 0 to 1 values multiply by 255 and round
% clr = rgb * [1 256 256^2]'; %'# convert to long number Excel understands
% 
% e = actxserver ('Excel.Application'); %# open Activex server
% filename = fullfile(pwd,'C:\Users\Tnani\Desktop\V1.0.8\testjournal_2.xlsx'); %# full path required
% if exist(filename,'file')
%     ewb = e.Workbooks.Open(filename); %# open the file
% else
%     error('File does not exist.') %# or create a new file
% end
% esh = ewb.ActiveSheet;
% for c = 1:numel(values)
%     esh.Range(strcat(headers{c},values{c})).Interior.Color = clr;
% end
% ewb.Save
% ewb.Close(false)
% e.Quit
