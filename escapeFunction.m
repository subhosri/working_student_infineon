function [string] = escapeFunction(string)
% This function escapes some special characters in Latex.
% called at multible occassions
% -----------------------------------------------------------------------
% Inputs:
% string: string with special characters
% -----------------------------------------------------------------------
% Outputs:
% string: string without special characters
% -----------------------------------------------------------------------
    if strfind(string, '\')
        string = strrep(string, '\', '\\bshyp ');
    end
    
    if strfind(string, '$')
        string = strrep(string, '$', '\\$');
    end

    if strfind(string, '#')
        string = strrep(string, '#', '\\#');
    end
    
    if strfind(string, '_')
        string = strrep(string, '_', '$\\_$');
    end
    
    if strfind(string, '%')
        string = strrep(string, '%', '\\%%');
    end
    
    if strfind(string, '&')
        string = strrep(string, '&', '\\&');
    end

end

