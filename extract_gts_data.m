function [data_matrix, failure_test_names, outcome_spots, Pre_sweep, Post_sweep, cols_failure_test, failure_tests_spots,criteria_matrix,Value_table] = extract_gts_data(parameters, parameters_num, data_string, data_num, sweep_num, current_num, voltage_num, stress_polarity, criterion_mode, threshold, criteria_table, Min_Max_Limit, Max_Variation )

    % search for "Failure Test"
    e = strfind(parameters, 'Failure Test #');
    [~, cols_failure_test] = find(~cellfun(@isempty, e));
    failure_test_names = cell(1, length(cols_failure_test));
    for k = 1:length(cols_failure_test)
        failure_test_names{k} = parameters_num{ismember(parameters(:, cols_failure_test(k)), 'Name:'), cols_failure_test(k) + 1};
    end
    
    % check which of the failure tests are spots (= columns)
    spot_indices = strfind(lower(failure_test_names), 'spot');
    [~, b] = find(not(cellfun('isempty', spot_indices)));
    num_of_spots = size(b, 2);
    
    % Check if SoftStop finished or not (workaround)
    all_action_type = data_string(:,3);
    HBM_idx = find(cellfun('isempty', all_action_type));
    last_pulse_idx = HBM_idx(end);
    if ~(length(all_action_type)>= last_pulse_idx+num_of_spots) || (length(all_action_type)>= last_pulse_idx+num_of_spots && any(ismember(data_string(last_pulse_idx+1:last_pulse_idx+num_of_spots,6), '(See CT Data)')))
        current_num(:,end) = [];
        voltage_num(:,end) = [];
        if ismember(data_string(end,6), '(See CT Data)')
            data_string(last_pulse_idx:end-1,:)=[];
            data_num(last_pulse_idx:end-1,:)=[];
        else
            data_string(last_pulse_idx:end,:)=[];
            data_num(last_pulse_idx:end,:)=[];
        end
    end
    
    %Pulse = current_num(3, 2:end);
    Pulse = [current_num{3, 2:end}];
    Pulse(find(isnan(Pulse)))=[];
    l = size(Pulse, 2);
    % DUT(A)
    I_hbm = zeros(l, 1);
    for k = 2:l+1
        if strcmp(stress_polarity,'Positive')
            I_hbm(k-1, 1) = max([current_num{5:end, k}]);
        elseif strcmp(stress_polarity,'Negative')
            I_hbm(k-1, 1) = min([current_num{5:end, k}]);
        else
            if current_num{3,k}>0
                I_hbm(k-1, 1) = max([current_num{5:end, k}]);
            else
                I_hbm(k-1, 1) = min([current_num{5:end, k}]);
            end
        end
    end
    I_hbm(isnan(I_hbm)) = 0;  %remove NANs
    % DUT(A)
    V_hbm = zeros(l, 1);
    for k = 2:l+1
        if strcmp(stress_polarity,'Positive')
            V_hbm(k-1, 1) = max([voltage_num{5:end, k}]);
        elseif strcmp(stress_polarity,'Negative')
            V_hbm(k-1, 1) = min([voltage_num{5:end, k}]);
        else
             if voltage_num{3,k}>0
                V_hbm(k-1, 1) = max([voltage_num{5:end, k}]);
            else
                V_hbm(k-1, 1) = min([voltage_num{5:end, k}]);
            end
        end
    end
    V_hbm(isnan(V_hbm)) = 0;  %remove NANs
    %-------------------------------

    
    %----------------------- Data
    % search for failure test names and save results (spots + sweeps)
    % result contains the values of the failure tests
    l_failure = length(failure_test_names);
    result = cell(1, l_failure);
    test_value = cell(1, l_failure);
    outcome = cell(1, l_failure);
    for z = 1:l_failure
        % rows of failure tests in "Data"
        [w, ~] = find(ismember(data_string, failure_test_names(z)));
        [~, c_result] = find(ismember(data_string, 'Result'));
        [~, c_value] = find(ismember(data_string, 'Test Value'));
        [~, c_outcome] = find(ismember(data_string, 'Outcome'));
        result{z} = data_num(w, c_result);
        test_value{z} = data_num(w, c_value);
        outcome{z} = data_num(w, c_outcome);
    end
    
    %spot_indices = strfind(lower(data_string), 'spot');
    %[a, ~] = find(not(cellfun('isempty', spot_indices)));
    % search for specific attributes
    %[~, c_outcome] = find(ismember(data_string, 'Outcome'));
    %outcome = data_num(a, c_outcome);
    
    %extract test_value and spots
    failure_tests_spots = cols_failure_test(b);
    outcome_spots = outcome(b);
    spots = result(b);
    test_value_spots = test_value(b);
    
    
    %  multi stress case:          
    for i=1:num_of_spots
       if length (spots{i})~=l+1
           number_total_stress = sum(ismember(data_string(:,c_result), 'Complete'));
           number_stress_iter = number_total_stress / (length (spots{i})-1);
           spots_temp = spots{i}';
           for j=1:number_stress_iter-1
               spots_temp = [spots_temp; spots{i}'];  % duplicate results for all stresses
           end
           spots_temp = reshape(spots_temp,[],1); %reshape size matrix
           spots{i} = spots_temp(1:(end-number_stress_iter+1));
           test_value_temp = test_value_spots{i}';
           for j=1:number_stress_iter-1
               test_value_temp = [test_value_temp; test_value_spots{i}'];  % duplicate results for all stresses
           end
           test_value_temp = reshape(test_value_temp,[],1); %reshape size matrix
           test_value_spots{i} = test_value_temp(1:(end-number_stress_iter+1));
       end
    end
    
    
    % check if sweep exists
        b = strfind(failure_test_names, 'Sweep');
    if all(cellfun(@isempty, b))
        b = strfind(failure_test_names, 'sweep');
    end
    if any(~cellfun(@isempty, b))
        % sweep exists
        % read sweep data
        Pre_sweep = sweep_num(5:end, 2);
        Post_sweep = sweep_num(5:end, 3);
    end

    % look for the criteria mode
    criteria_matrix = cell(length(failure_test_names),5);
    for i=1:length(failure_test_names)
        % Limit Value
        [row_limit_value, col_limit_value] = find(ismember(parameters(:,cols_failure_test(i)), 'Limit Value:'));
        if ~isempty(row_limit_value)
          limit_value = num2str(parameters_num{row_limit_value, cols_failure_test(i)+col_limit_value});
        else
         limit_value = 'None';
        end
        % Force Value
        [row_force_value, col_force_value] = find(ismember(parameters(:,cols_failure_test(i)), 'Force Value:'));
        [row_force_minvalue, col_force_minvalue] = find(ismember(parameters(:,cols_failure_test(i)), 'Force Start Value:'));
        [row_force_maxvalue, col_force_maxvalue] = find(ismember(parameters(:,cols_failure_test(i)), 'Force Stop Value:'));
        if ~isempty(row_force_value)
          force_value = num2str(parameters_num{row_force_value, cols_failure_test(i)+col_force_value});
        elseif ~isempty(row_force_minvalue) && ~isempty(row_force_maxvalue)
            force_min = parameters_num{row_force_minvalue, cols_failure_test(i)+col_force_minvalue};
            force_max = parameters_num{row_force_maxvalue, cols_failure_test(i)+col_force_maxvalue};
            force_value = [num2str(force_min) '...' num2str(force_max)];
        else
         force_value = 'None';
        end
        % criterion
        param_temp = parameters_num(12:end,cols_failure_test(i)+1) ;
        row_criteria_mode = find(cellfun(@(x)isequal(x,1),param_temp))+11;
        % fill matrix
        criteria_matrix(i,1) = failure_test_names(i);
        criteria_matrix(i,2) = cellstr(force_value);
        criteria_matrix(i,3) = cellstr(limit_value);
        
        if criterion_mode == 1
            if isempty(row_criteria_mode)
            Value_table = 'Value';
            criteria_matrix(i,4) = cellstr('MISSING CRITERION');
            criteria_matrix(i,5) = cellstr('NONE');
            else
                criteria_matrix(i,4) = cellstr(parameters_num{row_criteria_mode, cols_failure_test(i)+col_limit_value-1});
                Value_table = 'Value';
                if strcmp(criteria_matrix(i,4),'% Increase Enabled?')|| strcmp(criteria_matrix(i,4),'% Decrease Enabled?') 
                    to_edit = criteria_matrix(i,4); % delete (%)
                    criteria_matrix(i,4)= cellstr(to_edit{1}(2:end));
                    criteria_matrix(i,5) = cellstr([num2str(parameters_num{row_criteria_mode+1, cols_failure_test(i)+col_limit_value}*100),'\\%%']);
                else
                    criteria_matrix(i,5) = cellstr(num2str(parameters_num{row_criteria_mode+1, cols_failure_test(i)+col_limit_value}));
                end
            end
        elseif criterion_mode >= 2 && criterion_mode <= 7
            criteria_matrix(i,4) = criteria_table(criterion_mode);
            criteria_matrix(i,5) = cellstr(threshold);
            Value_table = 'Extraction Threshold';
        elseif criterion_mode == 8
             criteria_matrix(i,4) = criteria_table(criterion_mode);
           % criteria_matrix(i,5) = WILL BE ASSIGNED IN CRITERIA_GTS
             Value_table = 'abs(Maximum value)';
        elseif criterion_mode > 8 && criterion_mode <= 14
             criteria_matrix(i,4) = criteria_table(criterion_mode);
             criteria_matrix(i,5) = cellstr(num2str(threshold));
             Value_table = 'Min_Max Limit';
        elseif criterion_mode > 14 && criterion_mode ~= 18
            criteria_matrix(i,4) = criteria_table(criterion_mode);
             criteria_matrix(i,5) = cellstr(num2str(threshold));
             Value_table = 'Max Limit Variation';
        elseif criterion_mode == 18
             criteria_matrix(i,4) = criteria_table(criterion_mode);
             criteria_matrix(i,5) = cellstr(threshold);
             Value_table = 'NO_spots';
        end
    end
  
    if   num_of_spots == 2       
         data_matrix = [Pulse', V_hbm, I_hbm, zeros(l, 1), zeros(l, 1), zeros(l, 1), [test_value_spots{1}{2:end}]', [spots{1}{2:end}]', [test_value_spots{2}{2:end}]', [spots{2}{2:end}]'];
    elseif num_of_spots == 1
         data_matrix = [Pulse', V_hbm, I_hbm, zeros(l, 1), zeros(l, 1), zeros(l, 1), [test_value_spots{1}{2:end}]', [spots{1}{2:end}]'];
    end