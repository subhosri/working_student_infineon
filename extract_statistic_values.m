function [parameter_values, statistic_values, device_name, angel, space_str,dev_name_stat] = extract_statistic_values(csvsave_dir, hbm_var, hbm_mode, snapback, fileID, data_matrix,Limit_dies)
    % --------------------------------------------------------------------
    % This function calculates some values which are needed for the
    % statistics part.
    % Called by TLP_analyse_vTG_v07_12
    % --------------------------------------------------------------------
    % Input:
    % csvsave_dir: path to TLP_eval.csv
    % hbm_var: string; 'HBM Correlation[V]' if TLP, 'HBM Value[V]' if hbm
    % hbm_mode: True if hbm, False if TLP
    % snapback: True or False
    % fileID: id of measurement.tex
    % --------------------------------------------------------------------
    % Output:
    % parameter_values: contains: sample names, Vt2, It2, Rdyn, hbm,
    % Vbreakdown(Holding voltage)
    % statistic_values: contians: max, min, mean, std of the parameters and
    % in case it is choosed by user Vtrig and Vhold as well
    % device_name: if num of samples < 21: x-axis is labled by device name
    % and coordinate
    % angel: angel of the labels of the x-axis
    % space_str: some space necessary for latex
    % --------------------------------------------------------------------

    %Read V & I
    V = data_matrix(:, 2);%tlp voltage spot
    I = data_matrix(:, 3);%tlp current spot 
    [Vbreakdown, ~] = calc_V_breakdown_knickpoint(V, I);
    
    % Read xls-file
    %[~, str,~] = xlsread(csvsave_dir);
    str = readcell(csvsave_dir);

    idx1 = find(strcmp(str, 'Dataset'), 1, 'first');
    idx2=idx1+1;
    max_lines = size(str,1);

    strsplit = str(idx2:max_lines,:);
    %check if any data are empty
    check_empt = find(strcmp(str, 'Warning'));
    if ~isempty(check_empt)
        strsplit(check_empt,:) = [];
    end
    werte = strsplit;
    if isempty(werte)
        errordlg ('Keine Daten vorhanden.','Data Conflict');
        error('Keine Daten vorhanden');       
    end
    
    % filter empty TLP datafiles
    %find_empty_files = any(strcmp(strsplit, ' Warning: TLP_data file is empty.')');
    %werte = werte(find_empty_files == 0,:);
    
    if isempty(werte)
        fprintf(fileID, '\\end{document}');
        fclose('all');
        errordlg ('Alle TLP_data.csv Dateien enthalten keine Daten!','Data Conflict');
        error('Alle TLP_data.csv Dateien enthalten keine Daten!')
    end
    % change "missing" to NaN for later dataconversions
    mask = logical(zeros(size(werte)));
    mask(:,[4:10,13:15]) = cellfun(@ismissing, werte(:,[4:10,13:15]));
    werte(mask) = {NaN};
    
    werteVt2=cell2mat(werte(:,4));
    werteIt2=cell2mat(werte(:,5));
    werteRdyn=cell2mat(werte(:,6));
    werteHBM=cell2mat(werte(:,7));
    werteVbreakdown=cell2mat(werte(:,13));
    werteIatV = cell2mat(werte(:,9));
    werteVatI = cell2mat(werte(:,8));
    werteClampVoltage = cell2mat(werte(:,10));
    parameter_values = struct('Samples', {werte(:, 1)}, 'Vt2', werteVt2, 'It2', werteIt2, ...
        'rdyn', werteRdyn, 'hbm', werteHBM, 'Vbreakdown', werteVbreakdown,'VatI',werteVatI, 'IatV',werteIatV, ...
        'V_Clamp', werteClampVoltage);
    
    % Beschriftung der x-Achse mit device-name
    limit = size(werte, 1);
    limit_stat = size(werte, 1);
    


    if limit_stat < 100
        dev_name_stat = cell(limit_stat, 1);
        %w1 = cell(limit, 1);
        w2_stat = cell(limit_stat, 1);
            for k = 1:limit_stat
            %w1{k, 1} = escapeFunction(werte{k, 1});
            w2_stat{k, 1} = escapeFunction(werte{k, 3});
            end
        %dev_name = [w1, w2];
        dev_name_stat = [w2_stat];
                for k = 1:limit_stat
            dev_name_stat(k) = {[strjoin(dev_name_stat(k, :)) ',']};
            angel = 55;
                end
                
    end
    if limit < Limit_dies %21
        device_name = cell(limit, 1);
        w1 = cell(limit, 1);
        w2 = cell(limit, 1);
        for k = 1:limit
            w1{k, 1} = escapeFunction(werte{k, 1});
            w2{k, 1} = escapeFunction(werte{k, 3});
        end
        %dev_name = [w1, w2];
        dev_name = [w2];
        for k = 1:limit
            device_name(k) = {[strjoin(dev_name(k, :)) ',']};
            angel = 55;
        end
        space_str = '\\hspace{1.5cm}\n';
    else
        device_name = {};
        angel = 0;
        space_str = '';
    end
    
    % Statistical values for tabular
    if werteVt2<0
        maxV=min(werteVt2);
        minV=max(werteVt2);
        maxI=min(werteIt2);
        minI=max(werteIt2);
        maxHBM=min(werteHBM);
        minHBM=max(werteHBM);
        maxVbreakdown=min(werteVbreakdown);
        minVbreakdown=max(werteVbreakdown);
    else
        maxV=max(werteVt2);
        minV=min(werteVt2);
        maxI=max(werteIt2);
        minI=min(werteIt2);
        maxHBM=max(werteHBM);
        minHBM=min(werteHBM);
        maxVbreakdown=max(werteVbreakdown);
        minVbreakdown=min(werteVbreakdown);
    end
    meanV=mean(werteVt2);
    stdV=std(werteVt2);
    meanVbreakdown=mean(werteVbreakdown);

    % Save statistic values
    if snapback
        werteVhold = str2double(werte(:, 15));
        werteVholdx = str2double(werte(:, 14));
        werteVtrig = str2double(werte(:, 13));
        parameter_values.Vtrig = werteVtrig;
        parameter_values.Vhold = werteVhold;
        parameter_values.Vholdx = werteVholdx;
        
        statistic_values = struct('maxV', maxV, 'minV', minV, 'meanV', meanV,...
        'stdV', stdV, 'maxI', maxI, 'minI', minI, 'meanI', mean(werteIt2),...
        'stdI', std(werteIt2), 'maxRdyn', max(werteRdyn),...
        'minRdyn', min(werteRdyn), 'meanRdyn', mean(werteRdyn),...
        'stdRdyn', std(werteRdyn), 'maxHBM', maxHBM,...
        'minHBM', minHBM,...
        'meanHBM', mean(werteHBM),...
        'stdHBM', std(werteHBM), 'maxVhold_1', max(werteVhold),...
        'minVhold_1', min(werteVhold), 'meanVhold_1', mean(werteVhold),...
        'stdVhold_1', std(werteVhold), 'maxVtrig', max(werteVtrig),...
        'minVtrig', min(werteVtrig), 'meanVtrig', mean(werteVtrig),...
        'stdVtrig', std(werteVtrig));
    else
        werteVb = str2double(werte(:, 13));
        statistic_values = struct('maxV', maxV, 'minV', minV, 'meanV', meanV,...
            'stdV', stdV, 'maxI', maxI, 'minI', minI, 'meanI', mean(werteIt2),...
            'stdI', std(werteIt2), 'maxRdyn', max(werteRdyn),...
            'minRdyn', min(werteRdyn), 'meanRdyn', mean(werteRdyn),...
            'stdRdyn', std(werteRdyn), 'maxHBM', maxHBM,...
            'minHBM', minHBM,...
            'meanHBM', mean(werteHBM),...
            'stdHBM', std(werteHBM),...
            'maxVb', max(werteVb), 'minVb', min(werteVb),...
            'meanVb', mean(werteVb), 'stdVb', std(werteVb),...
            'maxVbreakdown', maxVbreakdown, 'minVbreakdown', minVbreakdown, 'meanVbreakdown', meanVbreakdown);
    end
    
