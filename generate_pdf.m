function generate_pdf(fileID, plotID, dir_structure, cnt_sample_plots, names, save_pngs,folder_name,plotID_stat)

%augerufen im TLP_analyse_vTG_v07_12
    
    fprintf(fileID, '\\end{document}\n');
    fclose(fileID);
    fprintf(plotID, '\\end{document}\n');
    fclose(plotID);
    fprintf(plotID_stat, '\\end{document}\n');
    fclose(plotID_stat);

    %[a, b] = system('pdflatex.exe Report\myReport013.tex');
    current_path = pwd;
    cd(dir_structure)
    cmd_plot = ['pdflatex.exe ' folder_name '_Plots.tex'];
    cmd_plot_stat = ['pdflatex.exe ' folder_name '_Plots_statistics.tex'];
    cmd_report = ['pdflatex.exe ' folder_name '_Measurement_Report.tex'];
    
     %added by chefai to generate report
    %!pdflatex Path_1_Plots.tex
    [c, d] = system(cmd_plot);
    %!pdflatex Path_1_Measurement_Report.tex
    [a, b] = system(cmd_report);
    %!pdflatex Path_1_Plots_statistics.tex
    [a, b] = system(cmd_plot_stat);
    
    
    if save_pngs
        mkdir('PNGPlots');
        cd('PNGPlots');
        for k = 1:cnt_sample_plots 
            %system(['"C:\Program Files\gs\gs9.18\bin\gswin32c.exe" -sDEVICE=pngalpha -sOutputFile=sandy' num2str(k) '.png -dFirstPage=' num2str(k) ' -dLastPage=' num2str(k) ' -dNOPAUSE -dBATCH -q -r144 ..\Plots.pdf'])
            %system(['"gswin32c.exe" -sDEVICE=pngalpha -sOutputFile=' names{k} ' -dFirstPage=' num2str(k) ' -dLastPage=' num2str(k) ' -dNOPAUSE -dBATCH -q -r144 ..\' folder_name '_Plots_statistics.pdf']);
            system(['"gswin32c.exe" -sDEVICE=png16m -sOutputFile=' names{k} ' -dFirstPage=' num2str(k) ' -dLastPage=' num2str(k) ' -dNOPAUSE -dBATCH -q -r300 ..\' folder_name '_Plots.pdf']);
        end
        stats_name = names(cnt_sample_plots+1:end);
        s=size(stats_name,2);
        for i = 1:s
            %system(['"C:\Program Files\gs\gs9.18\bin\gswin32c.exe" -sDEVICE=pngalpha -sOutputFile=sandy' num2str(k) '.png -dFirstPage=' num2str(k) ' -dLastPage=' num2str(k) ' -dNOPAUSE -dBATCH -q -r144 ..\Plots.pdf'])
            system(['"gswin32c.exe" -sDEVICE=png16m -sOutputFile=' stats_name{i} ' -dFirstPage=' num2str(i) ' -dLastPage=' num2str(i) ' -dNOPAUSE -dBATCH -q -r300 ..\' folder_name '_Plots_statistics.pdf']);
            %system(['"gswin32c.exe" -sDEVICE=pngalpha -sOutputFile=' names{i} ' -dFirstPage=' num2str(i) ' -dLastPage=' num2str(i) ' -dNOPAUSE -dBATCH -q -r144 ..\' folder_name '_Plots.pdf']);
        end
    end
    
    cd(current_path)
end

%     system(['C:\MatlabCode\Sandy\Tests\test998\Path_1',...
%         '>"C:\Program Files\gs\gs9.18\bin\gswin32c.exe" -sDEVICE=pngalpha',...
%         '-sOutputFile=sandy1.png -dFirstPage=1 -dLastPage=1 -dNOPAUSE -dBATCH -q -r144 Plots.pdf'])