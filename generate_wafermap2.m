function generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, ~, Wafer_Final, parameter, parameter_name, unit, samples, nonfails, contactfail, figg, plotsave_dir, fileID, colorbar_automatic_range, colorbar_min, colorbar_max, genParam, target_value, critical_area, notch_pos,ALLSAMP)
%load('unittest_files\input_generate_wafermap2.mat')
%target_value = NaN(1);
%critical_area = NaN(1);
%colorbar_min = 2.24;
%colorbar_max = 2.32;
%colorbar_automatic_range = 0;
%aufgerufen im wafermap_sandy2


%added chefai
[row1, col2] = find(isnan(Wafer_Final));
%end chefai
par_std = round(10000*std(parameter))/10000;
par_mean = round(1000*mean(parameter))/1000;
par_max = round(1000*max(parameter))/1000;
par_min = round(1000*min(parameter))/1000;
%
hold on
set(figg, 'Visible', 'on', 'Units','normalized', 'Units','normalized', 'Position',[0.03 0.15 0.45 0.60]);
hold on

% wafermap
handle = title(['\bf Wafermap - ' parameter_name], 'FontName', 'Calibri', 'Color', 'blue', 'FontSize', 12, 'Interpreter', 'tex');
set(handle, 'Position', [subdie_x*die_x/2, subdie_y*die_y + 2.5*subdie_y-0.75]);
xlabel('Chip X','FontWeight','bold','FontName','Calibri','FontSize',12);
ylabel('Chip Y','FontWeight','bold','FontName','Calibri','FontSize',12);
ylim([-1*subdie_y (die_y+2)*subdie_y]);
xlim([-1*subdie_x (die_x+1)*subdie_x]);

xtick = 0 + 0.5*subdie_x:subdie_x:die_x*subdie_x + 0.5;
ytick = 0 + 0.5*subdie_y:subdie_y:die_y*subdie_y + 0.5;

xticklabel = 0:die_x + 1;
yticklabel = 0:die_y + 1;
set(gca,'XTick', xtick)
set(gca,'YTick', ytick)
set(gca,'XTickLabel', xticklabel - offset_x)
set(gca,'YTickLabel', yticklabel - offset_y)

if colorbar_automatic_range
    Wafer_Final(Wafer_Final == -1) = NaN;
    if par_min == par_max
        c_range_min = par_min - 1;
        c_range_max = par_max + 1;
    else
        c_range_min = par_min;
        c_range_max = par_max;
    end
    
elseif ~isnan(colorbar_min) && ~isnan(colorbar_max)
    
    caxis([colorbar_min, colorbar_max]);
    
    Wafer_Final(Wafer_Final == -1) = NaN;
    c_range_min = colorbar_min;
    c_range_max = colorbar_max;
else
    Wafer_Final(Wafer_Final == -1) = NaN;
    c_range_min = 0;
    c_range_max = par_max;
end
% target value
if ~isnan(target_value)
    % valid target value + critical area
    if (target_value > c_range_min && target_value < c_range_max) && ~isnan(critical_area)
        %255, 105,  180         %// pink
        T = [255,   0,   0      %// red
            255, 105, 180       %// pink
            255, 160, 122       %// orange
            255, 255, 0         %// yellow
            0, 255, 0           %// green
            20, 64, 20]./255;   %// dark green
        
        th = (target_value - c_range_min)/(c_range_max - c_range_min) * 255;
        
        % critical area
        dev = critical_area/100 * target_value + target_value;
        if dev > c_range_max
            errordlg (['Die angegebene Abweichung vom Zielwert f�r ' parameter_name ' liegt nicht im Wertebereich.'], 'Data Conflict');
            error('Critical Areae liegt au�erhalb des Wertebereichs.');
        end
        dev_color = (dev - c_range_min)/(c_range_max - c_range_min) * 255;
        
        x =  [0, th, th + .1, dev_color, dev_color + .1, 255];
        map = interp1(x/255,T,linspace(0,1,255));
        if all(parameter < 0) || strcmp(parameter_name, 'R_{dyn}')
            colormap(flipud(map));
        else
            colormap(map);
        end
        
        d = colorbar('EastOutside');
        set(d, 'ylim', [c_range_min c_range_max]);
        dis = c_range_max - c_range_min;
        n1 = floor((target_value - c_range_min)/dis * 10);
        if n1 < 2
            n1 = 2;
        end
        n2 = floor((c_range_max - dev)/dis * 10);
        if n2 < 2
            n2 = 2;
        end
        set(d,'YTick', [linspace(2.243, target_value, n1), linspace(dev, c_range_max, n2)])
        % valid target value without specified critical area
    elseif (target_value > c_range_min && target_value < c_range_max) && isnan(critical_area)
        T = [255,   0,   0      %// red
            255, 105, 180       %// pink
            0, 255, 0           %// green
            20, 64, 20]./255;   %// dark green
        
        th = (target_value - c_range_min)/(c_range_max - c_range_min) * 255;
        x =  [0, th, th + .01, 255];
        map = interp1(x/255,T,linspace(0,1,255));
        
        if all(parameter < 0) || strcmp(parameter_name, 'R_{dyn}')
            colormap(flipud(map));
        else
            colormap(map);
        end
        d = colorbar('EastOutside');
        set(d, 'ylim', [c_range_min c_range_max]);
        dis = c_range_max - c_range_min;
        n1 = floor((target_value - c_range_min)/dis * 10);
        if n1 < 2
            n1 = 2;
        end
        n2 = floor((c_range_max - target_value)/dis * 10);
        if n2 < 2
            n2 = 2;
        end
        a = linspace(target_value, c_range_max, n2);
        set(d,'YTick', [linspace(c_range_min, target_value, n1), a(2:end)])
        % target value not valid
    else
        errordlg (['Der angegebene Zielwert der Wafermap f�r ' parameter_name ' liegt nicht im Wertebereich.'], 'Data Conflict');
        error('Target Value liegt au�erhalb des Wertebereichs.');
    end
    
    % Wafermap without target value
else
    cw = load('cm_wafer.mat');
    cw = cw.map(3:end, :);
    if all(parameter < 0) || strcmp(parameter_name, 'R_{dyn}')
        colormap(flipud(cw));
    else
        colormap(cw);
    end
    %pause(0.02)
    d = colorbar('EastOutside');
    set(d, 'ylim', [c_range_min c_range_max]);
    pause(0.02)
end
%% plot Wafermap
g = pcolor((1:size(Wafer_Final, 2)) - 1, (1:size(Wafer_Final, 1)) - 1, Wafer_Final);
%%%chefai: Werte zur Wafermap hinzuf�gen
[row, col] = find(~isnan(Wafer_Final));

if samples < 20
    for j=1: (size(ALLSAMP,2)-samples)
        text(col2(j)-0.9,row1(j)-0.4,'x','Color','red','FontSize',15);
    end
    for i =1:samples
        text(col(i)-0.9,row(i)-0.4,add_prefix(Wafer_Final(row(i),col(i))),'FontSize',7);
    end
elseif samples >=20 && samples < 40
    for j=1: (size(ALLSAMP,2)-samples)
        text(col2(j)-0.8,row1(j)-0.4,'x','Color','red','FontSize',15);
    end
    for i =1:samples
        text(col(i)-0.8,row(i)-0.4,add_prefix(Wafer_Final(row(i),col(i))),'FontSize',6);
    end
else
    for j=1: (size(ALLSAMP,2)-samples)
        text(col2(j)-0.65,row1(j)-0.4,'x','Color','red','FontSize',13);
    end
    for i =1:samples
        text(col(i)-0.85,row(i)-0.4,add_prefix(Wafer_Final(row(i),col(i))),'FontSize',6);
    end
end
%%% end chefai
set(g, 'EdgeColor', 'none');
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'Color', [1 1 1]); % Sets figure background
set(gca,'Color',[1 1 1],'FontWeight','bold','FontName','Calibri','FontSize',13);
set(gcf, 'Renderer', 'painters');

y = ylabel(d, [parameter_name '[' unit ']'],'FontWeight','bold','FontName','Calibri','FontSize',12);
set(y, 'Units', 'Normalized', 'Position', [0.1, 0.48, 0]);

%%%%%%%%%%%---write Text with data into Wafermap---%%%%%%%%%%%%%%%%%%%%%
text(0, y_size+subdie_y , [sprintf('%s\n','# Measurement parameter '),...
    sprintf('%s\n',['# Averaging V: ' char(genParam.Avg_Inter_VStart) ' - ' char(genParam.Avg_Inter_VStop) 'ns']),...
    sprintf('%s\n',['# Averaging I: ' char(genParam.Avg_Inter_IStart) ' - ' char(genParam.Avg_Inter_IStop)]),...
    sprintf('%s\n',['# tr = ' char(genParam.risetime)]),...
    sprintf('%s\n',['# tp = ' char(genParam.pulseshape)])],...
    'FontSize',7,'FontName','Calibri','FontWeight','normal','Color',[0 0 0], 'Interpreter', 'tex')

text((die_x/2 - .5)*subdie_x, y_size+subdie_y , [sprintf('%s\n','# Measurement '),...
    sprintf('%s\n', ['# Samples = ' num2str(samples)]),...
    sprintf('%s\n', ['# Non-fails = ' num2str(nonfails)]),...
    sprintf('%s\n', ['# Contact-fails = ' num2str(contactfail)])],...
    'FontSize', 7, 'FontName', 'Calibri', 'FontWeight', 'normal', 'Color',[0 0 0], 'Interpreter', 'tex')

text((die_x-1.5)*subdie_x, y_size+subdie_y , [sprintf('%s\n','# Statistics'),...
    sprintf('%s\n',['# Min Value : ' num2str(par_min) unit]),...
    sprintf('%s\n',['# Max. Value: ' num2str(par_max) unit]),...
    sprintf('%s\n',['# Mean Value: ' num2str(par_mean) unit]),...
    sprintf('%s\n',['# Std. Deviation: ' num2str(par_std) unit])],...
    'FontSize',7,'FontName','Calibri','FontWeight','normal','Color',[0 0 0], 'Interpreter', 'tex')

die_x = die_x - 1;
die_y = die_y - 1;

% gridlines x
for k = 0:subdie_x
    plot([k k],[2*subdie_y (die_y-2)*subdie_y], 'Color', [.71,.80,.80])
end

for k = subdie_x:2*subdie_x
    plot([k k],[subdie_y (die_y-1)*subdie_y], 'Color', [.71,.80,.80])
end
for o=2*subdie_x:die_x*subdie_x - 2*subdie_x
    plot([o o],[0 die_y*subdie_y], 'Color', [.71,.80,.80])
end
for k = die_x*subdie_x - 2*subdie_x:die_x*subdie_x - subdie_x
    plot([k k],[subdie_y (die_y-1)*subdie_y], 'Color', [.71,.80,.80])
end

for k = die_x*subdie_x - subdie_x:die_x*subdie_x
    plot([k k],[2*subdie_y (die_y-2)*subdie_y], 'Color', [.71,.80,.80])
end
%     for k = die_x*subdie_x:(die_x+1)*subdie_x
%          plot([k k],[2*subdie_y (die_y-2)*subdie_y], 'Color', [.31,.30,.30])
%     end
%
% gridlines y
for k = 0:subdie_y
    plot([2*subdie_x die_x*subdie_x - 2*subdie_x], [k k], 'Color', [.71,.80,.80])
end

for k = subdie_y:2*subdie_y
    plot([subdie_x die_x*subdie_x - subdie_x], [k k], 'Color', [.71,.80,.80])
end
for o=2*subdie_y:die_y*subdie_y - 2*subdie_y
    plot([0 die_x*subdie_x], [o o], 'Color', [.71,.80,.80])
end

for k = die_y*subdie_y - 2*subdie_y:subdie_y*die_y - subdie_y
    plot([subdie_x die_x*subdie_x - subdie_x], [k k], 'Color', [.71,.80,.80])
end

for k = die_y*subdie_y - subdie_y:die_y*subdie_y
    plot([2*subdie_x die_x*subdie_x - 2*subdie_x], [k k], 'Color', [.71,.80,.80])
end

% big x-grid
plot([0 0],[2*subdie_y (die_y-2)*subdie_y],'-black')
plot([1*subdie_x 1*subdie_x],[subdie_y (die_y-1)*subdie_y],'-black')
for o=2:die_x-2
    plot([o*subdie_x o*subdie_x],[0 die_y*subdie_y],'-black')
end
plot([(die_x)*subdie_x-subdie_x (die_x)*subdie_x-subdie_x],[subdie_y (die_y-1)*subdie_y],'-black')
plot([(die_x)*subdie_x (die_x)*subdie_x],[2*subdie_y (die_y-2)*subdie_y],'-black')
% big y-grid
plot([2*subdie_x (die_x-2)*subdie_x],[0 0],'-black')
plot([1*subdie_x (die_x-1)*subdie_x],[1*subdie_y 1*subdie_y],'-black')
for o=2:die_y-2
    plot([0 die_x*subdie_x],[o*subdie_y o*subdie_y],'-black')
end
plot([1*subdie_x (die_x-1)*subdie_x],[(die_y-1)*subdie_y (die_y-1)*subdie_y],'-black')
plot([2*subdie_x (die_x-2)*subdie_x],[(die_y)*subdie_y (die_y)*subdie_y],'-black')

%% Generate Curve for the notch
% bottom
if notch_pos == 1
    X = [die_x/1.89*subdie_x-(2*subdie_x) (die_x/1.89*subdie_x)+2*subdie_x];
    Y = [-0.13*subdie_y -0.13*subdie_y];
    % right
elseif notch_pos == 2
    X = [(die_x*subdie_x + 0.13) (die_x*subdie_x + 0.13)];
    Y = [(.5*die_y*subdie_y - .3*die_y*subdie_y) (.5*die_y*subdie_y + .3*die_y*subdie_y)];
    % left
elseif notch_pos == 3
    X = [-0.13*subdie_x -0.13*subdie_x];
    Y = [(.5*die_y*subdie_y - .3*die_y*subdie_y) (.5*die_y*subdie_y + .3*die_y*subdie_y)];
    % top
else
    X = [die_x/1.89*subdie_x-(2*subdie_x) (die_x/1.89*subdie_x)+2*subdie_x];
    Y = [(die_y*subdie_y + 0.13) (die_y*subdie_y + 0.13)];
end

%% plot circle
pos = [-0.05*die_x*subdie_x/2 -0.05*die_y*subdie_y/2 die_x*subdie_x+0.05*die_x*subdie_x die_y*subdie_y+0.05*die_y*subdie_y];
rectangle('Position', pos, 'Curvature', [1 1], 'LineWidth', 1.5);

pos_blue_circle = [-0.035*die_x*subdie_x/2 -0.035*die_y*subdie_y/2 die_x*subdie_x+0.035*die_x*subdie_x die_y*subdie_y+0.035*die_y*subdie_y];
rectangle('Position', pos_blue_circle, 'Curvature', [1 1], 'LineWidth', 1.2, 'EdgeColor', 'blue');

%% plot notch
plot(X, Y, 'black', 'LineWidth', 8)
hold off

%% save
path_wafermap_pic = [plotsave_dir 'Wafermap' parameter_name '.png'];
saveas(gcf, path_wafermap_pic);
close(gcf);

write_wafermap_tex(fileID, path_wafermap_pic)

end