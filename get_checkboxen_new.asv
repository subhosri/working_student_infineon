function [Results_String,Index,p] = get_checkboxen_new (app,It_2,Vt_2,data_matrix)
% -----------------------------------------------------------------------
% This function reads the value from the checkboxes and returns the selected 
% parameters of each dataset
% Called in the SOA_mode2_App.mlapp
% -----------------------------------------------------------------------
% Inputs:
% app: calling App
% -----------------------------------------------------------------------
% Outputs:
% Results_String: all data selected by the user for all visible datasets
% Index: Bool array with true if selected
% -----------------------------------------------------------------------
Val_Module_Name= get(app.chkboxmodulename, 'Value');
Val_Device_Name= get(app.Device_name_legend, 'Value');
Val_DeviceNb=get(app.Device_number_legend, 'Value');
Val_PinVsGnd=get(app.Pulse_Vs_gnd_legend, 'Value');
Val_Bias= get(app.Bias1_legend, 'Value');
Val_Bias2 = get(app.Bias2, 'Value');
Val_Coordinate= get(app.Coord, 'Value');
Val_Lot = get(app.CheckBox_Lot,'Value');
Val_WaferNb = get(app.CheckBox_waferNb,'Value');
Val_Vhold = get(app.CheckBox_VHold,'Value');
Val_Test_chip = get(app.Test_chip_checkbox,'Value');
Val_Spot1 = get(app.Spot1CheckBox,'Value');
Val_Spot2 = get(app.Spot2CheckBox,'Value');



if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value,'SPOT') || strcmp(app.LIST_SOA.Value, 'IV Curve with SPOT')
    Val_IT2 = get(app.IT2_check,'Value');
    Val_VT2 = get(app.VT2_check,'Value');
else    
    Val_IT2 = false;
    Val_VT2 = false;
end

if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value, 'IV Curve with SPOT')
    Val_VatI = get(app.V_at_I_Checkbox_L,'Value');
    Val_IatV = get(app.I_at_V_Checkbox_L,'Value');
else    
    Val_VatI = false;
    Val_IatV = false;
end

if strcmp(app.LIST_SOA.Value,'IV Curve') || strcmp(app.LIST_SOA.Value, 'IV Curve with SPOT')
    Val_R_on1 = get(app.Ron1_Checkbox_L,'Value');
    Val_R_on2 = get(app.Ron2_checkbox_L,'Value');
else    
   Val_R_on1 = false;
   Val_R_on2 = false;
end

Results = [Val_Module_Name,Val_Device_Name,Val_DeviceNb,Val_Coordinate,Val_PinVsGnd,Val_Bias,Val_Lot,Val_WaferNb,Val_IT2,Val_VT2,Val_Vhold,Val_VatI,Val_IatV,Val_R_on1,Val_R_on2,Val_Test_chip,Val_Spot1,Val_Spot2 ]; %,Val_Bias2
Index = find(Results);

%% Open a waitbar
h = waitbar(0,'Legend is being analyzed...','Position',[290 310 620 410], 'OuterPosition',[320 300 275 120], 'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
waitObject = onCleanup(@() delete(h));

if Val_Bias | Val_Bias2
    for i = 1:size(app.measure_params,1)
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'TLP_data.csv'), 'data.txt'); % generate the path for the data.txt file
        if isfile(Path_txt)
            table_txt = readtable(Path_txt);
            if Val_Bias
                if any(strcmp('Bias1_V',table_txt.Properties.VariableNames)) % Checking if -(-Bias1_V & VAL_bias) in other words: ensure that there is a bias if the checkbox is checked
                    Bias1_Value = table_txt{1,'Bias1_V'};
                    if (Bias1_Value > -0.0001) && (Bias1_Value < 0)
                        Bias1_Value = 0;
                    else
                        Bias1_Value=  round(Bias1_Value,3,'significant');
                    end
                    Bias1_Value= string(Bias1_Value);
                else
                    % Throw error if Bias 1 is not available if it is requested
                    error_dlg = errordlg ('Bias 1 is not available !');
                    waitfor(error_dlg);
                    fclose all;
                    return;
                end
            end
            if Val_Bias2
                if any(strcmp('Bias2_V',table_txt.Properties.VariableNames))
                    Bias2_Value = table_txt{1,'Bias2_V'};
                    Bias2_Value=  round(Bias2_Value,3,'significant');
                    Bias2_Value= string(Bias2_Value);
                else
                    % Throw error if Bias 2 is not available if it is requested
                    error_dlg = errordlg ('Bias 2 is not available !');
                    waitfor(error_dlg);
                    fclose all;
                    return;
                end
            end
            
            %% combination of the strings
            if Val_Bias && Val_Bias2
                app.measure_params{i,'Bias'} = strcat("Bias1_",Bias1_Value,"V_","Bias2_",Bias2_Value,"V_");
            elseif Val_Bias && ~Val_Bias2
                app.measure_params{i,'Bias'} = strcat (app.measure_params{i,'Bias Pin'}).';
            elseif ~Val_Bias && Val_Bias2
                app.measure_params{i,'Bias'} = strcat (app.measure_params{i,'Bias Pin'}).';
            end
        else
            % Throw error if the data.txt file is not available
            error_dlg = errordlg ('data.txt is not available !');
            waitfor(error_dlg);
            fclose all;
            return;
        end
    end
end

%VatI = zeros(1,s);

if Val_VatI 
    Idef = get(app.IdefEditField,'Value');
  
    for i = 1:size(app.measure_params,1)
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
        data_cell = readcell(Path_txt,'Delimiter',',');
        [xIn,~] = find(strcmp(data_cell,'Index'));
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);
        [x,y] = find(strcmp(data_cell,'Die'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                Die = data_cell{x,y+1};
            end
        end
        [x,y] = find(strcmp(data_cell,'Device Name'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                DeviceName = data_cell{x,y+1};
            end
        end
        if isempty(data_matrix)
            msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
            warndlg(msg ,'File Error');
        else
            %Calculation for VatI

            if Idef == 0
                %if Idef > data_matrix(size(data_matrix, 1)-1,3) || Idef < data_matrix(1,3)
                %errordlg('V could not be calculated for given Idef ');

                errordlg('Give the value for current under Correlation tab','Inputs not given');

            else
                c = data_matrix(:,3);
                v = data_matrix(:,2);
                [n, bin] = histc(c, unique(c));
                multiple = find(n > 1);
                index_c = find(ismember(bin, multiple)); % index of the duplicate elements in current
                [n, bin] = histc(v, unique(v));
                multiple = find(n > 1);
                index_v = find(ismember(bin, multiple)); % index of the duplicate elements in voltage if any
                if isempty(index_v) == 0
                    c(index_v)=[]
                    v(index_v)=[]
                    VatI = interp1(c, v, Idef);
                else
                    VatI = interp1(data_matrix(:,3), data_matrix(:,2), Idef);
                end
%                 for r=1:(size(data_matrix, 1)-1)
%                     if data_matrix(r,3)<=Idef && Idef<=data_matrix(r+1,3)
%                         VatI=(Idef-data_matrix(r,3))/(data_matrix(r+1,3)-data_matrix(r,3))*(data_matrix(r+1,2)-data_matrix(r,2))+data_matrix(r,2);
%                         break
%                     end
%                 end
            end
            %app.measure_params{i,'VatI'} = "V @ I_"+add_prefix(VatI(1))+"V";
            if Idef < min(data_matrix(:,3)) || Idef > max(data_matrix(:,3))
                ;
            else

                app.measure_params{i,'VatI'} = add_prefix(VatI(1))+" V "+' @ '+ Idef + ' A '
            end
        end
       
    end
    
end

if Val_IatV
    Vdef = get(app.VdefEditField,'Value');
    for i = 1:size(app.measure_params,1)
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
        data_cell = readcell(Path_txt,'Delimiter',',');
        [x,y] = find(strcmp(data_cell,'Die'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                Die = data_cell{x,y+1};
            end
        end
        [x,y] = find(strcmp(data_cell,'Device Name'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                DeviceName = data_cell{x,y+1};
            end
        end
        [xIn,~] = find(strcmp(data_cell,'Index'));
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);
        if isempty(data_matrix)
            msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
            warndlg(msg ,'File Error');
        else
            % Calculation for IatV
            if Vdef == 0
                errordlg('Give the value for voltage under Correlation tab','Inputs not given');

                %if Vdef < min(data_matrix(:,2)) || Vdef > max(data_matrix(:,2))
                % errordlg('I could not be calculated for given Vdef ');

            else
                c = data_matrix(:,3);
                v = data_matrix(:,2);
                [n, bin] = histc(c, unique(c));
                multiple = find(n > 1);
                index_c = find(ismember(bin, multiple)); % index of the duplicate elements in current
                [n, bin] = histc(v, unique(v));
                multiple = find(n > 1);
                index_v = find(ismember(bin, multiple)); % index of the duplicate elements in voltage if any
                if isempty(index_v) == 0
                    c(index_v)=[]
                    v(index_v)=[]
                    IatV = interp1(v, c, Vdef);
                else
                    IatV = interp1(data_matrix(:,2), data_matrix(:,3), Vdef);
                end
                
%                 for s=1:(size(data_matrix, 1)-1)
%                     if data_matrix(s,2)<=Vdef && Vdef<=data_matrix(s+1,2)
%                         IatV = (Vdef-data_matrix(s,2))/(data_matrix(s+1,2)-data_matrix(s,2))*(data_matrix(s+1,3)-data_matrix(s,3))+data_matrix(s,3)
%                         break
%                     end
%                 end
            end
            %app.measure_params{i,'IatV'} = "I @ V_"+add_prefix(IatV(1))+"A";
            if Vdef < min(data_matrix(:,2)) || Vdef > max(data_matrix(:,2))
                ;
            else
                app.measure_params{i,'IatV'} = add_prefix(IatV(1))+" A "+' @ '+ Vdef + ' V ';
            end
        end
    end
end
 
%%% Calculation for R_on1
imin = get (app.min_current_R_on1_2,'Value');
imax = get (app.max_current_R_on1_2,'Value');
umin = get (app.min_voltage_R_on1_3,'Value');
umax = get (app.max_voltage_R_on1_3,'Value');


% Value_criterion_mode = get(app.criterion_mode,'Value');
% Items_criterion_mode = get(app.criterion_mode,'Items');
% is_cr = cellfun(@(x)isequal(x,Value_criterion_mode),Items_criterion_mode);
% criterion_index= find(is_cr);
% extraction_threshold = app.ExtractionThrEditField.Value;
% Min_Max_Limit = app.absLimitAEditField.Value;
% min_fail = str2double(app.minfailDropDown.Value);
% Max_Variation = app.LimitMaxVarEditField.Value;



if Val_R_on1==1
    %Vt_2 = zeros(1,s);
    %It_2 = zeros(1,s);
    for i = 1:size(app.measure_params,1)
        %C{i} = app.X(1,i).Color;
        %[It_2(i), Vt_2(i), ~,crit_spot,spot_used] = Criteria_SOA(app.data_list{i}, criterion_index, extraction_threshold,...
           % Min_Max_Limit, min_fail, Max_Variation,app.spotLimit(i,:));
        %It2 = 7.67;
        %Vt2 = 7.03;
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
        data_cell = readcell(Path_txt,'Delimiter',',');
        [x,y] = find(strcmp(data_cell,'Die'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                Die = data_cell{x,y+1};
            end
        end
        [x,y] = find(strcmp(data_cell,'Device Name'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                DeviceName = data_cell{x,y+1};
            end
        end
        [xIn,~] = find(strcmp(data_cell,'Index'));
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);

        if isempty(data_matrix)
            msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
            warndlg(msg ,'File Error');
            R_dyn= NaN;
        else
            C = {'k','b','r','g','c','y','m',[.5 .6 .7],[.8 .2 .6],[0 0.4470 0.7410],[0.8500 0.3250 0.0980],[0.9290 0.6940 0.1250],[0.4940 0.1840 0.5560],[0.4660 0.6740 0.1880],[0.3010 0.7450 0.9330],[0.6350 0.0780 0.1840]}
            %C{i} = app.X(1,i).Color;
            if all(data_matrix(:,3) < 0)
                threshold = -0.010;
                signe=-1;
            else
                threshold = 0.010;
                signe=1;
            end
            c = data_matrix(:,3); % data_matrix for current values
            [neg ~] = find(c(:)<0)
            neg_no = size(neg,1)
            ratio_neg = size(neg,1)/size(c,1)
            per_neg = ratio_neg * 100
            if imax == 0 && imin == 0  && umin == 0 && umax == 0
                %                 if signe>0
                %                     start_index = find(data_matrix(:,3)>threshold,1);        % Bei INDEX -- Threshold  > 0.15
                %                     %stop_index = find (data_matrix(:,3)>= It_2(i),1);
                %                     stop_index = find (data_matrix(:,3)>= max(data_matrix(:,3)),1);
                %                 else
                %                     start_index = find(data_matrix(:,3)<threshold,1);        % Bei INDEX -- Threshold  > 0.15
                %                     %stop_index = find(data_matrix(:,3)<= It_2(i),1);
                %                     stop_index = find(data_matrix(:,3)<= max(data_matrix(:,3)),1);
                %                 end
                errordlg('Give current or voltage range before selecting R_on1 checkbox','Inputs not given');
            elseif umin==0 && umax==0
                %%%%STROMBEREICH Berechnung über die Oberfläche %%%
                fit_max=imax;   % Obere LIMIT Strombereich [A]
                fit_min=imin;   %Untere LIMIT Strombereich [A]
                if abs(fit_min) > abs(threshold)
                    if per_neg > 60
                        stop_index = find(abs(data_matrix(:,3))>abs(fit_min),1); 
                    else
                        start_index = find(abs(data_matrix(:,3))>abs(fit_min),1); % for positive currents
                    end
                end
                if per_neg > 60
                    if abs(fit_max) < abs(min(data_matrix(:,3)))
                        start_index_u= find (abs(data_matrix(:,3))>abs(fit_max),1);
                        start_index=start_index_u-1;
                    elseif fit_max > min(data_matrix(:,3)) || fit_min < max(data_matrix(:,3))
                    %                       fit_max = max(data_matrix(:,2));
                    %                       %fit_min = min(data_matrix(:,2));
                    %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                    %stop_index= find(abs(data_matrix(:,3)), 1, 'last' ) -2 ;
                    [M,start_index_u] = min(data_matrix(:,2));
                    start_index = start_index_u-1;
                    error_msg = ['The inputs given are out of the range for ',DeviceName, ' ',  Die, '.Calculations of R_on1 are done taking max or min possible value'];
                    warndlg(error_msg ,'Out of bound Error');

                    %                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
                    %                     errordlg('The inputs given are out of the range','Out of range error')
                    else
                    %stop_index_u= find (abs(data_matrix(:,3))>abs(It_2(i),1));
                    %stop_index_u= find (abs(data_matrix(:,3))> abs(max(data_matrix(:,3)),1));
                    start_index_u= find (abs(data_matrix(:,3))> abs(min(data_matrix(:,3))));
                    start_index=start_index_u-1;
                    end
                else
                    if abs(fit_max) < abs(max(data_matrix(:,3)))
                        stop_index_u= find (abs(data_matrix(:,3))>abs(fit_max),1);
                        stop_index=stop_index_u-1;
                    elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
                    %                       fit_max = max(data_matrix(:,2));
                    %                       %fit_min = min(data_matrix(:,2));
                    %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                    %stop_index= find(abs(data_matrix(:,3)), 1, 'last' ) -2 ;
                    [M,stop_index_u] = max(data_matrix(:,2));
                    stop_index = stop_index_u-1;
                    error_msg = ['The inputs given are out of the range for ',DeviceName, ' ',  Die, '.Calculations of R_on1 are done taking max or min possible value'];
                    warndlg(error_msg ,'Out of bound Error');

                    %                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
                    %                     errordlg('The inputs given are out of the range','Out of range error')
                    else
                    %stop_index_u= find (abs(data_matrix(:,3))>abs(It_2(i),1));
                    %stop_index_u= find (abs(data_matrix(:,3))> abs(max(data_matrix(:,3)),1));
                    stop_index_u= find (abs(data_matrix(:,3))> abs(min(data_matrix(:,3))));
                    stop_index=stop_index_u-1;
                    
                    end
                end
                
%                 %if abs(fit_max) < abs(max(data_matrix(:,3)))
%                 if abs(fit_max) < abs(min(data_matrix(:,3)))
% %                     stop_index_u= find (abs(data_matrix(:,3))>abs(fit_max),1);
% %                     stop_index=stop_index_u-1;
%                       start_index_u= find (abs(data_matrix(:,3))>abs(fit_max),1);
%                       start_index=start_index_u-1;
% 
%                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
%                     %                       fit_max = max(data_matrix(:,2));
%                     %                       %fit_min = min(data_matrix(:,2));
%                     %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     %stop_index= find(abs(data_matrix(:,3)), 1, 'last' ) -2 ;
%                     [M,stop_index_u] = max(data_matrix(:,2));
%                     stop_index = stop_index_u-1;
%                     error_msg = ['The inputs given are out of the range for ',DeviceName, ' ',  Die, '.Calculations of R_on1 are done taking max or min possible value'];
%                     warndlg(error_msg ,'Out of bound Error');
% 
%                     %                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
%                     %                     errordlg('The inputs given are out of the range','Out of range error')
%                 else
%                     %stop_index_u= find (abs(data_matrix(:,3))>abs(It_2(i),1));
%                     %stop_index_u= find (abs(data_matrix(:,3))> abs(max(data_matrix(:,3)),1));
%                     stop_index_u= find (abs(data_matrix(:,3))> abs(min(data_matrix(:,3))));
%                     stop_index=stop_index_u-1;
%                 end
%                 %%%%SPANNUNGSBEREICH Berechnung über die Oberfläche %%%
            elseif imax==0 && imin==0
                fit_max=umax;   % Obere LIMIT Strombereich [A]
                fit_min=umin;   %Untere LIMIT Strombereich [A]
                if per_neg > 60
                    stop_index = find(abs(data_matrix(:,2))>abs(fit_min),1);
                else
                   start_index = find(abs(data_matrix(:,2))>abs(fit_min),1); 
                end
                %start_index = find(abs(data_matrix(:,2))>abs(fit_min),1);
                %stop_index = find(abs(data_matrix(:,2))>abs(fit_min),1);
                %if abs(fit_max) < abs(Vt_2(i))
                if per_neg > 60
                    if abs(fit_max) < abs(min(data_matrix(:,2)))
                        start_index_u= find (abs(data_matrix(:,2))>abs(fit_max),1);
                        start_index=start_index_u-1;
                    elseif  abs(fit_max) > min(data_matrix(:,2)) || abs(fit_min) < max(data_matrix(:,2))
                        %                       fit_max = max(data_matrix(:,2));
                        %                       %fit_min = min(data_matrix(:,2));
                        %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                        %stop_index= find(sort(data_matrix(:,2)), 1, 'last' )  ;
                        [M,start_index_u] = min(data_matrix(:,2));
                        start_index = start_index_u-1;
                        msg = ['The inputs given are out of the range for ', DeviceName, ' ' ,Die, '.Calculations of R_on1 are done taking max or min possible value'];
                        warndlg(msg ,'Out of bound Error');

                    else
                        %stop_index_u= find (abs(data_matrix(:,2))>abs(Vt_2(i)),1);
                        start_index_u= find (sort(data_matrix(:,2))>abs(min(data_matrix(:,2)),1));
                        start_index=start_index_u-1;
                    end
                else
                    if abs(fit_max) < abs(max(data_matrix(:,2)))
                        start_index_u= find (abs(data_matrix(:,2))>abs(fit_max),1);
                        start_index=start_index_u-1;
                    elseif  abs(fit_max) > max(data_matrix(:,2)) || abs(fit_min) < min(data_matrix(:,2))
                        %                       fit_max = max(data_matrix(:,2));
                        %                       %fit_min = min(data_matrix(:,2));
                        %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                        %stop_index= find(sort(data_matrix(:,2)), 1, 'last' )  ;
                        [M,stop_index_u] = max(data_matrix(:,2));
                        stop_index = stop_index_u-1;
                        msg = ['The inputs given are out of the range for ', DeviceName, ' ' ,Die, '.Calculations of R_on1 are done taking max or min possible value'];
                        warndlg(msg ,'Out of bound Error');

                    else
                        %stop_index_u= find (abs(data_matrix(:,2))>abs(Vt_2(i)),1);
                        stop_index_u= find (sort(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                        stop_index=stop_index_u-1;

                    end


                end
                %if abs(fit_max) < abs(max(data_matrix(:,2)))
%                 if abs(fit_max) < abs(min(data_matrix(:,2)))
% %                     stop_index_u= find (abs(data_matrix(:,2))>abs(fit_max),1);
% %                     stop_index=stop_index_u-1;
%                       start_index_u= find (abs(data_matrix(:,2))>abs(fit_max),1);
%                       start_index=start_index_u-1;
% 
%                 elseif  abs(fit_max) > max(data_matrix(:,2)) || abs(fit_min) < min(data_matrix(:,2))
%                     %                       fit_max = max(data_matrix(:,2));
%                     %                       %fit_min = min(data_matrix(:,2));
%                     %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     %stop_index= find(sort(data_matrix(:,2)), 1, 'last' )  ;
%                     [M,stop_index_u] = max(data_matrix(:,2));
%                     stop_index = stop_index_u-1;
%                     msg = ['The inputs given are out of the range for ', DeviceName, ' ' ,Die, '.Calculations of R_on1 are done taking max or min possible value'];
%                     warndlg(msg ,'Out of bound Error');
% 
%                 else
%                     %stop_index_u= find (abs(data_matrix(:,2))>abs(Vt_2(i)),1);
%                     stop_index_u= find (sort(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     stop_index=stop_index_u-1;
%                 end

            end
            R_dyn = nan(1);
            fit_neu = nan(1);
            if isempty (start_index) || start_index == stop_index + 1 || start_index > stop_index
                start_index=1;
                if start_index > stop_index || start_index == stop_index + 1 || start_index == stop_index
                    stop_index = min(size(data_matrix, 1), 3);
                end
                v_values=data_matrix(start_index:stop_index,2);
                i_values=data_matrix(start_index:stop_index,3);

                if size(v_values, 1) > 1
                    r_fit=fit(v_values,i_values,'poly1');
                    r_fitCoeff = coeffvalues(r_fit);
                    a_1= r_fitCoeff(1,1);
                    a_0= r_fitCoeff(1,2);
                    R_dyn = 1/a_1
                    fit_neu =a_0+a_1*v_values
                end
            else

                v_values=data_matrix(start_index:stop_index,2);
                i_values=data_matrix(start_index:stop_index,3);

                if size(v_values, 1) > 1
                    r_fit=fit(v_values,i_values,'poly1');
                    r_fitCoeff = coeffvalues(r_fit);
                    a_1= r_fitCoeff(1,1);
                    a_0= r_fitCoeff(1,2);

                    R_dyn= 1/a_1

                    fit_neu=a_0+a_1*v_values
                end
            end
            app.measure_params{i,'Val_R_on1'} = "Val_R_on1_"+add_prefix(R_dyn(1))+" ohm";


            range = data_matrix(start_index:stop_index, 2);
            %col=['g', 'm', 'r'];
            % plotting
            if strcmp(app.LIST_SOA.Value,'IV Curve')
                p = plot(app.axes1,range,fit_neu, 'Color', C{i}, 'LineStyle',':', 'LineWidth', 2.0);
            else
                p = plot(app.UIAxes,range,fit_neu, 'Color', C{i}, 'LineStyle',':', 'LineWidth', 2.0);
            end

            %h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0)
                 %set(p,'Visible','off')
        end
            
    end

end



%%% Calculation for R_on2
imin2 = get (app.min_current_R_on2,'Value');
imax2 = get (app.max_current_R_on2,'Value');
umin2 = get (app.min_voltage_R_on2,'Value');
umax2 = get (app.max_voltage_R_on2,'Value');

if Val_R_on2==1
   
    for i = 1:size(app.measure_params,1)
        
        Path_txt = strcat(erase(app.measure_params{i,'PATH'},'data.txt'));
        data_cell = readcell(Path_txt,'Delimiter',',');
        [x,y] = find(strcmp(data_cell,'Die'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                Die = data_cell{x,y+1};
            end
        end
        [x,y] = find(strcmp(data_cell,'Device Name'));
        if not(isempty(x))
            if not(ismissing(data_cell{x,y+1}))
                DeviceName = data_cell{x,y+1};
            end
        end
        [xIn,~] = find(strcmp(data_cell,'Index'));
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);

        if isempty(data_matrix)
            msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
            warndlg(msg ,'File Error');
            %R_dyn= NaN;
        else
            C{i} = app.X(1,i).Color;
            if all(data_matrix(:,3) < 0)
                threshold = -0.010;
                signe=-1;
            else
                threshold = 0.010;
                signe=1;
            end
            c = data_matrix(:,3); % data_matrix for current values
            [neg ~] = find(c(:)<0)
            neg_no = size(neg,1)
            ratio_neg = size(neg,1)/size(c,1)
            per_neg = ratio_neg * 100
            if imax2 == 0 && imin2 == 0  && umin2 == 0 && umax2 == 0
               
                errordlg('Give current or voltage range before selecting R_on2 checkbox','Inputs not given');
            elseif umin2==0 && umax2==0
                %%%%STROMBEREICH Berechnung über die Oberfläche %%%
                fit_max2=imax2;   % Obere LIMIT Strombereich [A]
                fit_min2=imin2;   %Untere LIMIT Strombereich [A]
                if abs(fit_min2) > abs(threshold)
                    if per_neg > 60
                         stop_index2 = find(abs(data_matrix(:,3))>abs(fit_min2),1);
                    else
                         start_index2 = find(abs(data_matrix(:,3))>abs(fit_min2),1);
                    end
                    %start_index2 = find(abs(data_matrix(:,3))>abs(fit_min2),1);
                end
                %if abs(fit_max) < abs(It_2(i))
                if per_neg > 60
                    if abs(fit_max2) < abs(min(data_matrix(:,3)))
                        start_index_u2= find (abs(data_matrix(:,3))>abs(fit_max2),1);
                        start_index2=start_index_u2-1;
                    elseif fit_max2 > max(data_matrix(:,3)) || fit_min2 < min(data_matrix(:,3))
                        %                       fit_max = max(data_matrix(:,2));
                        %                       %fit_min = min(data_matrix(:,2));
                        %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
                        %stop_index= find(abs(data_matrix(:,3)), 1, 'last' ) -2 ;
                        [M2,start_index_u2] = min(data_matrix(:,2));
                        start_index2 = start_index_u2-1;
                        error_msg = ['The inputs given are out of the range for ',DeviceName, ' ',  Die, '.Calculations of R_on2 are done taking max or min possible value'];
                        warndlg(error_msg ,'Out of bound Error');

                        %                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
                        %                     errordlg('The inputs given are out of the range','Out of range error')
                    else
                        %stop_index_u= find (abs(data_matrix(:,3))>abs(It_2(i),1));
                        start_index_u2= find (abs(data_matrix(:,3))> abs(min(data_matrix(:,3)),1));
                        start_index2=start_index_u2-1;
                    end
                else
                    if abs(fit_max2) < abs(max(data_matrix(:,3)))
                        stop_index_u2= find (abs(data_matrix(:,3))>abs(fit_max2),1);
                        stop_index2=stop_index_u2-1;
                    end

                end
%                 if abs(fit_max2) < abs(max(data_matrix(:,3)))
%                     stop_index_u2= find (abs(data_matrix(:,3))>abs(fit_max2),1);
%                     stop_index2=stop_index_u2-1;
%                 elseif fit_max2 > max(data_matrix(:,3)) || fit_min2 < min(data_matrix(:,3))
%                     %                       fit_max = max(data_matrix(:,2));
%                     %                       %fit_min = min(data_matrix(:,2));
%                     %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     %stop_index= find(abs(data_matrix(:,3)), 1, 'last' ) -2 ;
%                     [M2,stop_index_u2] = max(data_matrix(:,2));
%                     stop_index2 = stop_index_u2-1;
%                     error_msg = ['The inputs given are out of the range for ',DeviceName, ' ',  Die, '.Calculations of R_on2 are done taking max or min possible value'];
%                     warndlg(error_msg ,'Out of bound Error');
% 
%                     %                 elseif fit_max > max(data_matrix(:,3)) || fit_min < min(data_matrix(:,3))
%                     %                     errordlg('The inputs given are out of the range','Out of range error')
%                 else
%                     %stop_index_u= find (abs(data_matrix(:,3))>abs(It_2(i),1));
%                     stop_index_u2= find (abs(data_matrix(:,3))> abs(max(data_matrix(:,3)),1));
%                     stop_index2=stop_index_u2-1;
%                 end
%                 %%%%SPANNUNGSBEREICH Berechnung über die Oberfläche %%%
%             elseif imax2==0 && imin2==0
%                 fit_max2=umax2;   % Obere LIMIT Strombereich [A]
%                 fit_min2=umin2;   %Untere LIMIT Strombereich [A]
%                 start_index2 = find(abs(data_matrix(:,2))>abs(fit_min2),1);
%                 %if abs(fit_max) < abs(Vt_2(i))
%                 if abs(fit_max2) < abs(max(data_matrix(:,2)))
%                     stop_index_u2= find (abs(data_matrix(:,2))>abs(fit_max2),1);
%                     stop_index2=stop_index_u2-1;
%                 elseif  abs(fit_max2) > max(data_matrix(:,2)) || abs(fit_min2) < min(data_matrix(:,2))
%                     %                       fit_max = max(data_matrix(:,2));
%                     %                       %fit_min = min(data_matrix(:,2));
%                     %                       %stop_index_u= find (abs(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     %stop_index= find(sort(data_matrix(:,2)), 1, 'last' )  ;
%                     [M2,stop_index_u2] = max(data_matrix(:,2));
%                     stop_index2 = stop_index_u2-1;
%                     msg = ['The inputs given are out of the range for ', DeviceName, ' ' ,Die, '.Calculations of R_on1 are done taking max or min possible value'];
%                     warndlg(msg ,'Out of bound Error');
% 
%                 else
%                     %stop_index_u= find (abs(data_matrix(:,2))>abs(Vt_2(i)),1);
%                     stop_index_u2= find (sort(data_matrix(:,2))>abs(max(data_matrix(:,2)),1));
%                     stop_index2=stop_index_u2-1;
%                 end

            end
            R_dyn2 = nan(1);
            fit_neu2 = nan(1);
            if isempty (start_index2) || start_index2 == stop_index2 + 1 || start_index2 > stop_index2
                start_index2=1;
                if start_index2 > stop_index2 || start_index2 == stop_index2 + 1 || start_index2 == stop_index2
                    stop_index2 = min(size(data_matrix, 1), 3);
                end
                v_values2=data_matrix(start_index2:stop_index2,2);
                i_values2=data_matrix(start_index2:stop_index2,3);

                if size(v_values2, 1) > 1
                    r_fit2=fit(v_values2,i_values2,'poly1');
                    r_fitCoeff2 = coeffvalues(r_fit2);
                    a_12= r_fitCoeff2(1,1);
                    a_02= r_fitCoeff2(1,2);
                    R_dyn2 = 1/a_12
                    fit_neu2 =a_02+a_12*v_values2
                end
            else

                v_values2=data_matrix(start_index2:stop_index2,2);
                i_values2=data_matrix(start_index2:stop_index2,3);

                if size(v_values2, 1) > 1
                    r_fit2=fit(v_values2,i_values2,'poly1');
                    r_fitCoeff2 = coeffvalues(r_fit2);
                    a_12= r_fitCoeff2(1,1);
                    a_02= r_fitCoeff2(1,2);

                    R_dyn2= 1/a_12

                    fit_neu2=a_02+a_12*v_values2
                end
            end
            app.measure_params{i,'Val_R_on2'} = "Val_R_on2_"+add_prefix(R_dyn2(1))+" ohm";


            range2 = data_matrix(start_index2:stop_index2, 2);
            %col=['g', 'm', 'r'];
            plot(app.axes1,range2,fit_neu2, 'Color', C{i}, 'LineStyle',':', 'LineWidth', 0.5,'Marker','*');
            plot(app.UIAxes,range2,fit_neu2, 'Color', C{i}, 'LineStyle',':', 'LineWidth', 0.5,'Marker','*');
            %h = findobj(app.axes1,'Color', C{i} ,'LineStyle',':','LineWidth', 2.0)
                 %set(p,'Visible','off')
        end
            
    end

end












if any(any(strcmp("",app.measure_params{:,Index}))) 
    error_dlg = errordlg ('Not possible to find all demanded parameter, PLS pick another set of parameters');
    waitfor(error_dlg);
    fclose all;
    return
end
Results_String = app.measure_params{:,Index};

end
