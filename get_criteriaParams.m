function [criteria_matrix,spot1LimitMAX,spot1LimitMIN,spot2LimitMAX,spot2LimitMIN] = get_criteriaParams(data_cell)
%GET_CRITERIAPARAMS Summary of this function goes here
%   Detailed explanation goes here
criteria_matrix = cell(2,5);
for i=1:2
    FailureName = ['Spot' num2str(i)];
    criteria_matrix(i,1) = cellstr(FailureName);
    
    [x,y] = find(strcmp(data_cell,[FailureName ' Value']));
    if not(isempty(data_cell{x,y}))
        criteria_matrix(i,2) = cellstr(data_cell{x,y+1});
    end
    [x,y] = find(strcmp(data_cell,[FailureName ' Compliance']));
    if not(isempty(data_cell{x,y}))
        criteria_matrix(i,3) = cellstr(string(data_cell{x,y+1}));
    end
end

[x,y] = find(strcmp(data_cell,'Spot1 Limit Max'));
if not(isempty(data_cell{x,y}))
    spot1LimitMAX = data_cell{x,y+1};
end

[x,y] = find(strcmp(data_cell,'Spot1 Limit Min'));
if not(isempty(data_cell{x,y}))
    spot1LimitMIN = data_cell{x,y+1};
end

[x,y] = find(strcmp(data_cell,'Spot2 Limit Max'));
if not(isempty(data_cell{x,y}))
    spot2LimitMAX = data_cell{x,y+1};
end

[x,y] = find(strcmp(data_cell,'Spot2 Limit Min'));
if not(isempty(data_cell{x,y}))
    spot2LimitMIN = data_cell{x,y+1};
end
end

