function [data_matrix,dieValid,Die,DeviceName, ModuleDie1, Pin1,Pin2,Operator,genParam,data_cell] = get_data(path)
% GET_data loads the data from the given path and exptracts information
% from the file header
Die = '';
DeviceName = '';
ModuleDie1 = '';
Pin1 = '';
Pin2 = '';
Operator = '';
TestChip ='';
try

    data_cell = readcell(path,'Delimiter',',');
    [x,y] = find(strcmp(data_cell,'Die'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            Die = data_cell{x,y+1};
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Device Name'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            DeviceName = data_cell{x,y+1};
        end
    end


    [x,y] = find(strcmp(data_cell,'Test Chip'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            TestChip = data_cell{x,y+1};
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Device Number'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            ModuleDie1 =  int2str(data_cell{x,y+1});
            % ModuleDie = regexp(ModuleDie1,'\d*','Match');
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Pulse Pin'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            Pin1 = char(string(data_cell{x,y+1}));
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Gnd Pin'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            Pin2 = char(string(data_cell{x,y+1}));
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Operator'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            Operator = data_cell{x,y+1};
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Pulse Shape'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            pulseShape = data_cell{x,y+1};
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Rise Time'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            riseTime = data_cell{x,y+1};
        end
    end
    
    [x,y] = find(strcmp(data_cell,'Avg. Interval V Start'));
    if not(isempty(x))
        if not(ismissing(data_cell{x,y+1}))
            avgInterVStart = data_cell{x,y+1};
            avgInterVStop = data_cell{x+1,y+1};
            avgInterIStart = data_cell{x+2,y+1};
            avgInterIStop = data_cell{x+3,y+1};
        end
    end
    
    % Build a struct to make it easyer to work with the parameter
    genParam = struct('pulseshape',pulseShape,'risetime',riseTime,...
        'Avg_Inter_VStart',avgInterVStart,'Avg_Inter_VStop',avgInterVStop,...
        'Avg_Inter_IStart',avgInterIStart,'Avg_Inter_IStop',avgInterIStop);
    
    % Check if there are measurements and put them in the corresponidng variable
    [xIn,~] = find(strcmp(data_cell,'Index'));
    if length(data_cell) > xIn
        data_matrix = cell2mat(data_cell(xIn+1:end,:));
        data_matrix = data_matrix(:,2:end);
        dieValid = true;
    else
        dieValid = false;
        data_matrix = [];
        if ~isempty(Die)&~isempty(DeviceName)
            msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
        else
            msg = ['TLP_data.csv for is empty/could not be found' ];
        end
        warndlg(msg ,'File Error');
    end

catch ME
    ME.message
    data_matrix = [];
    if ~isempty(Die)|~isempty(DeviceName)
        msg = ['TLP_data.csv for ', DeviceName, ' ', Die, ' is empty' ];
    else
        msg = ['TLP_data.csv for is empty/could not be found' ];
    end
    warndlg(msg ,'File Error'); % detect if the file empty or not
    dieValid = false;
    genParam = [];
end
end

