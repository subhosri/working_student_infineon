function varargout = mywaitbar(varargin)
% -----------------------------------------------------------------------
% waitbar
% Still in use in the GTS mode but should be replaced with the stock
% version
% -----------------------------------------------------------------------
% Inputs:
% varargin:  
% -----------------------------------------------------------------------

    width  = 400;                 % Figure width 
    height = 67;                  % Figure height
    scSize = get(0,'screensize'); % Screensize
    
if varargin{1} == 1
    wb.fig = figure('position',[(scSize(3)-width)/2 (scSize(4)-height)/2 width height],'menubar','none','toolbar','none','numbertitle','off','color',240/255*[1 1 1],'resize','off'); % Open a figure that will contain the objects
    set(wb.fig,'name','Please wait...');     % Define figure's title.
    set(gcf, 'GraphicsSmoothing','off') 
    wb.txt = uicontrol('style','text','pos',[10 42 385 17], 'String',varargin{2},'backgroundcolor',[1 1 1] *240/255,'horizontalalignment','left','fontname','helvetica','fontsize',10);
    [~, result] = system('set PATH');
    executableFolder = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once'));
    path_gif = ['<html><img src="file:/' executableFolder '\waitingbar.gif"/></html>'];
%      path_gif = ['<html><img src="file:/C:\Program Files\ESD_Analyze\application\waitingbar.gif"/></html>'];
    uicontrol('style','push', 'pos',[160 39 210 15], 'String',path_gif,'enable','inactive','CData',uint8(240*ones(18,18,3))); 
    drawnow; % Update figure's content
    varargout{1} = wb.fig;

elseif varargin{1} == 2
    
     h = waitbar(0,'Data is being analyzed...','Position',[(scSize(3)-width)/2 (scSize(4)-height)/2 width height], 'OuterPosition',[460 260 275 120], 'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
     titleHandle = get(findobj(h,'Type','axes'),'Title');
     set(titleHandle,'FontSize',20)
     setappdata(h,'canceling',0)
%      waitObject = onCleanup(@() delete(h)); 
     varargout{1} = h;
end
