function [pathstrc, n] = path2strc(sPath)
    %augerufen im TLP_analyse_vTG_v07_12
    
    pathstrc = struct('path',repmat({cell(1,1)},1,length(sPath)), 'pathfields',repmat({cell(1,11)},1,length(sPath)));
    for n=1:length(sPath)
        % array bs contains indices of the positions of a \ in the path
        bs = strfind(sPath{n}, '\');                                        % find positions backslash and return in a row
        nobs = length(bs);                                                  % number of backslashes
        txt = cell (1,nobs);
        for k=1:(nobs-1)
            % separate path in single segments
            txt{k} = strrep(sPath{n}(bs(k):bs(k+1)),'\','');                % orig Names{n}
        end

        txt{nobs} = strrep(sPath{n}(bs(nobs):length(sPath{n})),'\','');     % now txt contains all field of the path excluding backslashes
        pathstrc(n).path = sPath{n};                                        % struct with path
        pathstrc(n).pathfields = txt;                                       % cell with one row, each cell containing one folder
    end
end

