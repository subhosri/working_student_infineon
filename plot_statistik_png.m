function [Dies,path_name,xticks_string_scatter] =  plot_statistik_png(parameter_values,pathstrc,device_name)



%  for i=1:size(pathstrc,2)
% %     path_name(:,i) = pathstrc(i).pathfields(end-2);
%  device_name(i)= extractAfter(device_name(i),5);     
%  end
[path_name,iA] = unique(device_name);
iA=sort(iA);
idx = diff(iA);
a = (size(pathstrc,2) - iA(end)) +1;

idx = [idx(1:end)' a];
Dies = repelem(iA,idx);

xticks_string_scatter = ['                xtick=data,\n',...
                        '                xticklabel style={rotate=55,anchor=north east},\n',...
                        '                xticklabels={' strjoin(path_name) '},\n'];
%  xticks_string_rdyn_scatter = ['                xtick=data,\n',...
%                         '                xticklabel style={rotate=55,anchor=north east},\n',...
%                         '                xticklabels={' strjoin(path_name(~isnan(parameter_values.rdyn))) '},\n'];
end    

