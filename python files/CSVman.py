# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 11:05:55 2022

@author: Basu
"""

import sys
from PyQt5.QtWidgets import *
import nbimporter
import pandas as pd

#importing functions from other files
import func as f
import csv
import re

class PyQtLayout(QWidget):
 
    def __init__(self):
        super().__init__()
        self.UI()
        

    #Interface
    def UI(self):

        class DropLabel(QLabel):
            def __init__(self, text):
                super().__init__(text) 
                self.setAcceptDrops(True)


            def dragEnterEvent(self, e):
                print(e)          
                if e.mimeData().hasText():
                    e.accept()
                    print(e.mimeData().text())
                else:
                    e.ignore()

            def dropEvent(self, e):
            #(self,row, column, data, action):
                txt = e.mimeData().text()
                txt=txt.replace("file:///", "")
                print(txt)
                X = f.search_csv(txt)
                start = T.rowCount()
                T.setRowCount(start + len(X))
                i = start
                for x in X:
                    T.setItem(i, 0, QTableWidgetItem(x))
                    read_file = open(x, 'r')
                    L = read_file.readlines()
                    lines = ["Operator", "Technology","Fab","Test Chip","Lot","Wafer","Die","Module","Device Name","Device Number","Pulse Pin","Gnd Pin", "Bias Pin", "Number", "Comments"]
                    res=[]
                    for l in L:
                        if l.split(',')[0] in lines:
                            res.append(l.split(',')[1])
                        
                    j=1
                    while j <= len(res):
                        for res in res:
                            T.setItem(i, j, QTableWidgetItem(res))
                            j+=2
                        i=+1
                        
                    i+=1

        
        ### UI functions ####
        
        
        def read(self):
            new=[]
            headers = []
            for i in range(T.rowCount()):
                for j in range(T.columnCount()):
                    #item_name =  T.item(i,j)
                    header = T.horizontalHeaderItem(j)
                    if header is not None:
                         headers.append(header.text())
            df = pd.DataFrame(columns = headers)
            for i in range(T.rowCount()):
                for j in range(T.columnCount()):
                    item_name =  T.item(i,j)
                    if item_name is not None:
                        df.at[1,headers[j]] = item_name.text()
                        #new.append(item_name)
                       
            dict = df.to_dict('list')
            #print(dict)
            
            path = dict.get('File')[0]
            #print(path)
            read_file = open(path, 'r')
            L = read_file.readlines()
            #print(L)
            res = []
            lines = ["Operator new", "Technology new","Fab new","Test Chip new","Lot new","Wafer new","Die new","Module new","Device Name new","Device Number new","Pulse Pin new","Gnd Pin new", "Bias Pin new", "Number new", "Comments new"]
            for l in L:
                nan = ' '
                if l.split(',')[0] + ' new' in lines and dict.get(l.split(',')[0] + ' new') != None:
                    #fout = open(path, "w")
                    #print(dict.get(l.split(',')[0] + ' new')[0])
                    new_word = str(dict.get(l.split(',')[0] + ' new')[0])
                    if new_word != 'nan':
                        print(new_word)
                        print(l.split(',')[1])
                        f.change_line(path, l.split(',')[1], new_word)
                        #data = (l.split(',')[1]).replace(l.split(',')[1], str(new_word))
                        #write_file = open(path, 'w')
                       
            
                        
                        
                        
            
                    #res.append(l.split(',')[1])
                    #print(dict.get(l.split(',')[0] + ' new')[0])
                    
            #print(res)       
                  
                  
                  
        def exe():
            od = LE_old.text()
            nw = LE_new.text()
            ct = LW.count()
            if od=="" or nw=="" or ct==0:
                showdialog("Please enter text or \n elements")
            else:
                paths = []   
                for i in range(0,ct):
                    txt = LW.item(i).text()
                    print("processing: " + txt)
                    paths.append(txt)
                f.change_line(paths, od, nw)
                showdialog("success")

        def filter():
            od = LE_old.text()
            ct = LW.count()
            if od=="" or ct==0:
                return
            lst = []
            for x in range(0,ct):
                lst.append(T.item(x).text())
            nlst = f.filter_line(lst, od)
            print("nach dem Filtern verbleibend:")
            print(nlst)
            T.clear()
            T.setRowCount(nlst.count())


        ### UI Layout ###
            
        #a set of items of the UI...
        B_read = QPushButton('Change')
        B_read.clicked.connect(read)
        T = QTableWidget(1,31)
        #labels = ("File", "Operator", "new", "Technology","new","Fab", "new","Test Chip", "new","Lot", "new,", "Wafer", "new","Die", "new","Module", "new", \
            #"Device Name", "new","Device Number", "new","Pulse Pin", "new","Gnd Pin", "new", "Bias Pin", "new", "Number", "new", "Comments", "new")
        labels = ("File", "Operator", "Operator new", "Technology","Technology new","Fab", "Fab new","Test Chip", "Test Chip new","Lot", "Lot new,", "Wafer", "Wafer new","Die", "Die new","Module", "Module new", \
            "Device Name", "Device Name new","Device Number", "Device Number new","Pulse Pin", "Pulse Pin new","Gnd Pin", "Gnd Pin new", "Bias Pin", "Bias Pin new", "Comments", "Comments new", "Number", "Number new")
        T.setHorizontalHeaderLabels(labels)

        DL = DropLabel("please drag and drop here")

        #...adding all to the grid
        grid = QGridLayout()
        grid.addWidget(T, 0,0,100,100)
        grid.addWidget(DL, 3,101,-1,-1)
        grid.addWidget(B_read,1,101,2,-1)

        #setting UI Properties
        self.setLayout(grid)
        self.setGeometry(200, 100, 800, 600)
        self.setWindowTitle('CSV Manipulator')
        self.showMaximized()


def showdialog( arg ):
   d = QDialog()
   L = QLabel(arg, d, )
   b1 = QPushButton("Ok",d)
   b1.clicked.connect(d.done)
   b1.move(50,50)
   d.setWindowTitle("Info")
   #d.setWindowModality(Qt.ApplicationModal)
   d.exec_()


def main():
    app = QApplication(sys.argv)
    ex = PyQtLayout()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()