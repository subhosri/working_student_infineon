# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os

from numpy import true_divide

def search_csv( root ):
    "scan folders for csv files"
    paths = []
    print("Found the following TLP_data.csv files:")
    for root, dirs, files in os.walk(root, topdown=False):
        for name in files:
            if 'TLP_data.csv' in name:
                #print(os.path.join(root, name))
                paths.append(os.path.join(root,name))
    return paths


def filter_line( paths, line ):
    "filter given files whether they contain the expected string/line"
    res = []
    for X in paths:
        found = False
        f = open(X, 'r')
        L = f.readlines()
        for x in L:
            if x.split() == line.split() and found==False: # without
                res.append(X)
                found=True
        f.close()
    return res
    #return x

def change_line( paths, old, new ):
    "substitute given line and rewrite the file"
    newcont = ""
    file = open(paths,'r')
    L = file.readlines()
    for line in L:
        if len(line.split(',')) == 2:
            line_cmp = str(line.split(',')[1])
            print(line_cmp)
            if line_cmp == old:
               newcont= newcont + line.split(',')[0] + ',' +  new + "\n"
            else:
               newcont= newcont + line 
        else:
            newcont= newcont + line 
            
            
    file.close()
        # opening the file in write mode
    fout = open(paths, "w")
    fout.write(newcont)
    fout.close() 


def rm_dup( items ):
    "removes duplicates"
    items = list( dict.fromkeys(items) )
    return items

