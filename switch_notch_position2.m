function Results = switch_notch_position2(notch_pos, num_of_samples, x_y, x_step, y_step, subdie_index, x_offset, y_offset, x_size, y_size)
    %berechnen von Wafermap koordinaten
    %Nur USE-THIS-ONE-FOR-NOTCH-DOWN benutzt
    %aufgerufen in wafermap_sandy2    

    % indices: Nummerierung der Matrixelemente, entspricht einem Die
    indices = reshape(1:y_step*x_step, [x_step, y_step])';
    Results = zeros(num_of_samples, 2);
    switch notch_pos
        % USE-THIS-ONE-FOR-NOTCH-DOWN
        case 1
            for i=1:num_of_samples
                % suche Zeile, Spalte der Elementnummer
                %r = y_step - rem(subdie_index(i) - 1, x_step);
                %r = rem(subdie_index(i) - 1, x_step) + 1;
                %c = mod(subdie_index(i) - 1, x_step) + 1;
                [r, c] = find(indices == subdie_index(i));
                % berechne x-y-Koordinate eines chip
                Results(i,1) = x_step*(x_y(i,1) + x_offset) + c;       % calculate chip x
                Results(i,2) = y_step*(x_y(i,2) + y_offset) + r;       % calculate chip y
            end
            
        % NOTCH ON THE RIGHT SIDE
        case 2
            for i=1:size(x_y,1)
                % suche Zeile, Spalte der Elementnummer
                [r, c] = find(indices == subdie_index(i));
                Results(i,1) = (x_step*(x_y(i,2) + y_offset)+r);           %calculate chip x
                Results(i,2) = ((y_size)-(y_step*(x_y(i,1) + x_offset)+c));% calculate chip y
            end
        % USE-THIS-ONE-FOR-NOTCH-LEFT
        case 3
            for i=1:size(x_y,1)
                % suche Zeile, Spalte der Elementnummer
                [r, c] = find(indices == subdie_index(i));
                Results(i,1)=((x_size)-(x_step*(x_y(i,2)+1)+r));        % calculate chip x
                Results(i,2)=(y_step*(x_y(i,1)+2)+c);                   % calculate chip y
            end
        % USE-THIS-ONE-FOR-NOTCH-TOP
        case 4
            for i=1:size(x_y,1)
                % suche Zeile, Spalte der Elementnummer
                [r, c] = find(indices == subdie_index(i));
                Results(i,1)=x_size-(x_step*(x_y(i,2) + y_offset))+1+r;   % calculate chip x
                Results(i,2)=y_size-(y_step*(x_y(i,1) + x_offset))+1+c;   % calculate chip y
            end
    end
end