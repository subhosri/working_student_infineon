function wafermap_sandy2(die_x, die_y, subdie_x, subdie_y, offset_x, offset_y, ALLSAMP, sample_values, hbm_mode, plotsave_dir, notch_pos, dir_structure, colorbar_values, genParam, snapback, target_values, critical_area, cnt_fails)
%augerufen im TLP_analyse_vTG_v07_12
%    load('unittest_files\input_wafermap_sandy_pos.mat');

die_x = die_x + 1;
die_y = die_y + 1;

if isnan(offset_x)
    offset_x = 0;
end
if isnan(offset_y)
    offset_y = 0;
end
contactfail = 0;
samples = sample_values.Samples;
num_of_samples = size(samples, 1);
contact_fails = size(ALLSAMP, 2) - num_of_samples;
nonfails = num_of_samples - cnt_fails;
x_y = zeros(num_of_samples, 2);
for k = 1:num_of_samples
    xfind = strfind(samples(k), 'X');
    yfind = strfind(samples(k), 'Y');
    if isempty(xfind{1}) || isempty(yfind{1})
        warn_dlg = warndlg (['Die-Koordinate ist nicht korrekt: ' samples(k)], 'Data Conflict');
        waitfor(warn_dlg);
        fclose all;
        error('Falsche Koordinaten');
    end
    string_parts = strtrim(strsplit(char(samples(k)), {'X', 'Y'}));
    string_parts = strrep(string_parts, '_', '');
    x_y(k, 1:2) = [str2double(cell2mat(string_parts(:, 2))), str2double(cell2mat(string_parts(:, 3)))];
end
% nummeriere chips pro Die
% coord: x, y, nummer
cnt_dies = zeros(num_of_samples, 1);
for k = 1:num_of_samples
    % z�hle wie viele Subdies in einem Die vorkommen
    % cnt_dies: "Wie oft kommt jedes Die vor"
    t = sum(x_y(1:k, 1) == x_y(k, 1) & x_y(1:k, 2) == x_y(k, 2));
    cnt_dies(k) = t;
end

% stimmt Anzahl der Subdies mit Eingabe des Benutzers �berein?
if subdie_x * subdie_y < max(cnt_dies)
    errordlg ('Die angegebene Anzahl an Subdies ist zu klein.', 'Data Conflict');
    error('Anzahl der Subdies nicht korrekt.');
end

x_size = die_x * subdie_x;
y_size = die_y * subdie_y;

% berechne tats�chlichen Koordinaten
chip = switch_notch_position2(1, num_of_samples, x_y, subdie_x, subdie_y, cnt_dies, offset_x, offset_y, x_size, y_size);
if any(any(chip < 1))
    m = min(x_y);
    errordlg (['Das angegebene Offset f�r die Wafermap ist nicht korrekt. Probieren Sie doch mal: (' num2str(abs((1))) ', ' num2str(abs((2))) ').'], 'Data Conflict');
    error('Offset nicht korrekt.');
end

Wafer_Final_vt2 = -ones(y_size, x_size);
Wafer_Final_it2 = -ones(y_size, x_size);
Wafer_Final_Vbreakdown = -ones(y_size, x_size);
path_wafermap_texfile = [dir_structure '\wafermap.tex'];
fileID = fopen(path_wafermap_texfile, 'w');
if hbm_mode
    display("HBM - Mode")
    Wafer_Final_hbm = -ones(y_size, x_size);
    for i = 1:num_of_samples
        Wafer_Final_vt2(chip(i, 2), chip(i, 1)) = sample_values.Vt2(i);
        Wafer_Final_it2(chip(i, 2), chip(i, 1)) = sample_values.It2(i);
        Wafer_Final_hbm(chip(i, 2), chip(i, 1)) = sample_values.hbm(i);
        Wafer_Final_Vbreakdown(chip(i, 2), chip(i, 1)) = sample_values.Vbreakdown(i);
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_hbm, sample_values.hbm, 'HBM', 'V', num_of_samples, nonfails, contact_fails, figure(1), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_hbm, colorbar_values.colorbar_min_hbm, colorbar_values.colorbar_max_hbm, genParam, target_values.hbm, critical_area.hbm, notch_pos)
    catch ME
        warning('Wafermap could not be generated for HBM');
        ME.message
    end
    try 
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_it2, sample_values.It2, 'It_2', 'A', num_of_samples, nonfails, contact_fails, figure(2), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_it2, colorbar_values.colorbar_min_it2, colorbar_values.colorbar_max_it2, genParam, target_values.it2, critical_area.it2, notch_pos)
    catch ME
        warning('Wafermap could not be generated for It2');
        ME.message
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_vt2, sample_values.Vt2, 'Vt_2', 'V', num_of_samples, nonfails, contact_fails, figure(3), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_vt2, colorbar_values.colorbar_min_vt2, colorbar_values.colorbar_max_vt2, genParam, target_values.vt2, critical_area.vt2, notch_pos)
    catch ME
        warning('Wafermap could not be generated for Vt2');
        ME.message
    end
    
    
elseif snapback
    display("Snapback")
    Wafer_Final_vtrig = -ones(y_size, x_size);
    Wafer_Final_vhold = -ones(y_size, x_size);
    Wafer_Final_rdyn = -ones(y_size, x_size);
    rdyn_NaN = sample_values.rdyn;
    
    for i = 1:num_of_samples
        Wafer_Final_vt2(chip(i, 2), chip(i, 1)) = sample_values.Vt2(i);
        Wafer_Final_it2(chip(i, 2), chip(i, 1)) = sample_values.It2(i);
        Wafer_Final_vhold(chip(i, 2), chip(i, 1)) = sample_values.Vhold(i);
        Wafer_Final_vtrig(chip(i, 2), chip(i, 1)) = sample_values.Vtrig(i);
        Wafer_Final_Vbreakdown(chip(i, 2), chip(i, 1)) = sample_values.Vbreakdown(i);
        if (sample_values.rdyn(i)> 10) || sample_values.rdyn(i)<0
            contactfail = contactfail+1;
            Wafer_Final_rdyn(chip(i,2), chip(i,1)) = 0;
            rdyn_NaN(i,1) = NaN;
            
        else
            Wafer_Final_rdyn(chip(i,2), chip(i,1)) = sample_values.rdyn(i);
            rdyn_NaN(i,1) = sample_values.rdyn(i);
            
        end
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_it2, sample_values.It2, 'It_2', 'A', num_of_samples, nonfails, contact_fails, figure(2), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_it2, colorbar_values.colorbar_min_it2, colorbar_values.colorbar_max_it2, genParam, target_values.it2, critical_area.it2, notch_pos)
    catch ME
        warning('Wafermap could not be generated for It2');
        ME.message
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_vt2, sample_values.Vt2, 'Vt_2', 'V', num_of_samples, nonfails, contact_fails, figure(3), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_rdyn, colorbar_values.colorbar_min_vt2, colorbar_values.colorbar_max_vt2, genParam, target_values.vt2, critical_area.vt2, notch_pos)
    catch ME
        warning('Wafermap could not be generated for Vt2');
        ME.message
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_rdyn, sample_values.rdyn, 'R_{ON}', '\Omega', num_of_samples, nonfails, contact_fails, figure(1), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_rdyn, colorbar_values.colorbar_min_rdyn, colorbar_values.colorbar_max_rdyn, genParam, target_values.rdyn, critical_area.rdyn, notch_pos)
    catch ME
        warning('Wafermap could not be generated for R_ON');
        ME.message
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_vtrig, sample_values.Vtrig, 'V_{trig}', 'V', num_of_samples, nonfails, contact_fails, figure(6), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_vtrig, colorbar_values.colorbar_min_vtrig, colorbar_values.colorbar_max_vtrig, genParam, target_values.vtrig, critical_area.vtrig, notch_pos)
    catch ME
        warning('Wafermap could not be generated for Vtrig');
        ME.message
    end
    try
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_vhold, sample_values.Vhold, 'V_{hold x}', 'V', num_of_samples, nonfails, contact_fails, figure(4), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_vhold, colorbar_values.colorbar_min_vhold, colorbar_values.colorbar_max_vhold, genParam, target_values.vhold, critical_area.vhold, notch_pos)
    catch ME
        warning('Wafermap could not be generated for Vhold x');
        ME.message
    end
else
    disp('Regular Mode')
    % setze ung�ltige Werte auf 0 bzw. NaN
    Wafer_Final_rdyn = -ones(y_size, x_size);
    rdyn_NaN = sample_values.rdyn;
    [chip2,Wafer_Final_vt2,Wafer_Final_Vbreakdown,Wafer_Final_it2,Wafer_Final_rdyn] = Wafermap_emtyfile(subdie_x, subdie_y, offset_x, offset_y, ALLSAMP, sample_values,die_x, die_y,Wafer_Final_vt2,Wafer_Final_it2,Wafer_Final_Vbreakdown,Wafer_Final_rdyn);
    for i = 1:num_of_samples
        Wafer_Final_vt2(chip(i, 2), chip(i, 1)) = sample_values.Vt2(i);
        Wafer_Final_it2(chip(i, 2), chip(i, 1)) = sample_values.It2(i);
        Wafer_Final_Vbreakdown(chip(i, 2), chip(i, 1)) = sample_values.Vbreakdown(i);
        if sample_values.rdyn(i)<0 %(sample_values.rdyn(i)> 10) ||
            contactfail = contactfail+1;
            Wafer_Final_rdyn(chip(i,2), chip(i,1)) = 0;
            rdyn_NaN(i,1) = NaN;
            
        else
            Wafer_Final_rdyn(chip(i,2), chip(i,1)) = sample_values.rdyn(i);
            rdyn_NaN(i,1) = sample_values.rdyn(i);
        end
    end
    %chefai: Add the empty files in wafermap  
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_vt2, sample_values.Vt2, 'Vt_2', 'V', num_of_samples, nonfails, contact_fails, figure(), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_rdyn, colorbar_values.colorbar_min_vt2, colorbar_values.colorbar_max_vt2, genParam, target_values.vt2, critical_area.vt2, notch_pos,ALLSAMP)
    catch ME
        warning('Wafermap could not be generated for Vt2');
        ME.message
    end
    try 
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_it2, sample_values.It2, 'It_2', 'A', num_of_samples, nonfails, contact_fails, figure(), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_rdyn, colorbar_values.colorbar_min_it2, colorbar_values.colorbar_max_it2, genParam, target_values.it2, critical_area.it2, notch_pos,ALLSAMP)
    catch ME
        warning('Wafermap could not be generated for It2');
        ME.message
    end
    try
        close all
        generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_rdyn, sample_values.rdyn, 'R_{ON}', '\Omega', num_of_samples, nonfails, contact_fails, figure(), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_rdyn, colorbar_values.colorbar_min_rdyn, colorbar_values.colorbar_max_rdyn, genParam, target_values.rdyn, critical_area.rdyn, notch_pos,ALLSAMP)
    catch ME
        warning('Wafermap could not be generated for R_ON');
        ME.message
    end
    if  ~isnan(sample_values.Vbreakdown)
        try %chefai
            close all
            generate_wafermap2(die_x, subdie_x, die_y, subdie_y, offset_x, offset_y, y_size, x_size, Wafer_Final_Vbreakdown, sample_values.Vbreakdown, 'V_{Holding}', 'V', num_of_samples, nonfails, contact_fails, figure(), plotsave_dir, fileID, colorbar_values.colorbar_automatic_range_vhold, colorbar_values.colorbar_min_vhold, colorbar_values.colorbar_max_vhold, genParam, target_values.vhold, critical_area.vhold, notch_pos,ALLSAMP)
        catch ME
            warning('Wafermap could not be generated for V_{Holding}');
            ME.message
        end
    end
end
fclose(fileID);
end