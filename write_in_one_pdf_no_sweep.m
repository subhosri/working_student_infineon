function write_in_one_pdf_no_sweep(plotID, hbm_mode, data_matrix, start_index, stop_index, crit_spot, fit_neu, plot_VatI, plot_IatV, plot_V_clamp, Vtrig, Vhold, iMin, iMax, Vbreakdown, ind2, Option_Vbreakdown,Vhold_x,criterion_mode)
    %gleich wie ohne sweep
    %V = data_matrix(:, 2);%tlp voltage spot
    I = data_matrix(:, 3);%tlp current spot 

    if hbm_mode
        column = 1;
        xlabel_ = '            xlabel={Pulse[V]},\n';
        ylabel_ = '            ylabel={$I_{\\text{HBM}}$ [A]},\n';
        fit_neu = nan(1);
    else
        column = 2;
        xlabel_ = '            xlabel={$V_{\\text{TLP}}$ [V]},\n';
        ylabel_ = '            ylabel={$I_{\\text{TLP}}$ [A]},\n';
    end
    
    %[~, ~, ~, ~, plot_VatI, plot_IatV, plot_V_clamp] = HBM_calculations(Calc_case, It2, Vt2, R_dyn, VHBMlevel, PENUL, Die_coord, LAST, IforV, VforI, fid2, data_matrix, strContfail, nonfail, hbm_mode, hbm_at_it2, Vhold, Vtrig, Vbreakdown);

    fprintf(plotID,...
        ['    \\begin{tikzpicture}\n',... %%%% Stress Plot
        '        \\pgfplotsset{\n',...
        '            set layers,\n',...
        '            width=11.5cm,\n',...
        '            grid style={dotted, lightgray},\n',...
        '            /pgf/number format/.cd,\n',...
        '            1000 sep={},\n',...
        '        }\n',...
        '        \\begin{axis}[\n',...
        xlabel_,...
        ylabel_,...
        '            xtick pos=left,\n',...
        '            ytick pos=left,\n',...
        '            grid=both,\n',...
        '        ]\n']);
                    
	% these coordinates are always in the south west / north east corner of the plot
    fprintf(plotID,...
        ['            \\coordinate (sw) at (axis description cs:0, 0);\n',...
		'            \\coordinate (ne) at (axis description cs:1, 1);\n']);

    % draw before the \addplot command to avoid drawing over the plots
    fprintf(plotID,...
        ['            \\coordinate (sandys point) at (' num2str(data_matrix(crit_spot, column)) ',' num2str(data_matrix(crit_spot,3)) ');\n']);
     
    if ~isempty(Vhold_x)
         fprintf(plotID, ['            \\coordinate (sandys third point) at (' num2str(Vhold_x) ',' num2str(0) ');\n']);
     end
     
    if ~isempty(Vbreakdown)
        if Option_Vbreakdown==1
        fprintf(plotID, ['            \\coordinate (sandys second point) at (' num2str(Vbreakdown) ',' num2str(I(ind2)) ');\n']);
        elseif Option_Vbreakdown==2
        fprintf(plotID, ['            \\coordinate (sandys second point) at (' num2str(Vbreakdown) ',' num2str(0) ');\n']);   
        end
    end
   
    
    fprintf(plotID,...
    ['            \\draw[thick] (sandys point |- sw) -- (sandys point |- ne);\n',...
    '            \\draw[thick] (sandys point -| sw) -- (sandys point -| ne);\n',...
    '            \\addplot[blue,mark=square*,mark size=.65pt] table {\n']);
    % V_TLP
    for k = 1:size(data_matrix(:, 2), 1)
        fprintf(plotID, ['                ' num2str(data_matrix(k, column)) ' ' num2str(I(k)) '\n']);
    end
    fprintf(plotID, '            };\n');
    % Rdyn
    if ~isnan(fit_neu)
        range = data_matrix(start_index:stop_index, 2);
        fprintf(plotID, '            \\addplot[magenta,mark size=1pt, line width=.7pt] table {\n');
        for k = 1:size(range, 1)
            fprintf(plotID, ['                ' num2str(range(k)) ' ' num2str(fit_neu(k)) '\n']);
        end
        fprintf(plotID, '            };\n');
    end
    
    if ~isempty(iMax)
        
        fprintf(plotID,...
            ['            \\addplot[\n',...
            '                color = green,\n',...
            '                fill = green,\n',...
            '                mark = *,\n',...
            '                mark size = 1.2pt,\n',...
            '                only marks\n',...
            '            ] coordinates {\n']);
        
        for k = 1:size(iMax, 2)
            fprintf(plotID, ['                (' num2str(Vtrig(k)) ', ' num2str(I(iMax(k))) ')\n']);
        end
        fprintf(plotID,...
            '            };\n');
    end
    if ~isempty(iMin)
        fprintf(plotID,...
            ['            \\addplot[\n',...
            '                color = green,\n',...
            '                fill = green,\n',...
            '                mark = *,\n',...
            '                mark size = 1.2pt,\n',...
            '                only marks\n',...
            '            ] coordinates {\n']);
        
        for k = 1:size(iMin, 2)
            fprintf(plotID, ['                (' num2str(Vhold(k)) ', ' num2str(I(iMin(k))) ')\n']);
        end
        fprintf(plotID,...
            '            };\n');
    end
    fprintf(plotID, [plot_VatI plot_IatV plot_V_clamp]);
    
    minx = 1e-1*min([abs(data_matrix(:,8))'  abs(data_matrix(:,10))']);
    maxx = 1e2*max([abs(data_matrix(:,8))'  abs(data_matrix(:,10))']);
    
    fprintf(plotID,...
                '        \\end{axis}\n');
    
    if ~(criterion_mode == 18) 
            fprintf(plotID,...
                ['        \\begin{semilogxaxis}[\n',...
                '            axis y line*=right,\n',...
                '            axis x line*=top,\n',...
                '            xlabel={$I_{\\text{leak}}$ [A]},\n',...
                '            xtick pos=right,\n',...
                '            ytick pos=right,\n',...
                '            xmin=' num2str(minx) ',\n',...
                '            xmax=' num2str(maxx) ',\n',...
                '            legend style={legend pos=south east, font=\\tiny}\n',...
                '        ]\n',...
                '            \\addplot[red,mark=square*,mark size=.65pt] table { \n']);
            % spot 1 (red)
            for k = 1:size(data_matrix(:, 2), 1)
                fprintf(plotID, ['                ' num2str(abs(data_matrix(k, 8))) ' ' num2str(abs(data_matrix(k, 3))) '\n']);
            end
            fprintf(plotID,...
                ['            };\n',...
                '            \\addplot[green,mark=square*,mark size=.65pt] table {\n']);
            % spot 2 (green)
            for k = 1:size(data_matrix(:, 2), 1)
                fprintf(plotID, ['                ' num2str(abs(data_matrix(k, 10))) ' ' num2str(abs(data_matrix(k, 3))) '\n']);
            end

            fprintf(plotID,...
                ['            };\n',...
                '            \\legend{Spot 1,Spot 2}\n',...
                '        \\end{semilogxaxis}']);
    end

    % draw after the axes to be in front of everything
    if Vbreakdown
        fprintf(plotID,...
        ['        \\fill[red] (sandys second point) circle[radius=.7mm];\n',...
        '        \\draw[red,dashed] (sandys second point) -- (sandys second point |- sw);\n']);
    end
    if Vhold_x
                fprintf(plotID,...
            ['        \\fill[magenta] (sandys third point) circle[radius=.7mm];\n']);

    end
        
    fprintf(plotID, '    \\end{tikzpicture}\n');
end