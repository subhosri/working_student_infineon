function write_plot_statistics(parameter_values, device_name, angel, plotID, hbm_title, snapback, Vhold1x,pathstrc,plotID_stat,dev_name_stat)
% -----------------------------------------------------------------------
% writes the plots of the startistic in the tex file of the report
% Called by startistics 
% -----------------------------------------------------------------------
% Inputs: 
% parameter_values
% device_name
% angel 
% plotID: handle of the _plots.tex file
% hbm_title 
% snapback
% Vhold1x 
% pathstrc
% plotID_stat: handle of the _Plots_startistics.tex file
% dev_name_stat
% -----------------------------------------------------------------------
    [Dies,path_name,xticks_string_scatter] =  plot_statistik_png(parameter_values,pathstrc,dev_name_stat);
    write_plot_statistics_scatter(xticks_string_scatter,parameter_values,Dies, device_name, angel, hbm_title, snapback, Vhold1x,pathstrc,plotID_stat)
    if angel == 0
        xticks_string = '';
        xticks_string_rdyn = '';
        pgfplot = ['        \\pgfplotsset{ytick style={draw=none},\n',...
            '            xtick style={draw=none},\n',...
            '            grid=both,\n',...
            '            minor tick num=4,\n',...
            '            minor grid style = {gray!20, dotted},\n',...
            '            /pgf/number format/1000 sep={},\n',...
            '        }\n'];
    else
        xticks_string = ['                xtick=data,\n',...
                        '                xticklabel style={rotate=55,anchor=north east},\n',...
                        '                xticklabels={' strjoin(device_name) '},\n'];
        pgfplot = ['        \\pgfplotsset{small,\n',...
            '            ytick style={draw=none},\n',...
            '            xtick style={draw=none},\n',...
            '            grid=both,\n',...
            '            minor tick num=4,\n',...
            '            minor grid style = {gray!20, dotted},\n',...
            '            /pgf/number format/.cd,\n',...
            '            1000 sep={},\n',...
            '        }\n'];
        %'    \\hspace{1.5cm}\n'
        
        xticks_string_rdyn = ['                xtick=data,\n',...
                        '                xticklabel style={rotate=55,anchor=north east},\n',...
                        '                xticklabels={' strjoin(device_name(~isnan(parameter_values.rdyn))) '},\n'];
    end

    fprintf(plotID,...
        ['    \\begin{tikzpicture}\n',...
        pgfplot,...
        '        \\matrix {\n',...
        '            \\begin{axis}[\n',...
        '                title=Statistics of Vt$_2$,\n',...
        '                ylabel={Vt$_2$[V]},\n',...
        '                xlabel=Samples,\n',...
                         xticks_string,...
        '            ]\n',...
        '                \\addplot[red, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    
    for k = 1:size(parameter_values.Vt2, 1)
        fprintf(plotID,...
            ['                    ' num2str(k) ' ' num2str(parameter_values.Vt2(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            &\n',...
        '            \\begin{axis}[\n',...
        '                title=Statistics of It$_2$,\n',...
        '                ylabel={It$_2$[A]},\n',...
        '                xlabel=Samples,\n',...
                         xticks_string,...
        '            ]\n',...
        '                \\addplot[blue, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    for k = 1:size(parameter_values.It2, 1)
        fprintf(plotID,...
            ['                    ' num2str(k) ' ' num2str(parameter_values.It2(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            \\\\\n',...
        '            \\begin{axis}[\n',...
        '                title=Statistics of R$_\\text{ON}$,\n',...
        '                ylabel={R$_\\text{ON}$[$\\Omega$]},\n',...
        '                xlabel=Samples,\n',...
                         xticks_string_rdyn,...
        '            ]\n',...
        '                \\addplot[magenta, mark=*, mark size=1pt, line width=.5pt] table {\n']);
        
    for k = 1:size(parameter_values.rdyn, 1)
        fprintf(plotID,...
            ['                    ' num2str(k) ' ' num2str(parameter_values.rdyn(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            &\n',...
        '            \\begin{axis}[\n',...
        '                title=Statistics of ' hbm_title ',\n',...
        '                ylabel={HBM[kV]},\n',...
        '                xlabel=Samples,\n',...
                         xticks_string,...
        '            ]\n',...
        '                \\addplot[green, mark=*, mark size=1pt, line width=.5pt] table {\n']);
        for k = 1:size(parameter_values.hbm, 1)
        fprintf(plotID,...
            ['                    ' num2str(k) ' ' num2str(parameter_values.hbm(k)*1e-3) '\n']);
        end
    %% ---------- Jani
    
    if    ~isnan(parameter_values.Vbreakdown)
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            \\\\\n',...
        '            \\begin{axis}[\n',...
        '                title= statistics of V$_\\text{Holding}$,\n',...
        '                ylabel={V holding[V]},\n',...
        '                xlabel=Samples,\n',...
                         xticks_string,...
        '            ]\n',...
       '                \\addplot[green, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    for k = 1:size(parameter_values.Vbreakdown, 1)
        fprintf(plotID,...
            ['                    ' num2str(k) ' ' num2str(parameter_values.Vbreakdown(k)) '\n']);
    end
    end
    %% 
    
 
    
    if snapback
        
        if Vhold1x == 1
            fprintf(plotID,...
                ['                };\n',...
                '            \\end{axis}\n',...
                '            \\\\\n',...
                '            \\begin{axis}[\n',...
                '                title=Statistics of V$_{hold_1}$,\n',...
                '                ylabel={V$_{hold_1}$[V]},\n',...
                '                xlabel=Samples,\n',...
                                 xticks_string,...
                '            ]\n',...
                '                \\addplot[cyan, mark=*, mark size=1pt, line width=.5pt] table {\n']);
            for k = 1:size(parameter_values.Vhold, 1)
                fprintf(plotID,...
                    ['                    ' num2str(k) ' ' num2str(parameter_values.Vhold(k)) '\n']);
            end
        
        else
            fprintf(plotID,...
                ['                };\n',...
                '            \\end{axis}\n',...
                '            \\\\\n',...
                '            \\begin{axis}[\n',...
                '                title=Statistics of V$_{hold_x}$,\n',...
                '                ylabel={V$_{hold_x}$[V]},\n',...
                '                xlabel=Samples,\n',...
                                 xticks_string,...
                '            ]\n',...
                '                \\addplot[cyan, mark=*, mark size=1pt, line width=.5pt] table {\n']);
            for k = 1:size(parameter_values.Vholdx, 1)
                fprintf(plotID,...
                    ['                    ' num2str(k) ' ' num2str(parameter_values.Vholdx(k)) '\n']);
            end
        end
        
        fprintf(plotID,...
            ['                };\n',...
            '            \\end{axis}\n',...
            '            &\n',...
            '            \\begin{axis}[\n',...
            '                title=Statisticsof V$_\\text{trig}$,\n',...
            '                ylabel={V$_\\text{trig}$[V]},\n',...
            '                xlabel=Samples,\n',...
                             xticks_string,...
            '            ]\n',...
            '                \\addplot[violet, mark=*, mark size=1pt, line width=.5pt] table {\n']);
        for k = 1:size(parameter_values.Vtrig, 1)
            fprintf(plotID,...
                ['                    ' num2str(k) ' ' num2str(parameter_values.Vtrig(k)) '\n']);
        end
        
    end
    
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '        \\\\\n',...
        '        };\n',...
        '    \\end{tikzpicture}\n']);

 
    % Cumulative Distribution
    [CDF_rdyn, CDF_Vt2, CDF_It2, CDF_hbm, cdf,CDF_Vbreakdown] = calcCDF(parameter_values);
    fprintf(plotID,...
        ['    \\begin{tikzpicture}\n',...
        '        \\pgfplotsset{ytick style={draw=none},\n',...
        '            xtick style={draw=none},\n',...
        '            grid=both,\n',...
        '            minor tick num=4,\n',...
        '            minor grid style = {gray!20, dotted},\n',...
        '            /pgf/number format/.cd,\n',...
        '            1000 sep={},\n',...
        '        }\n',...
        '        \\matrix {\n',...
        '            \\begin{axis}[\n',...
        '                title=CDF of Vt$_2$,\n',...
        '                ylabel=CDF,\n',...
        '                xlabel={Vt$_2$[V]},\n',...
        '                grid=both,\n',...
        '            ]\n',...
        '                \\addplot[red, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    
    for k = 1:size(parameter_values.Vt2, 1)
        fprintf(plotID,...
            ['                    ' num2str(CDF_Vt2(k)) ' ' num2str(cdf(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            &\n',...
        '            \\begin{axis}[\n',...
        '                title=CDF of It$_2$,\n',...
        '                xlabel={It$_2$[A]},\n',...
        '                ylabel=CDF,\n',...
        '                grid=both,\n',...
        '            ]\n',...
        '                \\addplot[blue, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    for k = 1:size(parameter_values.It2, 1)
        fprintf(plotID,...
            ['                    ' num2str(CDF_It2(k)) ' ' num2str(cdf(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            \\\\\n',...
        '            \\begin{axis}[\n',...
        '                title=CDF of R$_\\text{dyn}$,\n',...
        '                xlabel={R$_\\text{dyn}$[$\\Omega$]},\n',...
        '                ylabel=CDF,\n',...
        '                grid=both,\n',...
        '            ]\n',...
        '                \\addplot[magenta, mark=*, mark size=1pt, line width=.5pt] table {\n']);
        
    for k = 1:size(parameter_values.rdyn, 1)
        fprintf(plotID,...
            ['                    ' num2str(CDF_rdyn(k)) ' ' num2str(cdf(k)) '\n']);
    end
        
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            &\n',...
        '            \\begin{axis}[\n',...
        '                title=CDF of ' hbm_title ',\n',...
        '                xlabel={HBM[kV]},\n',...
        '                ylabel=CDF,\n',...
        '                grid=both,\n',...
        '            ]\n',...
        '                \\addplot[green, mark=*, mark size=1pt, line width=.5pt] table {\n']);
    
    for k = 1:size(parameter_values.hbm, 1)
        fprintf(plotID,...
            ['                    ' num2str(CDF_hbm(k)*1e-3) ' ' num2str(cdf(k)) '\n']);
    end
    if ~isnan(parameter_values.Vbreakdown)
            fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            \\\\\n',...
        '            \\begin{axis}[\n',...
        '                title=CDF of R$_\\text{dyn}$,\n',...
        '                xlabel={V$_\\text{Breakdown}$[$\\Omega$]},\n',...
        '                ylabel=CDF,\n',...
        '                grid=both,\n',...
        '            ]\n',...
        '                \\addplot[magenta, mark=*, mark size=1pt, line width=.5pt] table {\n']);
        
    for k = 1:size(parameter_values.Vbreakdown, 1)
        fprintf(plotID,...
            ['                    ' num2str(CDF_rdyn(k)) ' ' num2str(cdf(k)) '\n']);
    end
    end
    fprintf(plotID,...
        ['                };\n',...
        '            \\end{axis}\n',...
        '            \\\\\n',...
        '        };\n',...
        '    \\end{tikzpicture}\n',...
        '\\newpage\n']);


end