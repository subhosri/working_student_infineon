function write_wafermap_tex(fileID, path_wafermap_pic)
% -----------------------------------------------------------------------
% Inserts the lines to load the wafermaps into the report tex-file
% Called by generate_wafermap2
% -----------------------------------------------------------------------
% Inputs:
% fileID: Handle of the report file
% path_wafermap_pic: path to the wafermap folder holding the pngs of the
% maps that were perviously generated
% -----------------------------------------------------------------------
    path_wafermap_pic = strrep(path_wafermap_pic, '\', '/');
    fprintf(fileID,...
        ['\\begin{figure}[H]\n',...
        '    \\centering\n',...
        '    \\includegraphics[trim=20 10 20 0, clip, width=.9\\textwidth]{' path_wafermap_pic '}\n',...
        '\\end{figure}\n',...
        '\\newpage\n']);
end